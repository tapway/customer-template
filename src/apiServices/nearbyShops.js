import HelperFetch from './HelperFetch';

/**
 * Fetch Nearby shops
 * @returns {Response:true,data}
 */

const handleFailure = () => {};
const fetchNearbyShops = async (limit = 10, skip = 0, shopType, location, handleSuccess, query) => {
  const extension = `/shops/getNearbyShopsByFilter`;
  await HelperFetch(
    {
      extension,
      post: true,
      body: {
        limit,
        skip,
        shopType,
        location,
        query,
      },
    },
    handleSuccess,
    handleFailure
  );
};
export default fetchNearbyShops;
