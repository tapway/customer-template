/* eslint-disable lines-between-class-members */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, Alert, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Header,
  Icon,
  Left,
  Body,
  Right,
  Card,
  Thumbnail,
  Button,
  CardItem,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
// import { loginResetAction } from '../../../navigation/resetNavigation';
import getImage from '../../../utils/getImage';
import styles from './styles';
import LogoutModal from '../../LogOut';

export class DrawerContent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fullName: '',
      logoutModal: false,
    };
  }

  componentDidMount() {
    const { state, props } = this;
    const { user } = props;
    this.setState({ ...state, ...user });
    console.log('profile props', this.props);
  }

  setLogoutModal = () => {
    this.setState(prevState => {
      return { logoutModal: !prevState.logoutModal };
    });
  };


  render() {
    const { state, props } = this;
    const { fullName } = this.state;
    return (
      <Container style={{ backgroundColor: '#1891D0' }}>
        <Header transparent androidStatusBarColor="transparent">
          <Left>
            <Button transparent onPress={() => Actions.HomePage()}>
              <Icon name="close" style={{ color: '#FFFF' }} />
            </Button>
          </Left>
          <Body />
          <Right />
        </Header>
        <Image source={require('../../../../assets/drawerBG.png')} resizeMode='contain' style={styles.drawerbg} />

        <Card transparent>
          <View style={{ flexDirection: 'row', padding: 16 }}>
            <Left
              style={{
                flexDirection: 'column',
                alignContent: 'flex-start',
                alignItems: 'flex-start',
              }}>
              {/* <View
                style={{
                  backgroundColor: '#ffff',
                  borderRadius: 5,
                  padding: 2,
                  shadowColor: '#0000',
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 2.0,
                  elevation: 4,
                }}>
                <Thumbnail
                  style={{ borderRadius: 5, height: 50, width: 50 }}
                  square
                  large
                  source={getImage.man}
                />
              </View> */}
              <View
                style={{
                  justifyContent: 'flex-end',
                  alignContent: 'flex-end',
                }}>
                <Text
                  style={{
                    color: '#ffffff',
                    fontFamily: 'Nunito-SemiBold',
                    fontSize: 20,
                    marginVertical: 10,
                  }}>
                  {fullName}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    Actions.drawerClose();
                    Actions.ProfileSetting();
                  }}>
                  <Text
                    style={{
                      color: '#ffffff',
                      opacity: 0.8,
                      fontSize: 15,
                    }}>
                    View Profile
                  </Text>
                </TouchableOpacity>
              </View>
            </Left>
          </View>
        </Card>
        <Content style={{ backgroundColor: '#FFFF' }}>
          <Card transparent>
            <TouchableOpacity onPress={() => Actions.HomePage()}>
              <CardItem>
                <Icon name="home" style={styles.listIcon} />
                <Text style={styles.listText}>Home</Text>
              </CardItem>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => Actions.OrderHistory()}>
              <CardItem>
                <Icon
                  type="MaterialCommunityIcons"
                  name="restore-clock"
                  style={styles.listIcon}
                />
                <Text style={styles.listText}>Order History</Text>
              </CardItem>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => Actions.TrackOrder()}>
              <CardItem>
                <Icon
                  type="MaterialCommunityIcons"
                  name="map-marker-radius"
                  style={[styles.listIcon]}
                />
                <Text style={[styles.listText, { paddingLeft: 5 }]}>
                  Track Order
                </Text>
              </CardItem>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => Actions.Wallet()}>
              <CardItem>
                <Icon name="wallet" style={styles.listIcon} />
                <Text style={styles.listText}>Wallet</Text>
              </CardItem>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => Actions.FAQ()}>
              <CardItem>
                <Icon
                  type="MaterialCommunityIcons"
                  name="comment-question"
                  style={styles.listIcon}
                />
                <Text style={styles.listText}>FAQs</Text>
              </CardItem>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => Actions.CustomerCare()}>
              <CardItem>
                <Icon
                  type="AntDesign"
                  name="customerservice"
                  style={styles.listIcon}
                />
                <Text style={styles.listText}>Customer Care</Text>
              </CardItem>
            </TouchableOpacity>

            {/* <TouchableOpacity onPress={() => Actions.ReviewPage()}>
              <CardItem>
                <Icon type="AntDesign" name="star" style={styles.listIcon} />
                <Text style={styles.listText}>Review</Text>
              </CardItem>
            </TouchableOpacity> */}

            {/* <CardItem />
            <LogoutModal
              navigate={props.navigation}
              logoutModal={state.logoutModal}
              setLogoutModal={this.setLogoutModal}
            /> */}
            <TouchableOpacity
              onPress={() =>
                // {
                this.setLogoutModal()

              }>
              {/* <CardItem>
                <Icon
                  type="MaterialCommunityIcons"
                  name="logout"
                  style={styles.listIcon}
                />
                <Text style={styles.listText}>Logout</Text>
              </CardItem> */}
            </TouchableOpacity>
          </Card>
        </Content>
      </Container >
    );
  }
}

const mapStateToProps = state => ({
  user: state.currentUser,
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DrawerContent);
