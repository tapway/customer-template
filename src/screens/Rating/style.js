import { StyleSheet, Dimensions } from 'react-native';
import Colors from '../../utils/colors';
import { heightRatio } from '../../utils/styleSheetGuide';

const { height, width } = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.themeBackgroundColor,
  },
  headingConatiner: {
    alignItems: 'center',
  },
  headingText: {
    marginTop: height / 20,
    fontSize: height / 40,
    fontWeight: '300',
  },
  firstContainer: {
    height: 100 * heightRatio,
    alignItems: 'center',
    justifyContent: 'center',
  },
  amountPaidText: {
    fontSize: height / 40,
    fontWeight: '800',
  },
  amountText: {
    marginTop: height / 150,
    fontSize: height / 20,
    fontWeight: '800',
    color: Colors.secondaryColor,
  },
  ratingContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 30 * heightRatio,
  },
  userIconImage: {
    height: width / 4,
    width: width / 4,
    borderRadius: width / 4 / 2,
  },
  userName: {
    marginTop: height / 50,
    fontSize: height / 40,
  },
  ratingDescritionContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: width / 15,
    marginRight: width / 15,
    marginVertical: 20 * heightRatio,
  },
  ratingDescritionText: {
    fontSize: height / 50,
    textAlign: 'center',
    marginBottom: height / 100,
  },
  lastContainer: {
    alignItems: 'center',
    marginTop: 20 * heightRatio,
  },
  loginButton: {
    width: width / 2,
    alignItems: 'center',
    padding: width / 30,
    borderRadius: 50,
    backgroundColor: Colors.primaryBtnBgColor,
    justifyContent: 'center',
    borderColor: Colors.transparent,
  },
  loginBtnContainer: {
    marginTop: height / 30,
    // flex: 2,
    alignItems: 'center',
  },
  loginText: {
    color: Colors.textForPrimaryBtnBgColor,
    fontSize: height / 40,
  },
  skipFeedbackButton: {
    marginTop: height / 13,
  },
  skipFeedbackText: {
    fontSize: height / 50,
  },
});
