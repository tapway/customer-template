import React from 'react';
import { View, ScrollView } from 'react-native';
import { PlaceholderContainer, Placeholder } from 'react-native-loading-placeholder';
import LinearGradient from 'react-native-linear-gradient';
import PropTypes from 'prop-types';
import styles from './style';

const Loading = props => {
  const { loadingComponent } = props;
  return (
    <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
      <PlaceholderExample loader={loadingComponent} />
    </ScrollView>
  );
};
const Gradient = () => {
  return (
    <LinearGradient
      colors={['#eeeeee', '#dddddd', '#eeeeee']}
      start={{ x: 1.0, y: 0.0 }}
      end={{ x: 0.0, y: 0.0 }}
      style={{
        flex: 1,
        width: 120,
      }}
    />
  );
};

const PlaceholderExample = ({ loader }) => {
  return (
    <PlaceholderContainer
      animatedComponent={<Gradient />}
      duration={1000}
      delay={1000}
      loader={loader}
    >
      <View style={[styles.placeholderSingleContainer]}>
        <View style={styles.resDetails}>
          <Placeholder style={[styles.placeholder, styles.placeholderName]} />
          <Placeholder style={[styles.placeholder, styles.placeholderBrand]} />
        </View>
        <View style={styles.resDetails}>
          <Placeholder style={[styles.placeholder, styles.placeholderButton]} />
        </View>
      </View>
      <View style={[styles.placeholderSingleContainer]}>
        <View style={styles.resDetails}>
          <Placeholder style={[styles.placeholder, styles.placeholderName]} />
          <Placeholder style={[styles.placeholder, styles.placeholderBrand]} />
        </View>
        <View style={styles.resDetails}>
          <Placeholder style={[styles.placeholder, styles.placeholderButton]} />
        </View>
      </View>
      <View style={[styles.placeholderSingleContainer]}>
        <View style={styles.resDetails}>
          <Placeholder style={[styles.placeholder, styles.placeholderName]} />
          <Placeholder style={[styles.placeholder, styles.placeholderBrand]} />
        </View>
        <View style={styles.resDetails}>
          <Placeholder style={[styles.placeholder, styles.placeholderButton]} />
        </View>
      </View>
      <View style={[styles.placeholderSingleContainer]}>
        <View style={styles.resDetails}>
          <Placeholder style={[styles.placeholder, styles.placeholderName]} />
          <Placeholder style={[styles.placeholder, styles.placeholderBrand]} />
        </View>
        <View style={styles.resDetails}>
          <Placeholder style={[styles.placeholder, styles.placeholderButton]} />
        </View>
      </View>
      <View style={[styles.placeholderSingleContainer]}>
        <View style={styles.resDetails}>
          <Placeholder style={[styles.placeholder, styles.placeholderName]} />
          <Placeholder style={[styles.placeholder, styles.placeholderBrand]} />
        </View>
        <View style={styles.resDetails}>
          <Placeholder style={[styles.placeholder, styles.placeholderButton]} />
        </View>
      </View>
    </PlaceholderContainer>
  );
};

Loading.propTypes = {
  loadingComponent: PropTypes.objectOf(PropTypes.any),
};
Loading.defaultProps = {
  loadingComponent: undefined,
};

PlaceholderExample.propTypes = {
  loader: PropTypes.objectOf(PropTypes.any),
};
PlaceholderExample.defaultProps = {
  loader: undefined,
};
export default Loading;
