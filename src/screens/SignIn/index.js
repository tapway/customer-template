/* eslint-disable react/no-unused-state */
import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native';
import {
  Container,
  Content,
  Button,
  Item,
  // Input,
  Text,
  Header,
  Grid,
  Row,
  Col,
  Left,
  Body,
  Spinner,
  //   Image,
} from 'native-base';
import { connect } from 'react-redux';
import PhoneInput from 'react-native-phone-input';
import Toast from 'react-native-easy-toast';
// import { get } from 'lodash';
import PropTypes from 'prop-types';
import Image from 'react-native-remote-svg';
// import { Actions } from 'react-native-router-flux';
import getImage from '../../utils/getImage';
// import Colors from '../../utils/colors';
import { RFValue, RFPercentage } from 'react-native-responsive-fontsize';
import { FontNames } from '../../../theme';

import signInUserDb from '../../apiServices/signIn';
import { signInStyles } from './style';
import Colors from '../../utils/tapColors';

// import height from '../../utils/layout';

class SignIn extends Component {
  constructor() {
    super();
    this.state = {
      defaultCode: '+234',
      phoneNumber: '+234',
      nameError: '',
      userInfo: null,
      imageLoader: false,
      phoneNumberModal: false,
      hidepassword: true,
      isLoading: false,
    };
    this.phone = null;
  }

  componentDidMount() {
    // const { props } = this;
  }

  signup = userInfo => {
    this.setState({ userInfo });
    const { props } = this;
    props.navigation.navigate('signUp', { userInfo });
  };

  signIn = async () => {
    const { refs } = this;
    refs.toast.show('Temporarily Not Available');
  };

  signInUser = () => {
    const { refs, state } = this;
    if (this.phone.isValidNumber()) {
      this.setState({ isLoading: true });
      signInUserDb(state.phoneNumber, this.handleResult, this.handleNotFound);
    } else {
      refs.toast.show('Phone number invalid');
    }
  };

  handleNotFound = () => {
    const { refs } = this;
    this.setState({ isLoading: false });
    refs.toast.show('Phone number not found');
  };

  handleResult = () => {
    const { props, state } = this;
    this.setState({ isLoading: false });
    props.navigation.navigate('verify', { phoneNumber: state.phoneNumber });
  };

  gotoSignUp = () => {
    const { props } = this;
    props.navigation.navigate('signUp');
  };

  gotoCheckPrice = () => {
    const { props } = this;
    props.navigation.navigate('checkPrice');
  };

  gotoCheckDeliveryTime = () => {
    const { props } = this;
    props.navigation.navigate('checkDeliveryTime');
  };

  gotoCheckNearestDriver = () => {
    const { props } = this;
    props.navigation.navigate('checkNearestDriver');
  };

  gotoForgetPassword = () => {
    const { props } = this;
    props.navigation.navigate('forgetPassword');
  };

  render() {
    const { phoneNumber, isLoading } = this.state;
    // const { hidepassword } = this.state;
    return (
      <React.Fragment>
        <Container style={signInStyles.container}>
          <Header
            transparent
            // hasTabs
            iosBarStyle="light-content"
            style={{ height: 5 }}
            androidStatusBarColor={Colors.primary}>
            <Left />
            <Body />
          </Header>
          <Content
            scrollEnabled
            keyboardShouldPersistTaps="never"
            contentContainerStyle={{
              padding: 16,
              // minHeight: height,
            }}>
            <View style={{ marginVertical: 5 }}>
              <Text style={signInStyles.welcomeHd}>Welcome</Text>
            </View>

            <View style={{ justifyContent: 'center', marginVertical: 20 }}>
              <View style={signInStyles.inputContainer}>
                <Text style={signInStyles.inputLabel}>
                  Enter Mobile Number to Proceed
                </Text>
                <Item regular style={signInStyles.inputItem}>
                  {/* <Input
                    placeholder="willywale@gmail.com"
                    placeholderTextColor="#FFF9"
                    style={signInStyles.inputText}
                  /> */}
                  <PhoneInput
                    ref={ref => {
                      this.phone = ref;
                    }}
                    value={phoneNumber}
                    onChangePhoneNumber={text => {
                      this.setState({
                        phoneNumber: text,
                        isLoading: false,
                      });
                    }}
                    textStyle={{ fontSize: RFValue(16), color: '#fff', fontFamily: FontNames.regular }}
                    textProps={{
                      placeholder: 'Enter Phone Number',
                      autoFocus: true,
                      onSubmitEditing: () => this.signInUser()
                    }}
                    style={signInStyles.phoneInput}
                  />
                </Item>
              </View>

              {/* <View style={signInStyles.inputContainer}>
                <Text style={signInStyles.inputLabel}>Password</Text>
                <Item regular style={signInStyles.inputItem}>
                  <Input
                    placeholder="Password"
                    placeholderTextColor="#FFF9"
                    secureTextEntry={hidepassword}
                    style={signInStyles.inputText}
                  />
                  <TouchableNativeFeedback
                    onPressIn={() => this.setState({ hidepassword: false })}
                    onPressOut={() => this.setState({ hidepassword: true })}
                    background={TouchableNativeFeedback.SelectableBackground()}>
                    <View style={{ padding: 10 }}>
                      <Text style={[signInStyles.inputText, { fontSize: 14 }]}>
                        SHOW
                      </Text>
                    </View>
                  </TouchableNativeFeedback>
                </Item>
              </View> */}

              {/* <View>
                <TouchableOpacity
                  style={{ padding: 0, margin: 0 }}
                  transparent
                  onPress={this.gotoForgetPassword}>
                  <Text
                    style={{
                      color: '#FFFFFF',
                      fontSize: 16,
                      fontFamily: 'Nunito-Regular',
                    }}>
                    Forgot Password?
                  </Text>
                </TouchableOpacity>
              </View> */}

              <Button
                block
                primary
                style={signInStyles.submitBtn}
                onPress={() => this.signInUser()}>
                {(!isLoading && (
                  <Text style={signInStyles.btnText}>LOGIN</Text>
                )) || <Spinner />}
              </Button>

              <View>
                <Text
                  style={{
                    color: '#FFFFFF',
                    textAlign: 'center',
                    fontFamily: FontNames.regular,
                  }}>
                  Don&apos;t have an account?
                </Text>
                <TouchableOpacity onPress={this.gotoSignUp}>
                  <Text
                    style={{
                      color: '#FFFFFF',
                      textAlign: 'center',
                      paddingTop: 3,
                      fontFamily: FontNames.bold,
                    }}>
                    Signup Now
                </Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={signInStyles.actionContainer}>
              <View style={{ marginVertical: 16 }}>
                <Text style={signInStyles.actionTitle}>Quick Actions</Text>
              </View>
              <Grid>
                <Row>
                  <Col style={{ alignSelf: 'center' }}>
                    <TouchableOpacity transparent onPress={this.gotoCheckPrice}>
                      <View style={signInStyles.actionColView}>
                        <Image
                          style={signInStyles.actionImageSize}
                          source={getImage.naira}
                          resizeMode='contain'
                        />
                      </View>
                    </TouchableOpacity>
                    <View
                      style={{
                        alignSelf: 'center',
                      }}>
                      <Text style={signInStyles.actionText}>
                        {'Check Price\n'}
                      </Text>
                    </View>
                  </Col>
                  <Col style={{ alignSelf: 'center' }}>
                    <TouchableOpacity
                      transparent
                      onPress={this.gotoCheckDeliveryTime}>
                      <View style={signInStyles.actionColView}>
                        <Image
                          style={signInStyles.actionImageSize}
                          source={getImage.clock}
                        />
                      </View>
                    </TouchableOpacity>
                    <View
                      style={{
                        alignSelf: 'center',
                      }}>
                      <Text style={signInStyles.actionText}>
                        {'Check Delivery\n Time'}
                      </Text>
                    </View>
                  </Col>
                  <Col style={{ alignSelf: 'center' }}>
                    <TouchableOpacity
                      transparent
                      onPress={this.gotoCheckNearestDriver}>
                      <View style={signInStyles.actionColView}>
                        <Image
                          style={signInStyles.actionImageSize}
                          source={getImage.scooter}
                        />
                      </View>
                    </TouchableOpacity>
                    <View
                      style={{
                        alignSelf: 'center',
                      }}>
                      <Text style={signInStyles.actionText}>
                        {'Check Nearest\n Driver'}
                      </Text>
                    </View>
                  </Col>
                </Row>
              </Grid>
            </View>
          </Content>
          <Image
            source={getImage.loginBG}
            resizeMode='cover'
            style={signInStyles.bgimg}
          />
          <Toast
            ref="toast"
            style={{ backgroundColor: '#FFFFFF' }}
            position="bottom"
            positionValue={100}
            fadeInDuration={750}
            fadeOutDuration={1000}
            opacity={0.8}
            textStyle={{ color: Colors.toastTextColor, fontSize: 15 }}
          />
        </Container>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    config: state.config,
  };
};

SignIn.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  null
)(SignIn);
