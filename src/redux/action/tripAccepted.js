import { SET_TRIP_INFO } from '../actionTypes';

/**
 * Set Trip Info To Redux
 */
const setTripInfo = incomingPayload => {
  return {
    type: SET_TRIP_INFO,
    payload: incomingPayload,
  };
};
export default setTripInfo;
