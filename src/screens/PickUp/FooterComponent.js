import React from 'react';
import { View, TouchableOpacity, Image } from 'react-native';
import PropTypes from 'prop-types';
import styles from './style';
import getImage from '../../utils/getImage';

const FooterComponent = props => {
  const { id, getDetailsModalVisible, toggleGetDetailsModal, toggleConfirmOrderModal } = props;
  return (
    <View style={styles.footerContainer}>
      <TouchableOpacity
        onPress={
          id === 1
            ? () => {
                toggleGetDetailsModal();
              }
            : () => {
                toggleConfirmOrderModal();
              }
        }
      >
        {!getDetailsModalVisible && (
          <Image
            resizeMode="contain"
            source={getImage.footerNavigationIcon}
            style={styles.footerGoBtn}
          />
        )}
      </TouchableOpacity>
    </View>
  );
};

FooterComponent.propTypes = {
  id: PropTypes.number.isRequired,
  getDetailsModalVisible: PropTypes.bool.isRequired,
  toggleGetDetailsModal: PropTypes.func.isRequired,
  toggleConfirmOrderModal: PropTypes.func.isRequired,
};

export default FooterComponent;
