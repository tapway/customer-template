import { StyleSheet } from 'react-native';
import { RFPercentage, RFValue } from 'react-native-responsive-fontsize';
const styles = StyleSheet.create({
  walletAmount: { fontSize: 25, color: '#030E1A' },
  greenTitle: { color: '#3FAF5D', fontSize: 16 },
  walletButton: { backgroundColor: '#3FAF5D', padding: 10 },
  historyText: { color: '#030E1A', fontSize: 20 },
  historyType: {
    color: 'rgba(3, 14, 26, 0.5)',
    fontSize: 16,
  },
  historyTitle: { color: '#030E1A', fontSize: 20 },
  headtxtdiv: { position: 'absolute', left: 0, right: 0, top: RFPercentage(5.5), zIndex: -100 },
});
export default styles;
