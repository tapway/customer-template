/* eslint-disable no-use-before-define */
import React from 'react';
import { View, Text, TouchableOpacity, Modal, Image, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import getImage from '../utils/getImage';

import { heightRatio, widthRatio } from '../utils/styleSheetGuide';
import Colors from '../utils/colors';

const ClearCartModal = props => {
  const { clearCartModal } = props;
  return (
    <Modal
      transparent
      visible={clearCartModal}
      onRequestClose={() => {
        props.setClearCartModal();
      }}
    >
      <View style={styles.backgroundContainer}>
        <View style={styles.mainContainer}>
          <View style={styles.imgContainer}>
            <Image resizeMode="contain" source={getImage.cart} style={styles.cartImg} />
          </View>
          <View>
            <Text style={styles.sureText}>Clear Cart</Text>
            <Text style={{ lineHeight: 16 * heightRatio }}>
              If you leave this page you will lose the items added. Are you sure you want to
              continue?
            </Text>
          </View>
          <View style={styles.btnsCntainer}>
            <TouchableOpacity style={[styles.clearBtn]} onPress={() => props.clear()}>
              <Text style={styles.btnText}>Clear</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.continueBtn]}
              onPress={() => {
                props.setClearCartModal();
              }}
            >
              <Text style={[styles.btnText, styles.continueColor]}>Stay here</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};
ClearCartModal.propTypes = {
  clearCartModal: PropTypes.bool.isRequired,
  setClearCartModal: PropTypes.func.isRequired,
  clear: PropTypes.func.isRequired,
};
export default ClearCartModal;

const styles = StyleSheet.create({
  backgroundContainer: {
    flex: 1,
    backgroundColor: Colors.modalBackgroundColor,
    justifyContent: 'flex-end',
  },
  mainContainer: {
    height: 200 * heightRatio,
    backgroundColor: Colors.modalContainerColor,
    borderTopColor: Colors.modalBorderColor,
    borderTopWidth: 4,
    elevation: 80,
    flexDirection: 'column',
    paddingLeft: 17 * widthRatio,
    paddingRight: 26 * widthRatio,
    paddingTop: 20 * heightRatio,
    paddingBottom: 29 * heightRatio,
  },
  imgContainer: {
    alignItems: 'flex-end',
  },
  cartImg: {
    height: 26 * heightRatio,
    width: 32 * heightRatio,
    marginBottom: 5 * heightRatio,
  },
  clearBtn: {
    height: 39 * heightRatio,
    width: 115 * widthRatio,
    borderRadius: (46 / 2) * heightRatio,
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: Colors.secondaryBtnBgColor,
    marginTop: 20 * heightRatio,
  },
  continueBtn: {
    height: 39 * heightRatio,
    width: 168 * widthRatio,
    borderRadius: (46 / 2) * heightRatio,
    backgroundColor: Colors.primaryBtnBgColor,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20 * heightRatio,
  },
  sureText: {
    fontSize: 14 * heightRatio,
    fontWeight: '600',
    marginBottom: 8 * heightRatio,
  },
  secondaryText: {
    fontSize: 12 * heightRatio,
    fontWeight: '300',
  },
  primaryColor: { backgroundColor: Colors.primaryColor },
  btnText: { fontSize: 16 * heightRatio, textAlign: 'center' },
  continueColor: { color: Colors.textForPrimaryBtnBgColor },
  btnsCntainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
});
