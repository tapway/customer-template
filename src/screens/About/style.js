import { StyleSheet } from 'react-native';

import { heightRatio, widthRatio } from '../../utils/styleSheetGuide';
import Colors from '../../utils/colors';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  keyValueContainer: {
    flex: 8,
    marginLeft: 18 * widthRatio,
  },
  key: {
    fontSize: 18,
    fontWeight: '100',
  },
  bottomLine: {
    height: 1,
    marginTop: 14.5 * heightRatio,
    marginBottom: 8.5 * heightRatio,
    marginRight: 22 * widthRatio,
    backgroundColor: Colors.primaryBorderColor,
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: 22 * widthRatio,
  },
  value: {
    fontWeight: 'bold',
    marginTop: 2 * heightRatio,
    color: '#999999',
  },

  bottomLastLine: {
    height: 1,
    marginTop: 14.5 * heightRatio,
    marginRight: 22 * widthRatio,
    backgroundColor: Colors.primaryBorderColor,
  },
});
