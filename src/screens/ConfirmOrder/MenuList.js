import React from 'react';
import { View, Text, FlatList } from 'react-native';
import PropTypes from 'prop-types';
import styles from './style';

const MenuList = props => {
  const { data } = props;
  return (
    <FlatList
      data={data}
      keyExtractor={item => item._id}
      showsVerticalScrollIndicator={false}
      ItemSeparatorComponent={() => {
        return <View style={styles.itemSeperator}></View>;
      }}
      renderItem={({ item }) => (
        <View style={styles.sigleListContainer}>
          <View style={styles.sigleListDescription}>
            <Text numberOfLines={1} style={styles.menuDescriptionText}>
              {item.itemDescription}
            </Text>
            <View style={styles.itemBrand}>
              <Text style={styles.itemBrandText}>Price : </Text>
              <Text style={styles.itemPriceText}>${item.price}</Text>
            </View>
          </View>
          <View style={styles.sigleListQuantityCont}>
            <View style={styles.quantityCont}>
              <Text style={styles.quantityText}>{item.quantity}</Text>
            </View>
          </View>
        </View>
      )}
      style={styles.productList}
    />
  );
};

MenuList.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number]))
  ).isRequired,
};

export default MenuList;
