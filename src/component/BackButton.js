/* eslint-disable no-use-before-define */
import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import IconI from 'react-native-vector-icons/Ionicons';
import PropTypes from 'prop-types';

import { heightRatio } from '../utils/styleSheetGuide';

const BackButton = props => {
  const { back } = props;
  return (
    <TouchableOpacity onPress={back}>
      <View style={styles.btnContainer}>
        <IconI color='#030E1A' name="md-arrow-back" size={23 * heightRatio} />
      </View>
    </TouchableOpacity>
  );
};
BackButton.propTypes = {
  back: PropTypes.func.isRequired,
};
export default BackButton;
const styles = StyleSheet.create({
  btnContainer: {
    height: 30 * heightRatio,
    width: 30 * heightRatio,
    borderRadius: 15 * heightRatio,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
