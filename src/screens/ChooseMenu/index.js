/* eslint-disable react/no-unused-state */
import React, { Component } from 'react';
import Toast from 'react-native-easy-toast';
import { connect } from 'react-redux';
import { View, Text, TouchableOpacity, BackHandler } from 'react-native';
import _ from 'lodash';
import PropTypes from 'prop-types';
import Header from '../../component/HeaderBar';
import ListBuilder from './ListBuilder';
import styles from './style';
import ClearCart from '../../component/ClearCartModal';
import { fetchItemsByShop } from '../../apiServices/fetchItemsInShop';
import Loading from './Loading';
import SearchBar from '../../component/SearchBar';
import TopComponent from './TopComponent';
import Colors from '../../utils/colors';
import { CLEAR_CART } from '../../redux/actionTypes';

class ChooseMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      vegMenu: [],
      nonVegMenu: [],
      searchVegMenu: [],
      searchNonVegMenu: [],

      shop: props.navigation.getParam('shop'),
      scroll: false,

      loaded: false,
      vegLoaded: false,
      nonVegLoaded: false,
      moreVegLoading: false,
      moreNonVegLoading: false,
      noMoreVegItems: false,
      noMoreNonVegItems: false,

      type: 'Veg',
      vegStatus: true,

      query: '',

      clearCartModal: false,
    };
  }

  componentDidMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.androidBack);
    const { props, state } = this;
    const resId = props.navigation.getParam('id');
    fetchItemsByShop(10, 0, 'Veg', resId, this.fillItems, state.query);
  }

  componentWillUnmount() {
    this.backHandler.remove();
    this.loadingComponent = new Promise(resolve => {
      resolve(true);
    });
  }

  androidBack = () => {
    this.handleBack(); // works best when the goBack is async
    return true;
  };

  fillItems = res => {
    const { data: result } = res;
    const { state } = this;
    const obj = state.vegStatus
      ? { vegMenu: result, vegLoaded: true }
      : { nonVegMenu: result, nonVegLoaded: true };
    this.setState(obj);
  };

  loadMore = () => {
    const { state } = this;
    const skip = state.vegStatus ? state.vegMenu.length : state.nonVegMenu.length;
    fetchItemsByShop(10, skip, state.type, state.shop._id, this.addMoreItems, state.query);
  };

  addMoreItems = res => {
    const { data: result } = res;
    const { state } = this;
    if (result.length === 0) {
      const obj = state.vegStatus ? { noMoreVegItems: true } : { noMoreNonVegItems: true };
      this.setState(obj);
      return null;
    }
    if (result[0].itemType === 'Veg') {
      const arr = _.uniqBy([...state.vegMenu, ...result], item => {
        return item._id;
      });
      this.setState({ vegMenu: arr }, () => {
        this.setState(prevState => {
          return { moreVegLoading: !prevState.moreVegLoading };
        });
      });
    } else {
      const arr = _.uniqBy([...state.nonVegMenu, ...result], item => {
        return item._id;
      });
      this.setState({ nonVegMenu: arr }, () => {
        this.setState(prevState => {
          return { moreNonVegLoading: !prevState.moreNonVegLoading };
        });
      });
    }
    return null;
  };

  handleSubmit = () => {
    const { props, refs, state } = this;
    if (props.cart.length === 0) refs.toast.show('Select at least 1 item!');
    else {
      props.navigation.navigate('confirmOrder', { shop: state.shop });
    }
  };

  handleBack = () => {
    const { props } = this;
    if (props.cart.length !== 0) this.setClearCartModal();
    else {
      props.navigation.goBack();
    }
  };

  handleQueryChange = query => {
    const { state } = this;
    const obj = state.vegStatus ? { query, vegLoaded: false } : { query, nonVegLoaded: false };
    this.setState(obj, () => {
      const { state: newState } = this;
      fetchItemsByShop(10, 0, newState.type, newState.shop._id, this.fillItems, newState.query);
    });
  };

  handleStateChange = payload => {
    this.setState(payload);
  };

  changeFoodType = (foodType, status) => {
    this.setState(
      {
        type: foodType,
        vegStatus: status,
      },
      () => {
        this.handleQueryChange('');
        this.setState({
          noMoreNonVegItems: false,
          noMoreVegItems: false,
          scroll: false,
        });
      }
    );
  };

  clear = () => {
    const { props } = this;
    this.setClearCartModal();
    props.clearCartDispatcher();
    props.navigation.goBack();
  };

  setClearCartModal = () => {
    this.setState(prevState => {
      return { clearCartModal: !prevState.clearCartModal };
    });
  };

  render() {
    const { state } = this;
    return (
      <View style={styles.container}>
        <Header plusTitle title="Choose menu" back={this.handleBack} />
        <ClearCart
          clearCartModal={state.clearCartModal}
          setClearCartModal={this.setClearCartModal}
          clear={this.clear}
        />
        <View style={styles.fullFlex}>
          <TopComponent
            shop={state.shop}
            scroll={state.scroll}
            vegStatus={state.vegStatus}
            vegMenu={state.vegMenu}
            nonVegMenu={state.nonVegMenu}
            handleStateChange={this.handleStateChange}
            changeFoodType={this.changeFoodType}
          ></TopComponent>
          <View style={styles.menuList}>
            <SearchBar
              handleQueryChange={this.handleQueryChange}
              value={state.query}
              itemList={state.vegStatus ? state.vegMenu : state.nonVegMenu}
            />
            {(state.vegStatus && state.vegLoaded) || (!state.vegStatus && state.nonVegLoaded) ? (
              <ListBuilder
                handleStateChange={this.handleStateChange}
                handleQueryChange={this.handleQueryChange}
                scroll={state.scroll}
                loadMore={this.loadMore}
                vegStatus={state.vegStatus}
                vegMenu={state.vegMenu}
                nonVegMenu={state.nonVegMenu}
                noMoreVegItems={state.noMoreVegItems}
                noMoreNonVegItems={state.noMoreNonVegItems}
                moreVegLoading={state.moreVegLoading}
                moreNonVegLoading={state.moreNonVegLoading}
              />
            ) : (
              <Loading loadingComponent={this.loadingComponent} />
            )}
          </View>
          <TouchableOpacity
            style={styles.footer}
            onPress={() => {
              this.handleSubmit();
            }}
          >
            <Text style={styles.footerText}>Continue</Text>
          </TouchableOpacity>
        </View>
        <Toast
          ref="toast"
          style={{ backgroundColor: Colors.toastBgColor }}
          position="bottom"
          positionValue={400}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: Colors.toastTextColor, fontSize: 15 }}
        />
      </View>
    );
  }
}
const mapStateToProps = state => {
  return { cart: state.cart };
};
const mapDispatchToProps = dispatch => {
  return {
    clearCartDispatcher: () => {
      dispatch({ type: CLEAR_CART });
    },
  };
};

ChooseMenu.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  cart: PropTypes.arrayOf(
    PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number]))
  ).isRequired,
  clearCartDispatcher: PropTypes.func.isRequired,
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChooseMenu);
