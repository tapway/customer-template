import React, { Component } from 'react';
import { View, Text, Animated } from 'react-native';
import PropTypes from 'prop-types';
import Image from 'react-native-remote-svg';
// import styles from './style';
import AppIntroSlider from 'react-native-app-intro-slider';
import { Header } from 'native-base';
import { store } from '../../redux/configureStore';
import { welcomeStyles } from './style';
import slides from '../../utils/slide';
// import { Actions } from 'react-native-router-flux';  //we'r using navigation instead of Actions

export default class WelcomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //   imageLoader: false,
      showRealApp: false,
    };
    this.springValue = new Animated.Value(0.3);
  }

  componentDidMount() {
    this.spring();
  }

  // Fetch the token from storage then navigate to our appropriate place
  check = async () => {
    const userToken = await store.getState().currentUser.id;

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    const { props } = this;
    // props.navigation.navigate(userToken ? 'home' : 'login');
    props.navigation.navigate(userToken ? 'scenes' : 'login');
  };

  spring() {
    this.springValue.setValue(0.3);
    Animated.spring(this.springValue, {
      toValue: 1,
      friction: 1,
    }).start();
  }

  renderIt = async () => {
    const userToken = await store.getState().currentUser.id;
    const res = (
      <AppIntroSlider
        showSkipButton
        renderItem={this._renderItem}
        slides={slides}
        onDone={this.check}
        onSkip={this.check}
      />
    )
    return userToken ? 'scenes' : res

  }

  _renderItem = ({ item }) => {
    return (
      <View
        style={[
          welcomeStyles.slide,
          { backgroundColor: item.backgroundColor },
        ]}>
        <Header
          transparent
          androidStatusBarColor="transparent"
          iosBarStyle="light-content"
        />
        <Image source={item.image} style={welcomeStyles.image} />
        <Text style={welcomeStyles.title}>{item.title}</Text>
        <Text style={welcomeStyles.text}>{item.text}</Text>
      </View>
    );
  };



  render() {
    // const { imageLoader } = this.state;
    return (
      <AppIntroSlider
        showSkipButton
        renderItem={this._renderItem}
        slides={slides}
        onDone={this.check}
        onSkip={this.check}
      />
    )
  }
}
WelcomeScreen.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};
