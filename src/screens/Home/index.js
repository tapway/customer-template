/* eslint-disable no-undef */
import React, { Component } from 'react';
import { View } from 'react-native';
import Geocoder from 'react-native-geocoding';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styles from './style';
import Header from '../../component/HeaderBar';
import SearchLocationModal from '../../component/SearchLocationModal';
import MySwiper from '../../component/Swiper';
import { socketRiderInit } from '../../socket';
import { store } from '../../redux/configureStore';
import getImage from '../../utils/getImage';
import setDestination from '../../redux/action/destination';
import Catagory from './Catagory';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchPopup: false,
      searchLocationModalVisible: false,
      imageLoader: false,
    };
    socketRiderInit();
  }

  componentDidMount() {
    const { props } = this;
    Geocoder.init(store.getState().config.googleMapsApiKey);
    navigator.geolocation.getCurrentPosition(
      position => {
        Geocoder.from([position.coords.latitude, position.coords.longitude])
          .then(json => {
            props.editDestination(
              json.results[0].formatted_address,
              json.results[0].geometry.location
            );
          })
          .catch(err => {
            throw err;
          });
      },
      error => {
        throw error;
      }
    );
    console.log('logging home props', this.props);
  }

  handleStateChange = payload => {
    this.setState(payload);
  };

  toggleSearchLocationModal = () => {
    this.setState(prevState => {
      return {
        searchLocationModalVisible: !prevState.searchLocationModalVisible,
      };
    });
  };

  render() {
    const arrayImg = [
      getImage.food3,
      getImage.pets1,
      getImage.medicals1,
      getImage.sports1,
      getImage.electronics1,
      getImage.superMart,
    ];
    const { props, state } = this;
    return (
      <View style={styles.container}>
        <Header
          home
          toggleSearchLocationModal={this.toggleSearchLocationModal}
          location={props.destination.destinationName}
          navigation={props.navigation}
        />
        <SearchLocationModal
          changeDestination
          searchLocationModalVisible={state.searchLocationModalVisible}
          toggleSearchLocationModal={this.toggleSearchLocationModal}
          setAddress={props.editDestination}
        />
        <View style={styles.mainContainer}>
          <View style={styles.swiperContainer}>
            <MySwiper arrayImg={arrayImg} />
          </View>
          <View style={{ flex: 1 }}>
            <View style={styles.orderTypeContainer}>
              <Catagory
                press={() => {
                  props.navigation.navigate('pickupDrop', { ide: 0 });
                }}
                image={getImage.pickupDrop}
                type="Pickup/Drop"
              />
              <Catagory
                press={() => {
                  props.navigation.navigate('restaurant', { ide: 1 });
                }}
                image={getImage.mainFoodIcon}
                type="Food"
              />
            </View>
            <View style={styles.orderTypeContainer}>
              <Catagory
                press={() => {
                  props.navigation.navigate('selectOrderType', { ide: 2 });
                }}
                image={getImage.mainOrderIcon}
                type="Order"
              />
              <Catagory
                press={() => {
                  props.navigation.navigate('bookRide', { ide: 3 });
                }}
                image={getImage.bookRideIcon}
                type="Ride"
              />
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return { destination: state.destination };
};
const mapDispatchToProps = dispatch => {
  return {
    editDestination: (name, coords) => {
      dispatch(setDestination(name, coords));
    },
  };
};

Home.propTypes = {
  editDestination: PropTypes.func.isRequired,
  destination: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.array, PropTypes.string])
  ).isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
