import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#FFFFFF' },
  closeIcon: { fontSize: 25, color: '#030E1A' },
});

export default styles;
