import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ActivityIndicator, FlatList } from 'react-native';
import { Rating } from 'react-native-elements';
import { connect } from 'react-redux';
import { uniqBy } from 'lodash';
import PropTypes from 'prop-types';
import Header from '../../component/HeaderBar';
import styles from './style';
import getImage from '../../utils/getImage';
import Colors from '../../utils/colors';
import fetchNearbyShops from '../../apiServices/nearbyShops';
import Loading from './Loading';
import Empty from '../../component/EmptyList';

const fetchCount = 10;

class Restaurants extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mainLoading: false,
      moreLoading: false,
      data: [],
      noMoreShops: false,
      query: '',
    };
    this.scrollFlag = false;
  }

  componentWillMount() {
    const { props, state } = this;
    fetchNearbyShops(
      fetchCount,
      0,
      'Food',
      props.destination.destinationCoords,
      this.fillShops,
      state.query
    );
  }

  componentWillUnmount() {
    this.loadingComponent = new Promise(resolve => {
      resolve(true);
    });
  }

  loadMore = () => {
    const { props, state } = this;
    fetchNearbyShops(
      fetchCount,
      state.data.length,
      'Food',
      props.destination.destinationCoords,
      this.addMoreShops,
      state.query
    );
  };

  addMoreShops = res => {
    const { data: result } = res;
    if (result.length === 0) {
      this.setState({ noMoreShops: true });
    }
    this.setState(
      prevState => {
        return {
          data: uniqBy([...prevState.data, ...result], item => {
            return item._id;
          }),
        };
      },
      () => {
        this.setState(prevState => {
          return {
            moreLoading: !prevState.moreLoading,
          };
        });
      }
    );
  };

  fillShops = res => {
    const { data: result } = res;
    this.setState(
      {
        data: result,
      },
      () => {
        this.setState(prevState => {
          return { mainLoading: !prevState.mainLoading };
        });
      }
    );
  };

  handleQueryChange = query => {
    this.setState({ query, mainLoading: false, noMoreShops: false }, () => {
      const { state, props } = this;
      fetchNearbyShops(
        fetchCount,
        0,
        'Food',
        props.destination.destinationCoords,
        this.fillShops,
        state.query
      );
    });
  };

  _onEndReached = () => {
    const { state } = this;
    if (!state.noMoreShops && this.scrollFlag) {
      this.setState({ moreLoading: true }, () => {
        this.loadMore();
        this.scrollFlag = false;
      });
    }
  };

  _onScrollBeginDrag = () => {
    this.scrollFlag = true;
  };

  _renderItem = ({ item }) => {
    const { props } = this;
    return (
      <TouchableOpacity
        onPress={() =>
          props.navigation.navigate('chooseMenu', {
            id: item._id,
            shop: item,
          })
        }
      >
        <View style={styles.singleContainer}>
          <View style={styles.resDetails}>
            <Text style={styles.restaurant_name}>{item.name}</Text>
            <Text style={styles.address}>{item.address}</Text>
            <Rating
              imageSize={16}
              type="custom"
              readonly
              ratingColor={Colors.secondaryColor}
              startingValue={item.rating}
              style={styles.rating}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    const { state, props } = this;
    const { data } = state;
    return (
      <View style={styles.container}>
        <Header
          title="Restaurants"
          plusSearch
          back={() => props.navigation.goBack()}
          data={state.data}
          handleQueryChange={this.handleQueryChange}
        />
        <View style={styles.container}>
          <View style={styles.highlightImageContainer}>
            <Image
              resizeMode="cover"
              source={getImage.restaurantCoverImage}
              style={styles.highlightImage}
            />
          </View>
          <View style={styles.listContainer}>
            {!state.mainLoading ? (
              <Loading loadingComponent={this.loadingComponent} />
            ) : (
              <FlatList
                data={data}
                keyExtractor={item => item._id}
                onEndReached={this._onEndReached}
                ListEmptyComponent={<Empty store></Empty>}
                onEndReachedThreshold={0.5}
                onScrollBeginDrag={this._onScrollBeginDrag}
                showsVerticalScrollIndicator={false}
                ListFooterComponent={
                  !state.noMoreShops && state.moreLoading ? (
                    <ActivityIndicator size="small"></ActivityIndicator>
                  ) : null
                }
                renderItem={this._renderItem}
              />
            )}
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return { destination: state.destination };
};

Restaurants.propTypes = {
  destination: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.array]))
    .isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  null
)(Restaurants);
