/* eslint-disable react/no-array-index-key */
/* eslint-disable no-use-before-define */
import React from 'react';
import Swiper from 'react-native-swiper';
import { StyleSheet, Image } from 'react-native';
import PropTypes from 'prop-types';

const SwiperComponent = props => {
  const { arrayImg } = props;
  return (
    <Swiper showsButtons={false} autoplay>
      {arrayImg.map((item, index) => {
        return <Image resizeMode="cover" source={item} style={styles.swiperImage} key={index} />;
      })}
    </Swiper>
  );
};

SwiperComponent.propTypes = {
  arrayImg: PropTypes.arrayOf(PropTypes.number).isRequired,
};
export default SwiperComponent;
const styles = StyleSheet.create({
  swiperImage: {
    flex: 1,
    width: null,
    height: null,
  },
});
