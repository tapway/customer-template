import { StyleSheet } from 'react-native';
import { RFPercentage, RFValue } from 'react-native-responsive-fontsize';

const styles = StyleSheet.create({
  inputContainer: {
    flex: 1,
    marginVertical: 10,
    // backgroundColor: 'red',
  },
  containerBorder: {
    borderBottomColor: '#DBDBDB',
    borderBottomWidth: 1,
    paddingBottom: 5,
  },
  headtxtdiv: { position: 'absolute', left: 0, right: 0, top: RFPercentage(2.5), zIndex: -100 },
  inputLabel: {
    fontSize: 20,
    fontWeight: '100',
    color: '#858F99',
    marginVertical: 5,
    // opacity: 0.7,
  },
  inputLabel2: {
    fontSize: 16,
    fontWeight: '100',
    color: '#858F99',
    marginVertical: 2,
    // opacity: 0.7,
  },
  inputItem: {
    color: '#383838',
    borderWidth: 0,
    fontSize: 22,
    marginTop: 3,
    paddingHorizontal: 10,
    height: 45,
    fontFamily: 'Nunito-Regular',
    backgroundColor: '#EFF4F5',
    borderColor: '#EFF4F5',
  },
  inputText: {
    color: '#383838',
    // opacity: 0.9,
  },
  inputText2: {
    color: '#383838',
    fontSize: 18,
    marginVertical: 8,
  },
  packageCard: {
    padding: 5,
    elevation: 1,
  },
  selectedCard: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.9,
    shadowRadius: 4.95,

    elevation: 10,
  },
  packageIcon: {
    color: '#A98258',
    fontSize: 80,
    alignSelf: 'center',
  },
  packageSize: {
    color: '#030E1A',
    fontSize: 20,
    alignSelf: 'center',
  },
  packageWeight: {
    color: '#030E1A',
    fontSize: 18,
    alignSelf: 'center',
    opacity: 0.4,
  },
  submitBtn: {
    backgroundColor: '#3FAF5D',
    borderRadius: 4,
    marginVertical: 30,
  },
  btnText: {
    color: '#FFFFFF',
    fontSize: 18,
    fontFamily: 'Nunito-SemiBold',
    fontWeight: '400',
    letterSpacing: 0.5,
  },
});

export default styles;
