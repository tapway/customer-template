import React from 'react';
import { View, Text, Image, TouchableOpacity, FlatList } from 'react-native';
import PropTypes from 'prop-types';
import Header from '../../component/HeaderBar';
import styles from './style';
import getImage from '../../utils/getImage';

const SelectOrderType = props => {
  const navigate = (item, index) => {
    if (index === 0) {
      props.navigation.navigate('restaurant');
    } else {
      props.navigation.navigate('selectShop', {
        id: index,
        shopType: item.name,
      });
    }
  };
  const array = [
    {
      name: 'Foods',
      image: getImage.foods,
    },
    {
      name: 'Electronincs',
      image: getImage.electronics,
    },
    {
      name: 'Sports',
      image: getImage.sports,
    },
    {
      name: 'Groceries',
      image: getImage.groceries,
    },
    {
      name: 'Medicals',
      image: getImage.medicals,
    },
    {
      name: 'Pet Supplies',
      image: getImage.petSupplies,
    },
  ];
  return (
    <View style={styles.mainContainer}>
      <Header title="Orders" />

      <FlatList
        data={array}
        showsVerticalScrollIndicator={false}
        keyExtractor={item => {
          return item.name.toString();
        }}
        contentContainerStyle={styles.containerList}
        renderItem={({ item, index }) => (
          <TouchableOpacity
            key={index}
            onPress={() => {
              navigate(item, index);
            }}
            style={index % 2 !== 0 ? [styles.card, styles.backOrange] : [styles.card]}
          >
            <Image resizeMode="contain" source={item.image} style={styles.img} />
            <View style={styles.textView}>
              <Text style={styles.itemNameTxt}>{item.name}</Text>
            </View>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

SelectOrderType.propTypes = { navigation: PropTypes.objectOf(PropTypes.any).isRequired };

export default SelectOrderType;
