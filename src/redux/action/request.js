import { SET_REQUEST } from '../actionTypes';

/**
 * Set Request To Redux
 */
const setRequest = (shop, cart, id, loc, add) => {
  return {
    type: SET_REQUEST,
    payload: {
      userId: id,
      category: shop.shopType,
      deliveryInstructions: {
        detailAddress: 'Room No:- 40',
        detailAddress2: 'First Floor',
        itemDetails: cart,
      },
      processingStatus: 'PENDING',
      paymentMode: '',
      paymentAmount: '',
      endUserGpsLocation: add,
      //   deliveryLocation: loc,
      pickupLocation: { latitude: shop.latitude, longitude: shop.longitude },
      deliveryAddress: add,
      pickupAddress: shop.Address,
    },
  };
};
export default setRequest;
