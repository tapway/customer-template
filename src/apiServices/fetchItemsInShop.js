/* eslint-disable no-undef */
import HelperFetch from './HelperFetch';

/**
 * Fetch items with the help of shops
 * @returns {Response:true,data:Array of items}
 */
const handleFailure = () => {};
const fetchItemsByShop = async (limit = 10, skip = 0, itemType, shopId, handleSuccess, query) => {
  const extension = `/dashboard/items/getAllByShopWithQuery`;
  await HelperFetch(
    {
      extension,
      post: true,
      body: {
        shopId,
        itemType,
        limit,
        skip,
        query,
      },
    },
    handleSuccess,
    handleFailure
  );
};

const fetchItemsByShopWithFilters = async (
  limit = 10,
  skip = 0,
  itemType,
  shopId,
  handleSuccess,
  query,
  filters
) => {
  const extension = `/dashboard/items/getAllByShopWithFilters`;
  await HelperFetch(
    {
      extension,
      post: true,
      body: {
        shopId,
        itemType,
        limit,
        skip,
        query,
        filters,
      },
    },
    handleSuccess,
    handleFailure
  );
};

export { fetchItemsByShop, fetchItemsByShopWithFilters };
