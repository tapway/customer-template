import React, { Component } from "react";
import { Container, Header, Content, Card, CardItem, Text, Body, Left, Button, Icon, Right } from "native-base";
import styles from './styles';

export default class CardItemBordered extends Component {
    constructor(props) {
        super(props);

    }
    render() {
        const { navigation } = this.props;
        return (
            <Container>
                <Header transparent androidStatusBarColor="#0003">
                    <Left style={{ paddingHorizontal: 5 }}>
                        <Button transparent onPress={() => navigation.goBack()}>
                            <Icon name="close" style={styles.closeIcon} />
                        </Button>
                    </Left>
                    <Body style={styles.headtxtdiv}>
                        <Text
                            style={{
                                fontFamily: 'Nunito-SemiBold',
                                fontSize: 18,
                                color: '#030E1A',
                                width: '100%',
                                textAlign: 'center',
                            }}>
                            Terms Of Service
                </Text>
                    </Body>
                    <Right />
                </Header>
                <Content padder>
                    <Card>
                        <CardItem header >
                            <Text>SERVICES</Text>
                        </CardItem>
                        <CardItem >
                            <Body>
                                <Text>
                                    At Tapway Logistics, we introduce a fast, convenient, and economical solution for the SMEs to achieve their last mile or same-day delivery goals.
                                    {'\n'}{'\n'}
                                    Our services offer a quicker yet simpler way for businesses to meet customer satisfaction while creating commercial employment opportunities for available, flexible drivers.
                                     {'\n'}{'\n'}
                                    Discover just how your business can benefit from our transformative and efficient technologies.
                </Text>
                            </Body>
                        </CardItem>

                    </Card>

                    <Card>
                        {/* <CardItem header >
                            <Text>We Make Same-Day Deliveries Easier, Affordable and Effective</Text>
                        </CardItem> */}
                        <CardItem header >
                            <Text>Crowdsourcing Logistics for Individuals, Merchants, and Businesses</Text>
                        </CardItem>
                        <CardItem >
                            <Body>
                                <Text>
                                    Our platform is designed to offer the innovation of crowdsourcing logistics for individuals, merchants, and businesses.
                                    {'\n'}{'\n'}
                                    Crowdsourcing logistics is a relatively new concept; however, its robust solutions are set to transform deliveries and business capabilities as we know it.
                                    {'\n'}{'\n'}

                                    With reliance on our crowdsourcing logistics, we can improve last mile deliveries and significantly enhance a business's shipping or logistics strategy.
                                    {'\n'}{'\n'}
                                    When customer demands are on the rise, and dedicated fleet deliveries are few and far between, connect to our crowdsourcing logistics using our application.
                                    {'\n'}{'\n'}
                                    Here, customers, merchants, and businesses can connect to a delivery provider who is available to perform last mile or same-day deliveries.
                                    {'\n'}{'\n'}
                                    For business, our crowdsourcing helps you stay on top of sales and meet the needs of your customers. It creates a positive business image of reliability and addresses the gap in logistics services.

                </Text>
                            </Body>
                        </CardItem>

                    </Card>

                    <Card>
                        <CardItem header >
                            <Text>The API for the SME</Text>
                        </CardItem>

                        <CardItem >
                            <Body>
                                <Text>
                                    Our platform offers an API for SMEs to integrate our services into your very own E-Commerce platform.
                                    {'\n'}{'\n'}The purpose is to enable businesses to provide same-day delivery. The API introduces a simple yet effective way to connect SME's or private customers to a delivery service for same-day delivery results.
                                    {'\n'}{'\n'}
                                    Our API is designed to accept a request from a retailer or a private customer to collect a product or parcel for same-day delivery.
                                    {'\n'}{'\n'}
                                    All persons and entities part of our E-commerce platform will have access to our services, and relevant parties notified as soon as a request is made.
                                    {'\n'}{'\n'}
                                    We make it simpler, more comfortable, and efficient to find a solution to last mile problems in the supply chain.
                                    {'\n'}{'\n'}
                                    Our contemporary application also creates cost-effectiveness for those in the supply chain industry, both locally and internationally.
                                    {'\n'}{'\n'}
                                    For individual customers, small businesses, and medium enterprises, less reliance on the cost of a dedicated fleet, and the option of crowdsourcing logistics helps address growing customer demand while managing transportation costs.


                </Text>
                            </Body>
                        </CardItem>

                    </Card>

                    <Card>
                        <CardItem header >
                            <Text>Job Opportunities for Delivery Services</Text>
                        </CardItem>

                        <CardItem >
                            <Body>
                                <Text>
                                    Individuals seeking an additional income stream can apply to become delivery personnel on the platform, choose the hours they work, and generate an additional income stream.
                                    {'\n'}{'\n'}
                                    We have created an exceptional opportunity for both businesses and individuals to connect in a single secure platform in which all parties can benefit from the latest E-commerce solutions.


                </Text>
                            </Body>
                        </CardItem>

                    </Card>
                </Content>
            </Container>
        );
    }
}