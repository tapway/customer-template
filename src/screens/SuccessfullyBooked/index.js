import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { View, Image, TouchableOpacity, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types';
import { heightRatio } from '../../utils/styleSheetGuide';
import Header from '../../component/HeaderBar';
import styles from './style';
import Loading from './Loading';
import { fetchTrackDetails } from '../../socket';
import getImage from '../../utils/getImage';
import Map from './MapView';
import BottomCard from './BottomCard';
import TopCard from './TopCard';
import Colors from '../../utils/colors';
import { DUMP_TRIP_INFO } from '../../redux/actionTypes';

class SuccessfullyBooked extends PureComponent {
  loadingComponent: Promise<React.Element<*>>;

  constructor(props) {
    super(props);
    this.state = {
      imageLoader: false,

      loaded: true,
      modalVisible: true,
      toggleAnimation: false,
      orderStatus: -1,

      orderId: props.navigation.getParam('id') ? props.navigation.getParam('id') : '',
      orderType: props.navigation.getParam('type') ? props.navigation.getParam('type') : '',
    };
    this.animatedValue1 = new Animated.Value(0);
    this.animatedValue2 = new Animated.Value(0);
  }

  componentWillMount() {
    const { state } = this;
    if (state.orderId !== '') {
      fetchTrackDetails({ id: state.orderId });
    }
  }

  componentWillReceiveProps(nextProps) {
    // const { props } = this;
    const { processingStatus } = nextProps.tripAccepted;

    const status =
      processingStatus === 'Pending'
        ? -1
        : processingStatus === 'Accepted'
        ? 0
        : processingStatus === 'Picked'
        ? 1
        : processingStatus === 'Completed'
        ? 2
        : 0;
    this.setState({ orderStatus: status });
  }

  componentWillUnmount() {
    const { props } = this;
    this.loadingComponent = new Promise(resolve => {
      resolve(true);
    });
    props.clearTripInfoDispatcher();
  }

  backHandler = () => {
    const { props } = this;
    this.setState({ modalVisible: false }, () => {
      const { state } = this;
      if (state.orderId !== '') {
        props.navigation.goBack();
      } else {
        props.navigation.navigate('home');
      }
    });
  };

  setToggleAnimation = () => {
    this.setState(prevState => {
      return { toggleAnimation: !prevState.toggleAnimation };
    });
  };

  animate = () => {
    const { toggleAnimation } = this.state;
    let initValue = 0;
    let finalValue = 1;
    if (toggleAnimation) {
      initValue = 1;
      finalValue = 0;
    }
    this.animatedValue1.setValue(initValue);
    this.animatedValue2.setValue(initValue);
    // eslint-disable-next-line func-names
    const createAnimation = function(value, duration, easing, delay = 0) {
      return Animated.timing(value, {
        toValue: finalValue,
        duration,
        easing,
        delay,
      });
    };
    if (toggleAnimation) {
      Animated.parallel([
        createAnimation(this.animatedValue1, 500, Easing.ease, 500),
        createAnimation(this.animatedValue2, 500, Easing.ease),
      ]).start();
      this.setToggleAnimation();
    } else {
      Animated.parallel([
        createAnimation(this.animatedValue1, 500, Easing.ease),
        createAnimation(this.animatedValue2, 500, Easing.ease, 500),
      ]).start(() => {
        this.setToggleAnimation();
      });
    }
  };

  render() {
    const { props, state } = this;
    const { tripAccepted } = props;
    const { orderStatus, orderType } = state;
    const opacity = this.animatedValue1.interpolate({
      outputRange: [1, 0.5, 0],
      inputRange: [0, 0.5, 1],
    });
    const additionalTop = orderType === 'Cab' ? 80 * heightRatio : 0;
    const top = this.animatedValue2.interpolate({
      inputRange: [0, 1],
      outputRange: [heightRatio * 320 + additionalTop, heightRatio * 580],
    });
    return (
      <View style={styles.container}>
        <Map tripAccepted={tripAccepted} orderStatus={orderStatus}></Map>
        <Header
          onlyBack
          style={{ position: 'absolute', backgroundColor: Colors.transparent }}
          back={
            state.orderId === ''
              ? () => {
                  props.navigation.navigate('home');
                }
              : () => {
                  props.navigation.goBack();
                }
          }
        />
        <Animated.View style={[styles.navigateContainer, { top }]}>
          <TouchableOpacity onPress={this.animate}>
            <View style={styles.navigateIconContainer}>
              <Image
                source={getImage.navigation}
                style={{ height: 16 * heightRatio, width: 16 * heightRatio }}
              ></Image>
            </View>
          </TouchableOpacity>
        </Animated.View>
        <View style={styles.modal}>
          {!state.toggleAnimation && (
            <Animated.View style={[styles.card, { opacity }]}>
              {tripAccepted._id ? (
                <TopCard tripAccepted={tripAccepted}></TopCard>
              ) : (
                <Loading loadingComponent={this.loadingComponent} />
              )}
              <BottomCard
                orderStatus={orderStatus}
                orderType={orderType}
                tripAccepted={tripAccepted}
              ></BottomCard>
            </Animated.View>
          )}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    tripAccepted: state.tripAccepted,
  };
};
function mapDispatchToProps(dispatch) {
  return {
    clearTripInfoDispatcher: () => {
      dispatch({ type: DUMP_TRIP_INFO });
    },
  };
}

SuccessfullyBooked.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  tripAccepted: PropTypes.objectOf(PropTypes.any).isRequired,
  clearTripInfoDispatcher: PropTypes.func.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SuccessfullyBooked);
