import React, { Component } from 'react';
import _ from 'lodash';
import { View, ActivityIndicator, FlatList } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styles from './style';
import Header from '../../component/HeaderBar';
import MySwiper from '../../component/Swiper';
import checkImage from './checkImage';
import fetchNearbyShops from '../../apiServices/nearbyShops';
import Loading from './Loading';
import Empty from '../../component/EmptyList';

import ShopRenderItem from './ShopRenderItem';

let catagory = '';
const fetchCount = 10;
class SelectShop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arrayData: [],
      loaded: false,
      moreLoading: false,
      noMoreShops: false,
      query: '',
    };
    this.scrollFlag = false;
  }

  componentDidMount() {
    const { props, state } = this;
    const id = props.navigation.getParam('id');
    catagory = this.checkCatagory(id);
    fetchNearbyShops(
      fetchCount,
      0,
      catagory,
      props.destination.destinationCoords,
      this.fillShops,
      state.query
    );
  }

  componentWillUnmount() {
    this.loadingComponent = new Promise(resolve => {
      resolve(true);
    });
  }

  checkCatagory = id => {
    switch (id) {
      case 1:
        return 'Electronics';
      case 2:
        return 'Sports';
      case 3:
        return 'Groceries';
      case 4:
        return 'Medical';
      case 5:
        return 'Pets';
      default:
        return 'none';
    }
  };

  fillShops = res => {
    const { data: result } = res;
    this.setState(
      {
        arrayData: result,
      },
      () => {
        this.setState(prevState => {
          return { loaded: !prevState.loaded };
        });
      }
    );
  };

  loadMore = () => {
    const { props, state } = this;
    fetchNearbyShops(
      fetchCount,
      state.arrayData.length,
      catagory,
      props.destination.destinationCoords,
      this.addMoreShops,
      state.query
    );
  };

  addMoreShops = res => {
    const { data: result } = res;
    if (result.length === 0) {
      this.setState({ noMoreShops: true });
    }

    this.setState(
      prevState => {
        return {
          arrayData: _.uniqBy([...prevState.arrayData, ...result], item => {
            return item._id;
          }),
        };
      },
      () => {
        this.setState(prevState => {
          return {
            moreLoading: !prevState.moreLoading,
          };
        });
      }
    );
  };

  handleQueryChange = query => {
    this.setState(
      {
        query,
        loaded: false,
        noMoreShops: false,
      },
      () => {
        const { state, props } = this;
        fetchNearbyShops(
          fetchCount,
          0,
          catagory,
          props.destination.destinationCoords,
          this.fillShops,
          state.query
        );
      }
    );
  };

  render() {
    const { state, props } = this;
    const shopType = props.navigation.getParam('shopType');
    return (
      <View style={styles.fullFlex}>
        <Header
          title={shopType}
          plusSearch
          back={() => props.navigation.goBack()}
          data={state.arrayData}
          handleQueryChange={this.handleQueryChange}
        />
        <View style={styles.fullFlex}>
          <View style={styles.swiper}>
            <MySwiper arrayImg={checkImage(shopType)} />
          </View>
          <View style={styles.mainContainer}>
            {state.loaded === false ? (
              <Loading loadingComponent={this.loadingComponent} />
            ) : (
              <FlatList
                data={state.arrayData}
                keyExtractor={shop => {
                  return shop._id;
                }}
                showsVerticalScrollIndicator={false}
                onScrollBeginDrag={() => {
                  this.scrollFlag = true;
                }}
                onEndReached={() => {
                  if (!state.noMoreShops && this.scrollFlag) {
                    this.setState({ moreLoading: true }, () => {
                      this.loadMore();
                      this.scrollFlag = false;
                    });
                  }
                }}
                onEndReachedThreshold={0.5}
                ListEmptyComponent={<Empty store></Empty>}
                ListFooterComponent={
                  !state.noMoreShops && state.moreLoading ? (
                    <ActivityIndicator size="small"></ActivityIndicator>
                  ) : null
                }
                renderItem={({ item }) => {
                  return (
                    <ShopRenderItem item={item} navigation={props.navigation}></ShopRenderItem>
                  );
                }}
              />
            )}
          </View>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return { destination: state.destination };
};

SelectShop.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  destination: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.array, PropTypes.string]))
    .isRequired,
};

export default connect(
  mapStateToProps,
  null
)(SelectShop);
