/* eslint-disable react/no-unused-state */
/* eslint-disable no-return-assign */
import React, { PureComponent } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { CardItem, Body } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import HeaderBar from '../../component/HeaderBar';
import styles from './style';
import { addAddress, checkLocation } from '../../redux/action/savedAddresses';
import { store } from '../../redux/configureStore';
import SearchLocationModal from '../../component/SearchLocationModal';
import AddressModal from '../../component/AddressModal';
import Colors from '../../utils/colors';
import addNewAddressDb from '../../apiServices/savedAddresses';
import Button from '../../component/Button';

class SavedAddress extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      searchLocationModalVisible: false,
      addressModal: false,
      locationName: '',
      location: {},
      work: false,
      home: false,
      others: false,
    };
  }

  addNewAddress = (name, location) => {
    this.setState({ location, locationName: name, searchLocationModalVisible: false }, () => {
      this.setState({ addressModal: true });
    });
  };

  addNew = type => {
    const { props, state } = this;
    this.setState({ addressModal: false });
    addNewAddressDb(
      {
        type,
        locationName: state.locationName,
        location: state.location,
      },
      props.user.jwt,
      this.handleResult
    );
  };

  handleResult = newAddress => {
    const { props } = this;
    props.addAddressDispatcher(newAddress);
  };

  selectMethod = input => {
    this.setState({ work: false, others: false, home: false, [input]: true });
  };

  setAddressModal = () => {
    const { addressModal } = this.state;
    this.setState({ addressModal: !addressModal });
  };

  toggleSearchLocationModal = () => {
    const { searchLocationModalVisible } = this.state;
    this.setState({ searchLocationModalVisible: !searchLocationModalVisible });
  };

  checkType = address => {
    switch (address.type.toLowerCase()) {
      case 'home':
        return <Icon name="home" size={25} color={Colors.primaryColor} style={styles.icon} />;
      case 'work':
      case 'office':
        return <Icon name="briefcase" size={23} color={Colors.primaryColor} style={styles.icon} />;
      default:
        return (
          <Icon name="location-arrow" size={25} color={Colors.primaryColor} style={styles.icon} />
        );
    }
  };

  render() {
    const { props, state } = this;
    return (
      <View style={styles.container}>
        <HeaderBar plusTitle title="Addresses" back={() => props.navigation.goBack()} />
        <SearchLocationModal
          addAddress
          addNewAddress={this.addNewAddress}
          searchLocationModalVisible={state.searchLocationModalVisible}
          toggleSearchLocationModal={this.toggleSearchLocationModal}
        />
        <AddressModal
          selectMethod={this.selectMethod}
          addressModalVisible={state.addressModal}
          add={this.addNew}
          setAddressModal={this.setAddressModal}
        />
        <View style={styles.container}>
          <ScrollView style={{ paddingHorizontal: 8 }} showsVerticalScrollIndicator={false}>
            {store.getState().currentUser.savedAddresses.map(address => {
              return (
                <View style={styles.card} key={address.type + address.locationName}>
                  <CardItem>
                    {this.checkType(address)}
                    <Body>
                      <View style={styles.contLoc}>
                        <Text style={styles.typeText}>{address.type}</Text>
                        <Text style={styles.locationNameText}>{address.locationName}</Text>
                      </View>
                    </Body>
                  </CardItem>
                </View>
              );
            })}
          </ScrollView>
          <View style={styles.btnContainer}>
            <Button
              title="Add New"
              submit={() => {
                this.setState({ searchLocationModalVisible: true });
              }}
            ></Button>
          </View>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.currentUser,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    addAddressDispatcher: userAddress => {
      dispatch(addAddress(userAddress));
    },
    checkLocationDispatcher: index => {
      dispatch(checkLocation(index));
    },
  };
};

SavedAddress.propTypes = {
  user: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.array, PropTypes.bool])
  ).isRequired,
  addAddressDispatcher: PropTypes.func.isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SavedAddress);
