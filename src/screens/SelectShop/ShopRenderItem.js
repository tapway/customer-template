import React, { PureComponent } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import styles from './style';
import getImage from '../../utils/getImage';

export default class ShopRenderItem extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      openStatus: true,
    };
  }

  componentWillMount() {
    const { item } = this.props;
    const currentHr = new Date().getHours();
    const currentMin = new Date().getMinutes();
    const { hr: shopOpeningHr, min: shopOpeningMin } = item.openingTime;
    const { hr: shopClosingHr, min: shopClosingMin } = item.closingTime;
    if (currentHr > shopOpeningHr && currentHr < shopClosingHr) {
      return 'Open';
    }
    if (currentHr === shopOpeningHr && currentMin > shopOpeningMin) {
      return 'Open';
    }
    if (currentHr === shopClosingHr && currentMin < shopClosingMin) {
      return 'Open';
    }
    this.setState({ openStatus: false });
    return 'Closed';
  }

  render() {
    const { props, state } = this;
    const { item } = props;
    return (
      <TouchableOpacity
        onPress={() =>
          props.navigation.navigate('chooseProduct', {
            shopDetails: item,
          })
        }
      >
        <View style={styles.itemContainer}>
          <View style={styles.nameContainer}>
            <Text style={styles.nameText}>{item.name}</Text>
          </View>
          <View style={styles.addressContainer}>
            <Text style={styles.addressText}>{item.address}</Text>
          </View>
          <View style={styles.timeContainer}>
            <Image resizeMode="contain" source={getImage.deliveryTime} style={styles.timeImage} />
            <View style={styles.timeTextContainer}>
              <Text style={styles.timeText}>{`${state.openStatus ? 'Open' : 'Closed'}, Timings : ${
                item.openingTime.hr
              } AM - ${item.closingTime.hr - 12} PM`}</Text>
            </View>
          </View>
          <View style={styles.deliveryTypeContainer}>
            <Image
              resizeMode="contain"
              source={getImage.deliveryTruck}
              style={styles.deliveryImg}
            />
            <View style={styles.deliveryTextContainer}>
              <Text style={styles.deliveryText}>Free Delivery</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

ShopRenderItem.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string,
    openingTime: PropTypes.objectOf(PropTypes.number),
    closingTime: PropTypes.objectOf(PropTypes.number),
    address: PropTypes.string,
  }).isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};
