import { FETCH_ORDER_HISTORY, CLEAR_ORDER_HISTORY } from '../actionTypes/index';

export default function(state = [], action) {
  switch (action.type) {
    case FETCH_ORDER_HISTORY: {
      return [...action.payload];
    }
    case CLEAR_ORDER_HISTORY: {
      return [];
    }
    default: {
      return state;
    }
  }
}
