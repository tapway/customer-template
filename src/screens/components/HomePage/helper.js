import { getDistance } from 'geolib';
import distance from 'geo-dist';
import { store } from '../../../redux/configureStore';

const getInitialState = () => {
    return {
        searchLocationModalVisible: false,
        getDetailsModalVisible: false,
        confirmOrderModalVisible: false,
        tripEstimate: false,
        deliveryType:'bike',
        itemDetails: [],
        detailAddress: '',
        detailAddress2: '',

        origin: 'Search here...',
        destination: 'Search here...',
        data: {},
        details: {},


        isPackageDetailsVisible: false,
        fromLocation: '',
        fromLocationSet: false,
        toLocation: '',
        toLocationSet: false,
        predictions: [],
        currentInput: '',
        region: {
            latitude: 6.599033,
            longitude: 3.3411348,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },
        id: 0,
        marker: [
            {
                title: 'pickup',
                coordinates: {
                    latitude: 0,
                    longitude: 0,
                },
            },
            {
                title: 'drop',
                coordinates: {
                    latitude: 0,
                    longitude: 0,
                },
            },
        ],
    };
};

const handleSetAddress = (name, coords, marker, id, handleStateChange) => {
    const mark = marker;
    mark[id].coordinates = {
        latitude: coords.lat,
        longitude: coords.lng,
    };

    let dist = getDistance(marker[0].coordinates, {
        latitude: coords.lat,
        longitude: coords.lng,
    });
    dist = (dist * 1.6) / 1000;
    let delta = 0;

    if (id === 0) {
        delta = 0.05;
        handleStateChange({
            origin: name,
            marker: mark,
            id: 1,
        });
    }

    if (id === 1) {
        delta = dist * 0.018;
        handleStateChange({
            destination: name,
            marker: mark,
            id: 2,
        });
    }
    handleStateChange({
        region: {
            latitude: coords.lat,
            longitude: coords.lng,
            latitudeDelta: delta,
            longitudeDelta: delta,
        },
    });
};

const getFutureOrderObject = (props, state) => {
    const { detailAddress, detailAddress2, fromLocation, toLocation, itemDetails, fromLocation_lat, fromLocation_lng, toLocation_lat, toLocation_lng, dist, est, time } = props;
    const { itemQuantity, addInfo, packageSize, personInCharge, contactNumber } = state
    return {
        userId: store.getState().currentUser.id,
        paymentMode: 'Card',
        paymentAmount: est,
        endUserGpsLocation: { fromLocation_lat, fromLocation_lng },
        deliveryLocation: [toLocation_lng, toLocation_lat],
        deliveryInstructions: {
            fromLocation,
            toLocation,
            itemDetails,
            dist,
            est,
            time,
            itemQuantity,
            packageSize,
            addInfo,
            personInCharge,
            contactNumber
        },
        pickUpLocation: [fromLocation_lng, fromLocation_lat],
        deliveryAddress: toLocation,
        pickUpAddress: fromLocation,
        category: 'Pickup',
    };
}

const getRegisterOrderObject = (props, state) => {

    const { detailAddress, detailAddress2, fromLocation, toLocation, itemDetails, fromLocation_lat, fromLocation_lng, toLocation_lat, toLocation_lng, dist, est, time } = props;
    const { itemQuantity, addInfo, packageSize, personInCharge, contactNumber, chosenDate } = state
    return {
        userId: store.getState().currentUser.id,
        paymentMode: 'Card',
        paymentAmount: est,
        endUserGpsLocation: { fromLocation_lat, fromLocation_lng },
        deliveryLocation: [toLocation_lng, toLocation_lat],
        deliveryInstructions: {
            fromLocation,
            toLocation,
            itemDetails,
            dist,
            est,
            time,
            itemQuantity,
            packageSize,
            addInfo,
            personInCharge,
            contactNumber
        },
        deliveryDate: chosenDate,
        pickUpLocation: [fromLocation_lng, fromLocation_lat],
        deliveryAddress: toLocation,
        pickUpAddress: fromLocation,
        category: 'Pickup',
    };
};

const getDist = (fromLocation_lat, fromLocation_lng, toLocation_lat, toLocation_lng) => {
    const dist = parseFloat(distance(fromLocation_lat, fromLocation_lng, toLocation_lat, toLocation_lng)).toFixed(2)
    return parseFloat(dist)
}

const getTimeEstimate = (fromLocation_lat, fromLocation_lng, toLocation_lat, toLocation_lng) => {
    const dist = parseFloat(distance(fromLocation_lat, fromLocation_lng, toLocation_lat, toLocation_lng)).toFixed(2)
    return dist * 5
}

const getAmountEstimate = (fromLocation_lat, fromLocation_lng, toLocation_lat, toLocation_lng) => {
    const dist = parseFloat(distance(fromLocation_lat, fromLocation_lng, toLocation_lat, toLocation_lng)).toFixed(2)
    return dist * 300
}




export { getInitialState, handleSetAddress, getRegisterOrderObject, getDist, getTimeEstimate, getAmountEstimate, getFutureOrderObject };
