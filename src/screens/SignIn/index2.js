/* eslint-disable react/no-unused-state */
import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  Modal,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import { GoogleSignin } from 'react-native-google-signin';
import { connect } from 'react-redux';
import Toast from 'react-native-easy-toast';
import PhoneInput from 'react-native-phone-input';
import { get } from 'lodash';
import PropTypes from 'prop-types';
import styles from './style';
import requestFbLogin from './FbSignUp';
import Loader from '../../component/Loader';
import signInUserDb from '../../apiServices/signIn';
import getImage from '../../utils/getImage';
import Header from '../../component/HeaderBar';
// import { widthRatio } from '../../utils/styleSheetGuide';
import Colors from '../../utils/colors';

class SignIn extends Component {
  constructor() {
    super();
    this.state = {
      phoneNumber: '+234',
      nameError: '',
      userInfo: null,
      imageLoader: false,
      phoneNumberModal: false,
    };
    this.phone = null;
  }

  componentDidMount() {
    const { props } = this;
    GoogleSignin.configure({
      scopes: ['https://www.googleapis.com/auth/drive.readonly'],
      webClientId:
        '949253402923-a6qobffd1kop5m58aj4t5cus9mrguo7c.apps.googleusercontent.com',
      iosClientId: get(
        props,
        'config.googleAuth.GOOGLE_IOS_CLIENT_ID',
        '949253402923-a6qobffd1kop5m58aj4t5cus9mrguo7c.apps.googleusercontent.com'
      ),
    });
  }

  signup = userInfo => {
    this.setState({ userInfo });
    const { props } = this;
    props.navigation.navigate('signUp', { userInfo });
  };

  signIn = async () => {
    const { refs } = this;
    refs.toast.show('Temporarily Not Available');
    // try {
    //   await GoogleSignin.hasPlayServices();
    //   const userInfo = await GoogleSignin.signIn();
    //   this.signup(userInfo.user);
    // } catch (error) {
    //   if (error.code === statusCodes.SIGN_IN_CANCELLED) {
    //     // user cancelled the login flow
    //   } else if (error.code === statusCodes.IN_PROGRESS) {
    //     // operation (f.e. sign in) is in progress already
    //   } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
    //     // play services not available or outdated
    //   } else {
    //     // some other error happened
    //   }
    // }
  };

  signUpUser = phoneNumber => {
    const { refs } = this;
    if (this.phone.isValidNumber()) {
      this.setState({ phoneNumberModal: false, imageLoader: true });
      signInUserDb(phoneNumber, this.handleResult, this.handleNotFound);
    } else {
      refs.toast.show('Phone number invalid');
    }
  };

  handleNotFound = () => {
    const { refs } = this;
    this.setState({ imageLoader: false });
    refs.toast.show('Phone number not found');
  };

  handleResult = () => {
    const { props, state } = this;
    this.setState({ imageLoader: false });
    props.navigation.navigate('verify', { phoneNumber: state.phoneNumber });
  };

  fbSignUp = async () => {
    await requestFbLogin(this.signup);
  };

  render() {
    const { phoneNumber, imageLoader, phoneNumberModal } = this.state;
    return (
      <React.Fragment>
        <Modal
          visible={phoneNumberModal}
          animated
          animationType="slide"
          onRequestClose={() => {
            this.setState({ phoneNumberModal: false });
          }}>
          <Loader loading={imageLoader} />
          <Header
            onlyBack
            back={() => this.setState({ phoneNumberModal: false })}></Header>

          <TouchableWithoutFeedback
            onPress={() => {
              Keyboard.dismiss();
            }}
            accessible={false}>
            <KeyboardAvoidingView
              style={styles.modalInputContainer}
              behavior="padding"
              enabled={Platform.OS === 'ios'}>
              <View style={{ flex: 1 }}>
                <Text style={styles.inputHeading}>Please enter your</Text>
                <Text style={[styles.inputHeading, { marginTop: 4 }]}>
                  mobile number to proceed
                </Text>
                <PhoneInput
                  ref={ref => {
                    this.phone = ref;
                  }}
                  value={phoneNumber}
                  onChangePhoneNumber={text => {
                    this.setState({
                      phoneNumber: text,
                    });
                  }}
                  textStyle={{ fontSize: 18 }}
                  textProps={{
                    placeholder: 'Enter Phone Number',
                    autoFocus: true,
                  }}
                  style={styles.phoneInput}
                />
              </View>
              <View style={styles.loginBtnContainer}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => {
                    this.signUpUser(phoneNumber);
                  }}>
                  <Text style={styles.loginText}>Login</Text>
                </TouchableOpacity>
              </View>
            </KeyboardAvoidingView>
          </TouchableWithoutFeedback>
        </Modal>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <View style={styles.main}>
            <View style={styles.mainContainer}>
              <View style={styles.headingContainer}>
                <Text style={styles.headingTxt}>Welcome to</Text>
                <Text style={styles.subHeadingTxt}>Tapway !!!</Text>
              </View>
              <Image
                source={getImage.signUpImage}
                resizeMode="contain"
                style={styles.signUpImage}
              />
              <View style={styles.inputContainer}>
                <Text style={styles.inputHeading}>Please enter your</Text>
                <Text style={[styles.inputHeading, { marginTop: 4 }]}>
                  mobile number to proceed
                </Text>
                <View style={styles.gap} />
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ phoneNumberModal: true });
                  }}
                  style={[styles.textInputBox, styles.nofocus]}>
                  <Image
                    source={getImage.ng}
                    resizeMode="contain"
                    style={styles.flagImg}></Image>
                  <Text style={{ fontSize: 20 }}>+234</Text>
                </TouchableOpacity>
              </View>
              {/* <View style={styles.signUpContainer}>
                <Text style={styles.signUpText}>Sign Up with</Text>
                <View style={styles.googleFbBtns}>
                  <TouchableOpacity
                    onPress={this.signIn}
                    style={{ marginLeft: 15 * widthRatio }}>
                    <Image
                      resizeMode="contain"
                      style={styles.img}
                      source={getImage.google}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={this.fbSignUp}
                    style={{ marginLeft: 10 * widthRatio }}>
                    <Image
                      resizeMode="contain"
                      style={styles.img}
                      source={getImage.fb}
                    />
                  </TouchableOpacity>
                </View>
              </View> */}
            </View>
            <Toast
              ref="toast"
              style={{ backgroundColor: Colors.toastBgColor }}
              position="bottom"
              positionValue={100}
              fadeInDuration={750}
              fadeOutDuration={1000}
              opacity={0.8}
              textStyle={{ color: Colors.toastTextColor, fontSize: 15 }}
            />
          </View>
        </TouchableWithoutFeedback>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    config: state.config,
  };
};

SignIn.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  null
)(SignIn);
