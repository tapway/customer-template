import io from 'socket.io-client';
import { Alert } from 'react-native';
import { store } from '../redux/configureStore';
import setTripInfo from '../redux/action/tripAccepted';
import NavigationService from '../navigation/navigationService';
import SERVER from '../config/config';

let socket = null

export function socketRiderInit() {
  socket = io(SERVER, {
    jsonp: false,
    transports: ['websocket'],
    query: `token=${store.getState().currentUser.jwt}&&id=${
      store.getState().currentUser.id
      }&&type=user`,
  });

  socket.heartbeatTimeout = 10000; // reconnect if not received heartbeat for 17 seconds

  socket.on('connect', () => { });
  socket.on('disconnect', () => {
    socket.on('connect', () => { });
  });
  socket.on('tripAccepted', data => {
    store.dispatch(setTripInfo(data));
  });
  socket.on('trackDetailsResult', data => {
    store.dispatch(setTripInfo(data));
  });
  socket.on('OrderCompleted', data => {
    NavigationService.navigate('ratingPage', { trip: data });
  });
  socket.on('UpdateOrderStatus', data => {
    store.dispatch(setTripInfo({ processingStatus: data }));
  });
  socket.on('TripUpdates', data => {
    if (store.getState().tripAccepted._id === data._id) {
      store.dispatch(setTripInfo(data));
    }
  });
  socket.on('noNearbyDriver', () => {
    Alert.alert(
      'No Nearby Driver Found',
      'Their is no driver available in your area. Please try after some time',
      [{ text: 'OK', onPress: () => NavigationService.navigate('scenes') }]
    );
  });
}
export function deliveryRatingUpdate(payload) {
  socket.emit('DeliveryUserRating', payload);
}
export function requestTrip(payload) {
  socket.emit('requestTrip', payload);
}

export function test(payload) {
  socket.emit('requestTrip', payload);
}

export function futureTrip(payload) {
  socket.emit('futureTrip', payload);
}

export function cancelRideByRider(tripRequest) {
  socket.emit('tripRequestUpdate', tripRequest);
}
export function updateLocation(user) {
  socket.emit('updateLocation', user);
}
export function checkPayment(payload) {
  socket.emit('capturePayment', payload);
}
export function fetchTrackDetails(payload) {
  socket.emit('fetchTrackDetails', payload);
}
