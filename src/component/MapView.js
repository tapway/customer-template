/* eslint-disable no-use-before-define */
import React from 'react';
import MapView from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { connect } from 'react-redux';
import { StyleSheet, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import { pickUpPin, destinationPin } from './MapPins';

let x = 0;
const MapViewComponent = props => {
  const { region, marker: markers, config } = props;
  return (
    <MapView
      style={styles.map}
      region={region}
      showsUserLocation
      provider={MapView.PROVIDER_GOOGLE}
      showsBuildings
      zoomEnabled
      moveOnMarkerPress
      scrollEnabled
      mapType="standard"
    >
      {markers ? (
        markers.map(marker => {
          x += 1;
          return (
            <MapView.Marker coordinate={marker.coordinates} title={marker.title} key={x}>
              {marker.title === 'pickup' ? pickUpPin() : destinationPin()}
            </MapView.Marker>
          );
        })
      ) : (
        <React.Fragment />
      )}
      {markers ? (
        <MapViewDirections
          mode="DRIVING"
          origin={markers[0].coordinates}
          destination={markers[1].coordinates}
          apikey={config.googleMapsApiKey}
          strokeWidth={3}
          strokeColor="black"
        />
      ) : (
        <React.Fragment />
      )}
    </MapView>
  );
};
MapViewComponent.propTypes = {
  region: PropTypes.objectOf(PropTypes.number).isRequired,
  marker: PropTypes.arrayOf(
    PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.object]))
  ).isRequired,
  config: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ).isRequired,
};
const { height, width } = Dimensions.get('window');
const heightRatio = height / 667.0;
const widthRatio = width / 375.0;
const styles = StyleSheet.create({
  map: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  markerImg: {
    height: 30 * heightRatio,
    width: 30 * widthRatio,
    position: 'relative',
    bottom: 15,
  },
});

const mapStateToProps = state => {
  return {
    config: state.config,
  };
};

export default connect(
  mapStateToProps,
  null
)(MapViewComponent);
