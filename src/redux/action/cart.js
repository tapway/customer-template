import { store } from '../configureStore';
import { ADD_PRODUCT, INCREASE_QUANTITY, DECREASE_QUANTITY } from '../actionTypes';
/**
 * Add product to cart
 */

const addProduct = data => {
  const select = data;
  const selected = {
    itemDescription: select.name,
    brand: select.brand,
    price: select.price,
    quantity: 1,
    _id: select._id,
  };
  const arr = [...store.getState().cart, selected];
  return {
    type: ADD_PRODUCT,
    payload: arr,
  };
};

const increaseQuantity = item => {
  return {
    type: INCREASE_QUANTITY,
    payload: { item },
  };
};

const decreaseQuantity = item => {
  return {
    type: DECREASE_QUANTITY,
    payload: { item },
  };
};

export { addProduct, increaseQuantity, decreaseQuantity };
