/* eslint-disable no-underscore-dangle */
import React, { PureComponent } from 'react';
import { View, Text, Image, FlatList, ActivityIndicator, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import styles from './style';
import getImage from '../../utils/getImage';
import Empty from '../../component/EmptyList';

const moment = require('moment');

export default class Details extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
    this.scrollFlag = false;
  }

  checkImg = element => {
    switch (element.catagory.name.toLowerCase()) {
      case 'food':
        return <Image resizeMode="contain" source={getImage.dish} style={styles.dishImg} />;
      case 'pets':
        return <Image resizeMode="contain" source={getImage.petsBlack} style={styles.dishImg} />;
      case 'groceries':
        return (
          <Image resizeMode="contain" source={getImage.groceriesBlack} style={styles.dishImg} />
        );
      case 'medical':
        return <Image resizeMode="contain" source={getImage.medicalBlack} style={styles.dishImg} />;
      case 'sports':
        return <Image resizeMode="contain" source={getImage.sportsBlack} style={styles.dishImg} />;
      case 'electronics':
        return (
          <Image resizeMode="contain" source={getImage.electronicsBlack} style={styles.ideaImg} />
        );
      case 'car':
        return <Image resizeMode="contain" source={getImage.car} style={styles.carImg} />;
      default:
        return <Image resizeMode="contain" source={getImage.car} style={styles.carImg} />;
    }
  };

  _renderItem = ({ item }) => {
    const { props } = this;
    return (
      <View style={styles.singleDetail} key={item._id}>
        <View style={styles.datContainer}>
          <View style={styles.imgContainer}>{this.checkImg(item)}</View>
          <View style={styles.details}>
            <View style={styles.location}>
              <Image source={getImage.history} resizeMode="contain" style={styles.ptrImg} />
              <View style={styles.locationTxt}>
                <Text numberOfLines={1} style={styles.address}>
                  {item.deliveryAddress}
                </Text>
                <Text numberOfLines={1} style={styles.address}>
                  {item.pickUpAddress}
                </Text>
              </View>
            </View>
            <View style={styles.value}>
              <Text style={styles.price}>${item.paymentAmount}</Text>
              <Text style={styles.bar}>|</Text>
              <Text style={styles.date}>
                {moment(item.createdAt).format('ddd DD MMM YY hh:mm A')}
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.progressContainer}>
          <View style={styles.progressSecCont}>
            <Image
              resizeMode="contain"
              style={styles.progressImage}
              source={getImage.secondPickup}
            />
            <Text style={styles.progressText}>{item.processingStatus.status}</Text>
          </View>
          {item.processingStatus.status === 'Accepted' ||
          item.processingStatus.status === 'Picked' ||
          item.processingStatus.status === 'Started' ? (
            <TouchableOpacity
              style={styles.trackBtn}
              onPress={() => {
                props.trackOrder(item._id);
              }}
            >
              <Text style={styles.trackText}>Track Order</Text>
            </TouchableOpacity>
          ) : null}
        </View>
      </View>
    );
  };

  _onEndReached = () => {
    const { props } = this;
    if (this.scrollFlag && !props.noMoreHistory) {
      props.loadMore();
      this.scrollFlag = false;
    }
  };

  render() {
    const { props } = this;
    const { history } = props;
    return (
      <FlatList
        data={history}
        showsVerticalScrollIndicator={false}
        keyExtractor={item => item._id}
        onEndReached={this._onEndReached}
        ListEmptyComponent={<Empty></Empty>}
        onEndReachedThreshold={0.5}
        onScrollBeginDrag={() => {
          this.scrollFlag = true;
        }}
        ListFooterComponent={
          props.loaded ? <ActivityIndicator size="small"></ActivityIndicator> : null
        }
        renderItem={this._renderItem}
        style={styles.detailsBlock}
      />
    );
  }
}
Details.propTypes = {
  trackOrder: PropTypes.func.isRequired,
  noMoreHistory: PropTypes.bool.isRequired,
  loadMore: PropTypes.func.isRequired,
  history: PropTypes.arrayOf(
    PropTypes.objectOf(
      PropTypes.oneOfType([PropTypes.number, PropTypes.string, PropTypes.object, PropTypes.array])
    )
  ).isRequired,
  loaded: PropTypes.bool.isRequired,
};
