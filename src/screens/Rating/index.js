/* eslint-disable react/no-unused-state */
import React, { PureComponent } from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import { Rating } from 'react-native-elements';
import { connect } from 'react-redux';
import RadioGroup from 'react-native-radio-buttons-group';
import PropTypes from 'prop-types';
import styles from './style';
import Colors from '../../utils/colors';
import getImage from '../../utils/getImage';
import HeaderBar from '../../component/HeaderBar';
import { deliveryRatingUpdate } from '../../socket';

class RatingComponent extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      trip: props.navigation.getParam('trip'),
      rating: null,
      data: [
        { label: 'Very Good', color: Colors.primaryColor, size: 15 },
        { label: 'Good', color: Colors.primaryColor, size: 15 },
        { label: 'Fair', color: Colors.primaryColor, size: 15 },
        { label: 'Poor', color: Colors.primaryColor, size: 15 },
      ],
      ratingComment: '',
    };
    this.home = this.home.bind(this);
    this.orderFinished = this.orderFinished.bind(this);
    this.ratingCompleted = this.ratingCompleted.bind(this);
    this.ratingComment = this.ratingComment.bind(this);
  }

  ratingComment(data) {
    const selectedButton = data.find(e => e.selected === true);
    this.setState({ ratingComment: selectedButton.value });
  }

  home() {
    const { props } = this;
    props.navigation.navigate('home');
  }

  orderFinished() {
    const { rating, ratingComment, trip } = this.state;
    deliveryRatingUpdate({
      deliveryRating: rating,
      deliveryReview: ratingComment,
      _id: trip._id,
    });
    this.home();
  }

  ratingCompleted(rating) {
    this.setState({ rating });
  }

  render() {
    const { data, trip } = this.state;
    return (
      <View style={styles.container}>
        <HeaderBar title="Order Complete"></HeaderBar>
        <View style={styles.firstContainer}>
          <Text style={styles.amountPaidText}>Amount Paid</Text>
          <Text style={styles.amountText}>$ {trip.paymentAmount}</Text>
        </View>
        <View style={styles.ratingContainer}>
          <Image
            style={styles.userIconImage}
            source={trip.profileImage ? { uri: trip.profileImage } : getImage.secondDriver}
          />
          <Text style={styles.userName}>{trip.fullName}</Text>
          <Rating
            onFinishRating={this.ratingCompleted}
            imageSize={45}
            style={{ paddingVertical: 10 }}
          />
        </View>
        <View style={styles.ratingDescritionContainer}>
          <Text style={styles.ratingDescritionText}>
            How would you rate your overall experience with our service?
          </Text>
          <RadioGroup radioButtons={data} flexDirection="row" onPress={this.ratingComment} />
        </View>
        <View style={styles.lastContainer}>
          <View style={[styles.loginBtnContainer]}>
            <TouchableOpacity style={styles.loginButton} onPress={this.orderFinished}>
              <Text style={styles.loginText}>Submit</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    trip: state.tripAccepted,
  };
};

RatingComponent.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  null
)(RatingComponent);
