/* eslint-disable no-use-before-define */
import React from 'react';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
// import Toast from 'react-native-easy-toast';
import { connect } from 'react-redux';
import { View, StyleSheet, Dimensions, Modal } from 'react-native';
import PropTypes from 'prop-types';
import Header from './HeaderBar';
import { heightRatio } from '../utils/styleSheetGuide';
import Colors from '../utils/colors';

const SearchLocationModal = props => {
  const { user, config, searchLocationModalVisible, activeModalId, addAddress } = props;
  // let toastRef = null;
  const places = user.savedAddresses.map(item => {
    return {
      description: item.type,
      geometry: { location: item.location },
      address: item.locationName,
    };
  });

  const Query = {
    key: config.googleMapsApiKey,
    language: 'en',
    types: ['(cities)'],
    components: 'country:in',
  };

  const _onSelectLocation = (data, details = null) => {
    const { changeDestination, pickupDrop, toggleSearchLocationModal } = props;
    if (changeDestination) {
      if (details.vicinity !== undefined) {
        props.setAddress(details.vicinity, details.geometry.location);
      } else {
        props.setAddress(details.address, details.geometry.location);
      }
      toggleSearchLocationModal();
      return null;
    }
    if (addAddress) {
      props.addNewAddress(details.formatted_address, details.geometry.location);
      return null;
    }
    if (pickupDrop) {
      props.setLocation(data, details);
      props.toggleSearchLocationModal();
      return null;
    }
    const { setLocation, toggleGetDetailsModal, toggleConfirmOrderModal } = props;
    setLocation(data, details);
    toggleSearchLocationModal();
    if (activeModalId === 0) toggleGetDetailsModal();
    else toggleConfirmOrderModal();
    return null;
  };

  return (
    <Modal
      animationType="slide"
      visible={searchLocationModalVisible}
      onRequestClose={() => {
        props.toggleSearchLocationModal();
      }}
    >
      <View style={styles.mainContainer}>
        <Header onlyBack back={() => props.toggleSearchLocationModal()} />
        <View style={styles.searchOutputBox}>
          <GooglePlacesAutocomplete
            placeholder="Enter Location"
            minLength={1}
            autoFocus
            returnKeyType="default"
            fetchDetails
            currentLocation
            onPress={_onSelectLocation}
            query={Query}
            styles={{
              description: {
                fontSize: 12 * heightRatio,
                fontWeight: '300',
              },
              row: {
                height: 40 * heightRatio,
              },
              separator: {
                marginHorizontal: 8 * widthRatio,
                height: 0.5,
              },
              textInputContainer: {
                backgroundColor: 'rgba(0,0,0,0)',
                borderTopWidth: 0,
                borderBottomWidth: 0,
              },

              textInput: {
                marginLeft: 0,
                marginRight: 0,
                height: 38 * heightRatio,
                color: 'black',
                fontSize: 12 * heightRatio,
              },
              predefinedPlacesDescription: {
                color: Colors.primaryColor,
                fontSize: 12 * heightRatio,
              },
            }}
            predefinedPlaces={addAddress ? [] : places}
            placeholderTextColor="grey"
            currentLocationLabel="Current location"
            nearbyPlacesAPI="GooglePlacesSearch"
            GooglePlacesSearchQuery={{
              rankby: 'distance',
            }}
            filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']}
            debounce={200}
          />
        </View>
      </View>
      {/* <Toast
        ref={input => {
          toastRef = input;
        }}
        style={{ backgroundColor: Colors.toastBgColor }}
        position="bottom"
        positionValue={400}
        fadeInDuration={750}
        fadeOutDuration={1000}
        opacity={0.8}
        textStyle={{ color: Colors.toastTextColor, fontSize: 15 }}
      /> */}
    </Modal>
  );
};

const mapStateToProps = state => {
  return { user: state.currentUser, config: state.config };
};
SearchLocationModal.propTypes = {
  user: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.array, PropTypes.bool])
  ).isRequired,
  config: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ).isRequired,
  searchLocationModalVisible: PropTypes.bool.isRequired,
  activeModalId: PropTypes.number,
  addAddress: PropTypes.bool,
  setAddress: PropTypes.func,
  addNewAddress: PropTypes.func,
  setLocation: PropTypes.func,
  toggleSearchLocationModal: PropTypes.func,
};
SearchLocationModal.defaultProps = {
  activeModalId: null,
  addAddress: false,
  setAddress: () => {},
  addNewAddress: () => {},
  setLocation: () => {},
  toggleSearchLocationModal: () => {},
};
export default connect(
  mapStateToProps,
  null
)(SearchLocationModal);

const { width } = Dimensions.get('window');
const widthRatio = width / 375.0;
const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    backgroundColor: Colors.modalContainerColor,
  },
  searchOutputBox: { flex: 1 },
});
