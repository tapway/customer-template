/* eslint-disable no-use-before-define */
import React, { PureComponent } from 'react';
import {
  View,
  Text,
  Modal,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  FlatList,
} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { indexOf } from 'lodash';
import { Dropdown } from 'react-native-material-dropdown';
import PropTypes from 'prop-types';
import Colors from '../../utils/colors';
import { heightRatio, width, widthRatio } from '../../utils/styleSheetGuide';
import Back from '../../component/BackButton';

export default class FilterModal extends PureComponent {
  state = { test: false, filters: { range: [0, 100], sort: 0, brands: [] } };

  componentWillReceiveProps = nextProps => {
    const { filters } = this.props;
    if (nextProps.filters !== filters)
      this.setState({
        filters: nextProps.filters,
      });
  };

  customMarker = () => {
    return <View style={styles.marker}></View>;
  };

  handleClearAll = () => {
    const { minPrice, maxPrice } = this.props;
    this.setState({
      filters: {
        range: [minPrice, maxPrice],
        sort: 0,
        brands: [],
      },
    });
  };

  renderBrands = ({ item }) => {
    const { state } = this;
    return (
      <TouchableOpacity
        style={[
          styles.itemContainer,
          styles.itemBtn,
          {
            borderColor:
              indexOf(state.filters.brands, item) !== -1
                ? Colors.primaryColor
                : Colors.secondaryBorderColor,
          },
        ]}
        onPress={() => {
          if (indexOf(state.filters.brands, item) !== -1) {
            const index = state.filters.brands.indexOf(item);
            const arr = state.filters.brands;
            const res = arr.slice(0, index).concat(arr.slice(index + 1, arr.length));
            this.setState(prevState => {
              return {
                filters: {
                  range: prevState.filters.range,
                  sort: prevState.filters.sort,
                  brands: res,
                },
              };
            });
          } else {
            this.setState(prevState => {
              return {
                filters: {
                  range: prevState.filters.range,
                  sort: prevState.filters.sort,
                  brands: [...prevState.filters.brands, item],
                },
              };
            });
          }
        }}
      >
        <Text numberOfLines={1}>{item}</Text>
      </TouchableOpacity>
    );
  };

  render() {
    const data = [
      {
        value: 'None',
        sort: 0,
      },
      {
        value: 'Price: Low to High',
        sort: 1,
      },
      {
        value: 'Price: High to Low',
        sort: 2,
      },
    ];
    const { props, state } = this;
    const { minPrice, maxPrice } = props;
    return (
      <Modal
        visible={props.filterModal}
        animated
        animationType="slide"
        onRequestClose={() => {
          props.handleStateChange({ filterModal: false });
        }}
      >
        <SafeAreaView>
          <View style={styles.mainContainer}>
            <View style={styles.pageHeadingContainer}>
              <Back back={() => props.handleStateChange({ filterModal: false })} />
              <TouchableOpacity
                onPress={() => {
                  this.handleClearAll();
                }}
                style={styles.clearBtnContainer}
              >
                <Text style={styles.pageHeading}>Clear All</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.filterContainer}>
              <View style={styles.filterName}>
                <Text style={styles.filterNameText}>Price Range</Text>
              </View>
              <View style={styles.filterContent}>
                <View style={styles.filterTopContent}>
                  <View>
                    <Text style={styles.priceDescription}>MIN PRICE</Text>
                    <Text style={styles.priceValue}>{state.filters.range[0]}</Text>
                  </View>
                  <View>
                    <Text style={styles.priceDescription}>MAX PRICE</Text>
                    <Text style={styles.priceValue}>{state.filters.range[1]}</Text>
                  </View>
                </View>
                <MultiSlider
                  min={minPrice}
                  max={maxPrice}
                  step={100}
                  values={state.filters.range}
                  onValuesChange={values => {
                    this.setState(prevState => {
                      return {
                        filters: {
                          range: values,
                          sort: prevState.filters.sort,
                          brands: prevState.filters.brands,
                        },
                      };
                    });
                  }}
                  sliderLength={width - 50 * widthRatio}
                  selectedStyle={{
                    backgroundColor: Colors.primaryColor,
                  }}
                  customMarker={this.customMarker}
                />
              </View>
            </View>
            <View style={styles.filterContainer}>
              <View style={styles.filterName}>
                <Text style={styles.filterNameText}>Sort By</Text>
              </View>
              <View style={styles.filterContent}>
                <Dropdown
                  ref="sort"
                  data={data}
                  value={data[state.filters.sort].value}
                  fontSize={12 * heightRatio}
                  containerStyle={{ marginTop: -25 * heightRatio }}
                  style={{ fontWeight: '500' }}
                ></Dropdown>
              </View>
            </View>
            <View style={[styles.filterContainer, { flex: 1, marginBottom: 50 }]}>
              <View style={styles.filterName}>
                <Text style={styles.filterNameText}>Brands</Text>
              </View>
              <View style={styles.filterContent}>
                <FlatList
                  keyExtractor={({ item }) => item}
                  showsVerticalScrollIndicator={false}
                  data={props.brandList}
                  extraData={state.filters.brands}
                  numColumns={3}
                  renderItem={this.renderBrands}
                  columnWrapperStyle={{
                    justifyContent: 'space-evenly',
                    marginBottom: 10,
                  }}
                ></FlatList>
              </View>
            </View>
            <View style={{ alignItems: 'center', marginTop: 2 }}>
              <TouchableOpacity
                style={styles.applyBtn}
                onPress={() => {
                  props.handleStateChange({ filterModal: false });
                  props.handleApplyFilters({
                    range: state.filters.range,
                    sort: this.refs.sort.selectedItem().sort,
                    brands: state.filters.brands,
                  });
                }}
              >
                <Text style={styles.applyText}>Apply</Text>
              </TouchableOpacity>
            </View>
          </View>
        </SafeAreaView>
      </Modal>
    );
  }
}

FilterModal.propTypes = {
  filters: PropTypes.shape({
    brands: PropTypes.arrayOf(PropTypes.string),
    range: PropTypes.arrayOf(PropTypes.number),
    sort: PropTypes.number,
  }).isRequired,
  minPrice: PropTypes.number.isRequired,
  maxPrice: PropTypes.number.isRequired,
  filterModal: PropTypes.bool.isRequired,
  handleStateChange: PropTypes.func.isRequired,
  brandList: PropTypes.arrayOf(PropTypes.string).isRequired,
  handleApplyFilters: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  marker: {
    height: 20,
    width: 20,
    borderRadius: 10,
    borderColor: Colors.primaryColor,
    borderWidth: 2,
    backgroundColor: Colors.white,
  },
  clearBtnContainer: {
    marginTop: 10,
    borderColor: Colors.primaryBtnBgColor,
    borderWidth: 1,
    padding: 5 * heightRatio,
    paddingHorizontal: 10 * widthRatio,
    borderRadius: 5,
  },
  pageHeadingContainer: {
    marginBottom: 10 * heightRatio,
    marginLeft: -10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  pageHeading: {
    fontSize: 12 * heightRatio,
  },
  mainContainer: {
    padding: 20 * heightRatio,
    paddingTop: 10,
    height: '100%',
  },
  filterContainer: {
    marginVertical: 8 * heightRatio,
  },
  filterName: {
    marginBottom: 10 * heightRatio,
  },
  filterNameText: {
    fontSize: 14 * heightRatio,
  },
  filterTopContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  priceDescription: {
    fontSize: 8 * heightRatio,
    fontWeight: '100',
  },
  priceValue: {
    fontWeight: '500',
    marginTop: 4 * heightRatio,
    fontSize: 12 * heightRatio,
  },
  applyBtn: {
    width: '90%',
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderWidth: 2,
    borderColor: Colors.primaryBtnBgColor,
    backgroundColor: Colors.primaryBtnBgColor,

    borderRadius: 30,
  },
  itemContainer: {
    width: '30%',
    borderWidth: 1,
  },
  itemBtn: {
    padding: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  applyText: { color: Colors.textForPrimaryBtnBgColor, fontSize: 20 },
});
