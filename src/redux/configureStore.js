import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { createLogger } from 'redux-logger';
import reducer from './reducer';

const persistConfig = {
  timeout: null,
  key: 'root',
  whitelist: ['currentUser'],
  storage,
};
const log = createLogger({ diff: true, collapsed: true });
const middleware = [thunk, log];
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; //eslint-disable-line
const persistedReducer = persistReducer(persistConfig, reducer);
const enhancer = composeEnhancers(applyMiddleware(...middleware));
export const store = createStore(persistedReducer, enhancer);
export const persistor = persistStore(store);
