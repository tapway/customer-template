import { StyleSheet, Dimensions, Platform } from 'react-native';
import { heightRatio, widthRatio } from '../../utils/styleSheetGuide';
import Colors from '../../utils/colors';

const { height } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  placeholder: {
    height: 8,
    marginTop: 6,
    marginLeft: 15,
    alignSelf: 'flex-start',
    justifyContent: 'center',
    backgroundColor: Colors.flatListPlaceholderColor,
  },
  highlightImageContainer: {
    height: 144 * heightRatio,
  },
  singleContainer: Platform.select({
    ios: {
      flexDirection: 'row',
      marginHorizontal: 5 * widthRatio,
      marginVertical: 5 * heightRatio,
      padding: heightRatio * 10,
      paddingRight: 0,
      paddingBottom: 5 * heightRatio,
      backgroundColor: Colors.flatListItemBgColor,
      shadowColor: Colors.primaryShadowColor,
      shadowOffset: { width: 0.3, height: 0.3 },
      shadowOpacity: 0.5,
      shadowRadius: 1,
    },
    android: {
      flexDirection: 'row',
      marginHorizontal: 5,
      marginVertical: 5 * heightRatio,
      padding: heightRatio * 10,
      paddingRight: 0,
      paddingBottom: 5 * heightRatio,
      backgroundColor: Colors.flatListItemBgColor,
      borderWidth: 1,
      borderColor: Colors.primaryBorderColor,
    },
  }),
  placeholderSingleContainer: Platform.select({
    ios: {
      width: widthRatio * 350,
      flexDirection: 'row',
      marginHorizontal: 5,
      marginVertical: 5 * heightRatio,
      padding: heightRatio * 10,
      paddingRight: 0,
      backgroundColor: Colors.flatListItemBgColor,
      shadowColor: Colors.primaryShadowColor,
      shadowOffset: { width: 0.3, height: 0.3 },
      shadowOpacity: 0.5,
      shadowRadius: 1,
    },
    android: {
      width: widthRatio * 350,
      flexDirection: 'row',
      marginHorizontal: 5,
      marginVertical: 10,
      padding: heightRatio * 10,
      paddingRight: 0,
      backgroundColor: Colors.flatListItemBgColor,
      borderWidth: 1,
      borderColor: Colors.primaryBorderColor,
    },
  }),
  highlightImage: {
    flex: 1,
    height: 144 * heightRatio,
    width: '100%',
  },
  listContainer: {
    flex: 6,
    margin: height / 150,
  },
  menuIcon: {
    height: heightRatio * 40,
  },
  rating: {
    marginBottom: heightRatio * 10,
  },
  resDetails: {
    flex: 1,
  },
  resIcon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  restaurant_name: {
    fontSize: 14 * heightRatio,
    fontWeight: '400',
  },
  placeholderRestaurantName: {
    height: 14 * heightRatio,
    width: widthRatio * 300,
  },
  address: {
    fontSize: 10 * heightRatio,
    fontWeight: '100',
    marginTop: 5,
    marginBottom: 10,
  },
  placeholderAddress: {
    height: 11 * heightRatio,
    width: widthRatio * 260,
  },
  placeholderRating: {
    height: 11 * heightRatio,
    width: widthRatio * 140,
  },
});
