/* eslint-disable import/no-unresolved */
import React, { Component } from 'react';
import SplashScreen from 'react-native-splash-screen';
import { Sentry } from 'react-native-sentry';
import OneSignal from 'react-native-onesignal';
import { StyleProvider } from 'native-base';
import { store } from './src/redux/configureStore';
import setDeviceInfo from './src/redux/action/deviceInfo';
import setConfig from './src/redux/action/config';
import StackNav from './src/navigation/stackNavigation';
import config from './src/apiServices/config';
import NavigationService from './src/navigation/navigationService';

import getTheme from './theme/native-base-theme/components';
// import material from './theme/native-base-theme/variables/material';
import variables from './theme/native-base-theme/variables/variables';

Sentry.config(
  'https://17f21459eb01485888ac107c687d4381@sentry.io/1474432',
).install();

class App extends Component {
  constructor() {
    super();
    // OneSignal.init('08805615-cf3a-43a0-b451-32cf9c30289e', {
    OneSignal.init('7617b417-aad4-41f1-ad26-d1544befc6be', {
      kOSSettingsKeyAutoPrompt: true,
    });
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
    OneSignal.configure(); // triggers the ids event
  }

  componentDidMount() {
    config(this.handleAppConfig);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  handleAppConfig = res => {
    if (res.success) {
      store.dispatch(setConfig(res.data));
      SplashScreen.hide();
    } else {
      SplashScreen.hide();
    }
  };

  onOpened = openResult => {
    if (openResult.notification.payload.additionalData.status === 'Completed') {
      NavigationService.navigate('ratingPage', {
        trip: openResult.notification.payload.additionalData.trip,
      });
    } else {
      NavigationService.navigate('successfullyBooked', {
        id: openResult.notification.payload.additionalData.trip._id,
      });
    }
  };

  onIds = device => {
    store.dispatch(setDeviceInfo(device.userId, device.pushToken));
  };

  render() {
    // eslint-disable-next-line no-console
    console.disableYellowBox = true;
    return (
      <StyleProvider style={getTheme(variables)}>
        <StackNav
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </StyleProvider>
    );
  }
}
export default App;

// some changes just for flexing
