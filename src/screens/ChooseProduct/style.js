import { StyleSheet, Platform } from 'react-native';
import Colors from '../../utils/colors';
import { heightRatio, widthRatio } from '../../utils/styleSheetGuide';

export default StyleSheet.create({
  mainView: {
    flex: 1,
  },
  fullFlex: { flex: 1 },
  container: { flex: 1 },
  restaurantDetails: {
    flexDirection: 'column',
    paddingHorizontal: 18 * widthRatio,
    backgroundColor: Colors.highlightedContainer,
  },
  placeholderContainer: {
    width: '90%',
    backgroundColor: Colors.themeBackgroundColor,
    height: 200,
  },
  resDetails: {
    justifyContent: 'center',
  },
  placeholderName: {
    width: widthRatio * 160,
    height: heightRatio * 12,
  },
  placeholderBrand: {
    width: widthRatio * 120,
    height: heightRatio * 12,
  },
  placeholderButton: {
    width: widthRatio * 80,
    height: heightRatio * 20,
  },
  placeholder: {
    height: 8,
    marginTop: 6,
    marginLeft: 15,
    alignSelf: 'flex-start',
    justifyContent: 'center',
    backgroundColor: Colors.flatListPlaceholderColor,
  },
  resName: {
    color: Colors.textForThemeBgColor,
    paddingTop: heightRatio * 10,
    fontSize: 12 * heightRatio,
    marginBottom: 6 * heightRatio,
    fontWeight: '600',
  },
  resAddress: {
    fontSize: 10 * heightRatio,
    fontWeight: '200',
  },
  secondaryColor: {
    color: Colors.textForThemeBgColor,
  },
  bottomLine: {
    height: 0.5,
    marginTop: 8 * heightRatio,
    marginBottom: 7 * heightRatio,
    backgroundColor: Colors.secondaryBorderColor,
  },

  deliveryDetails: {
    flexDirection: 'row',
    marginBottom: 10 * heightRatio,
    alignItems: 'center',
  },
  iconTime: {
    marginRight: 11 * widthRatio,
    width: 16 * widthRatio,
    height: 16 * heightRatio,
  },
  iconTruck: {
    marginRight: 9 * widthRatio,
    height: 14 * heightRatio,
    width: 18 * widthRatio,
  },
  itemList: {
    flex: 1,
    paddingHorizontal: 18 * widthRatio,
  },
  delType: {
    fontSize: 15,
    fontWeight: '600',
  },
  footer: {
    height: 49 * heightRatio,
    backgroundColor: Colors.primaryColor,
  },
  footerText: {
    color: Colors.themeBackgroundColor,
    fontSize: 16 * heightRatio,
    textAlign: 'center',
    fontWeight: '500',
  },
  footerButton: {
    flex: 1,
    justifyContent: 'center',
  },
  placeholderSingleContainer: Platform.select({
    ios: {
      width: widthRatio * 330,
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingRight: widthRatio * 20,
      marginTop: heightRatio * 10,
      marginHorizontal: widthRatio * 1,
      padding: heightRatio * 10,
      backgroundColor: Colors.flatListItemBgColor,
      shadowColor: Colors.primaryShadowColor,
      shadowOffset: { width: 0.3, height: 0.3 },
      shadowOpacity: 0.5,
      shadowRadius: 1,
    },
    android: {
      width: widthRatio * 330,
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingRight: widthRatio * 20,
      marginTop: heightRatio * 10,
      marginHorizontal: widthRatio * 1,
      padding: heightRatio * 10,
      backgroundColor: Colors.flatListItemBgColor,
      borderWidth: 1,
      borderColor: Colors.primaryBorderColor,
    },
  }),
});
