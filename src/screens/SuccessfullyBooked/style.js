import { StyleSheet } from 'react-native';
import { heightRatio, widthRatio } from '../../utils/styleSheetGuide';
import Colors from '../../utils/colors';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    backgroundColor: Colors.themeBackgroundColor,
    width: widthRatio * 340,
    borderColor: Colors.secondaryBorderColor,
    borderWidth: 0.5,
    shadowColor: Colors.secondaryShadowColor,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 1.5,
    elevation: 3,
    borderRadius: 10,
    marginVertical: heightRatio * 20,
  },
  navigateContainer: {
    height: heightRatio * 40,
    position: 'absolute',
    right: 17 * widthRatio,
    alignItems: 'flex-end',
    alignSelf: 'flex-end',
    justifyContent: 'flex-end',
  },
  navigateIconContainer: {
    height: heightRatio * 40,
    width: heightRatio * 40,
    borderRadius: (heightRatio * 40) / 2,
    backgroundColor: Colors.themeBackgroundColor,
    shadowColor: Colors.secondaryShadowColor,
    shadowOffset: { width: 4, height: 4 },
    shadowOpacity: 0.1,
    shadowRadius: 1.5,
    elevation: 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  locationArrow: {
    fontSize: heightRatio * 15,
  },
  map: {
    flex: 1,
    position: 'relative',
  },
  modal: {
    width: '100%',
    alignItems: 'center',
    position: 'absolute',
    bottom: 30,
  },
  topCard: {
    height: 80 * heightRatio,
    flexDirection: 'row',
    margin: heightRatio * 10,
    marginHorizontal: widthRatio * 20,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderColor: Colors.secondaryBorderColor,
  },
  bottomCard: {
    flex: 1,
    flexDirection: 'column',
    paddingHorizontal: widthRatio * 20,
  },
  image: {
    height: heightRatio * 50,
    width: heightRatio * 50,
    borderRadius: (heightRatio * 50) / 2,
  },
  placeholderImage: {
    height: heightRatio * 50,
    width: heightRatio * 50,
    borderRadius: (heightRatio * 50) / 2,
  },
  carNumber: {
    fontSize: heightRatio * 14,
  },
  driverDetails: {
    flex: 1,
    flexDirection: 'row',
  },
  driverDetailsInfo: {
    padding: heightRatio * 10,
    alignItems: 'flex-start',
  },
  callIconContainer: {
    height: 22 * heightRatio,
    width: 22 * heightRatio,
    padding: 4 * heightRatio,
    borderRadius: 11 * heightRatio,
    backgroundColor: Colors.primaryBtnBgColor,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: -5,
    right: -5,
  },
  callIcon: {
    fontSize: 16 * heightRatio,
    color: Colors.textForPrimaryBtnBgColor,
  },
  name: {
    fontSize: heightRatio * 13,
    marginBottom: heightRatio * 5,
    width: widthRatio * 100,
  },
  placeholderName: {
    width: widthRatio * 100,
    marginBottom: heightRatio * 5,
  },
  otpContainer: {
    // flex: 1,
    height: '100%',
    width: widthRatio * 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  otp: {
    padding: 10,
    borderWidth: 0.5,
    borderRadius: 10,
  },
  placeholderContainer: {
    backgroundColor: Colors.themeBackgroundColor,
    height: 80 * heightRatio,
    flexDirection: 'row',
    margin: heightRatio * 10,
    marginHorizontal: widthRatio * 20,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderColor: Colors.secondaryBorderColor,
  },
  placeholder: {
    height: heightRatio * 10,
    backgroundColor: Colors.flatListPlaceholderColor,
  },
  otpPlaceholder: {
    width: widthRatio * 50,
  },
  orderStatusContainer: {
    height: 70 * heightRatio,
    width: '100%',
    paddingVertical: 10 * heightRatio,
  },
  orderStatusIconsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 26 * widthRatio,
    alignItems: 'center',
  },
  orderStatusTextContainer: {
    height: 14 * heightRatio,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  tickIconStyle: {
    fontSize: 24 * heightRatio,
  },
  orderStatusTextStyle: {
    fontSize: 10 * heightRatio,
  },
  lineStyle: {
    height: 0.5,
    width: 70 * widthRatio,
  },
  orderStatusBottomContainer: {
    height: 80 * heightRatio,
    width: '100%',
    justifyContent: 'center',
  },
  orderStatusOnlyBottomContainer: {
    marginTop: -10 * heightRatio,
    height: 70 * heightRatio,
    width: '100%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  orderStatusHeading: { fontSize: 12 * heightRatio, fontWeight: '500' },
  orderStatusSubHeading: {
    fontSize: 10 * heightRatio,
    marginTop: 5 * heightRatio,
  },
});
