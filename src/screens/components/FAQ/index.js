/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, TouchableNativeFeedback, TextInput } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Header,
  Left,
  Button,
  Icon,
  Body,
  Right,
  Content,
  Grid,
  Row,
  Col,
  Card,
  Item,
  Input,
  Picker,
  Thumbnail,
  CardItem,
  H1,
  Accordion,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import styles from './styles';

const dataArray = [
  {
    title: 'How much does shipping cost?',
    content:
      'Shipping charges vary based on your location and the size. For an accurate estimate of shipping charges please use our quick action buttons.',
  },
  {
    title: 'What information do I need to order?',
    content:
      `In order to process your order we require the following information: ${'\n'}${'\n'}* Quantity of the product(s) you want to dispatch ${'\n'}${'\n'} * The address and information of the recipient  ${'\n'}${'\n'} * Phone number – in case we need to contact you quickly in regards to your order and tracking of your parcel ${'\n'}${'\n'} * Email address – for order confirmation and invoice ${'\n'}${'\n'}  * Payment method – please use our wallet to credit your account. Deductions would be made once the order has been confirmed.`,
  },
  {
    title: 'How do I know if my order has been delivered?',
    content:
      `We have added various quality control process to ensure that the recipient receives the order the same way as sent. ${'\n'}${'\n'}The rider will upload picture evidence close to a landmark confirming this. An sms alert or notification via the application will show an indication of this.`,
  },
  {
    title: 'How do I contact customer service?',
    content:
      `You can contact our customer service team via: ${'\n'}${'\n'} Email: ${'\n'} Phone:`,
  },
  {
    title: 'How can I pay for my order?',
    content:
      `Our inbuilt wallet system allows you to top up your account and also refund you if you cancel or have some change left `,
  },
  {
    title: 'Is it safe using Tapway?',
    content:
      `We are safe, highly reliable, pocket friendly and convenient. Our independent courier personnels go through series of security checks to ensure quality assurances which includes: ${'\n'}${'\n'}
* Background check : personal identification, guarantors attestation and authentication${'\n'}
* Drivers license, insurance and vehicular registration${'\n'}
* Full vehicle check 
${'\n'}
* Tapway also provides insurances.`,
  },
  {
    title: 'Who will deliver my parcel?',
    content:
      `Tapway has a pool of crowd sourced and vetted riders  within your district. We have selected folks from varying demographics or age brackets who are fit and dependable enough. ${'\n'}${'\n'}All of them have been ingrained with the nitty gritty of customer service. In addition, we have reviewed their background checks as well his vehicular registration.`,
  },

  {
    title: 'What are ratings important?',
    content:
      `The ratings helps us deliver top notch services by giving riders with higher ratings better access to customers. In so doing, we keep you happy. ${'\n'}${'\n'}Tapway only associates with riders with higher ratings from customers and merchants. Ratings also helps us reward riders who have kept our customers really happy.`,
  },
];

class FAQ extends Component {
  render() {
    return (
      <Container>
        <Header
          transparent
          hasTabs
          style={{
            backgroundColor: '#FFFFFF',
            borderBottomColor: '#0001',
            borderBottomWidth: 1.5,
          }}
          androidStatusBarColor={'#0001'}
          iosBarStyle="dark-content">
          <Left>
            <Button transparent onPress={() => Actions.HomePage()}>
              <Icon
                style={{ paddingHorizontal: 5, color: '#000000' }}
                name={'arrow-back'}
              />
            </Button>
          </Left>
          <Body style={styles.headtxtdiv}>
            <Text
              style={{
                fontFamily: 'Nunito-SemiBold',
                fontSize: 18,
                color: '#030E1A',
                width: '100%',
                textAlign: 'center',
              }}>
              FAQ
            </Text>
          </Body>
          <Right />
        </Header>
        <Content contentContainerStyle={{ padding: 20 }}>
          <View style={styles.inputContainer}>
            <H1>Frequently Asked Questions:</H1>
          </View>
          <View style={styles.inputContainer}>
            <Accordion dataArray={dataArray} expanded={0} />
          </View>
          {/* <View style={styles.inputContainer}>
            <Button
              block
              full
              style={{
                backgroundColor: '#3FAF5D',
                borderRadius: 5,
                marginTop: 50,
              }}
              onPress={() => { }}>
              <Text
                style={{
                  color: '#ffffff',
                  fontFamily: 'Nunito-SemiBold',
                  fontSize: 14,
                }}>
                SUBMIT YOUR QUESTION
              </Text>
            </Button>
          </View> */}
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FAQ);
