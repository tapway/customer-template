import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Header from '../../component/HeaderBar';
import LogoutModal from '../LogOut';
import styles from './style';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      logoutModal: false,
    };
  }

  setLogoutModal = () => {
    this.setState(prevState => {
      return { logoutModal: !prevState.logoutModal };
    });
  };

  render() {
    const list = [
      { title: 'Order History', nav: 'orderHistory' },
      { title: 'Saved Addresses', nav: 'savedAddress' },
      { title: 'About', nav: 'about' },
    ];
    const { state, props } = this;
    return (
      <View style={styles.container}>
        <LogoutModal
          navigate={props.navigation}
          logoutModal={state.logoutModal}
          setLogoutModal={this.setLogoutModal}
        />
        <View style={styles.container}>
          <Header title="Profile" />
          <View style={styles.container}>
            <View style={styles.profile}>
              <View style={styles.imgContainer}>
                <Image
                  resizeMode="contain"
                  source={{ uri: props.user.profileImage }}
                  style={styles.profilePic}
                />
              </View>
              <View style={styles.details}>
                <Text style={styles.name}>{props.user.fullName}</Text>
                <Text style={styles.email}>{props.user.email}</Text>
                <Text style={styles.phoneNumber}>{props.user.phoneNumber}</Text>
              </View>
            </View>
            <View style={styles.bottomContainer}>
              {list.map(item => {
                return (
                  <TouchableOpacity
                    onPress={() => props.navigation.navigate(item.nav)}
                    key={item.title + item.nav}
                  >
                    <Text style={styles.listText}>{item.title}</Text>
                  </TouchableOpacity>
                );
              })}
              <TouchableOpacity onPress={() => this.setLogoutModal()}>
                <Text style={styles.signOutText}>Sign out</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View></View>
      </View>
    );
  }
}
Profile.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  user: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.array, PropTypes.bool])
  ).isRequired,
};
const mapStateToProps = state => {
  return {
    user: state.currentUser,
  };
};
export default connect(
  mapStateToProps,
  null
)(Profile);
