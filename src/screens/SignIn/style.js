import { StyleSheet } from 'react-native';
import { heightRatio, widthRatio } from '../../utils/styleSheetGuide';
import Colors from '../../utils/colors';
import { FontNames } from '../../../theme';
import { RFValue, RFPercentage } from 'react-native-responsive-fontsize';

export default StyleSheet.create({
  main: {
    flex: 1,
  },
  phoneInput: {
    padding: 10 * widthRatio,
    paddingBottom: 10 * heightRatio,
    marginTop: 20 * heightRatio,
    borderBottomWidth: 1,
    borderBottomColor: Colors.primaryBorderColor,
  },
  mainContainer: {
    flex: 1,
    margin: heightRatio * 40,
    marginBottom: heightRatio * 20,
  },
  container: {
    flex: 1,
  },
  headingContainer: {
    height: heightRatio * 80,
    justifyContent: 'flex-end',
  },
  nofocus: {
    borderColor: Colors.primaryBorderColor,
  },
  inputContainer: {
    height: heightRatio * 130,
    justifyContent: 'center',
  },
  modalInputContainer: {
    flex: 1,
    padding: heightRatio * 40,
    justifyContent: 'space-between',
    paddingBottom: 0 * heightRatio,
    marginBottom: 10,
  },
  flagImg: {
    height: 26,
    width: 32,
    marginRight: 10,
  },
  textInputBox: {
    borderStyle: 'solid',
    borderBottomWidth: 1,
    padding: widthRatio * 10,
    paddingLeft: 0,
    width: '100%',
    color: Colors.inputTextColor,
    flexDirection: 'row',
  },
  loginBtnContainer: {
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    marginBottom: 10 * heightRatio,
  },
  signUpContainer: {
    height: heightRatio * 50,
    backgroundColor: Colors.signUpContainer,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: heightRatio * 30,
    justifyContent: 'center',
  },
  signUpText: { fontSize: 12 * heightRatio, fontWeight: '200' },
  gap: { height: heightRatio * 20 },
  googleFbBtns: {
    flexDirection: 'row',
  },
  headingTxt: {
    fontSize: heightRatio * 30,
    color: Colors.onBoardHeadingColor,
    fontWeight: '500',
  },
  subHeadingTxt: {
    fontSize: heightRatio * 32,
    color: Colors.secondaryColor,
    fontWeight: 'bold',
  },
  inputHeading: {
    fontWeight: '200',
    fontSize: 14 * heightRatio,
  },
  text4: {
    fontWeight: '200',
    fontSize: 20,
  },
  loginText: {
    fontSize: 16 * heightRatio,
    textAlign: 'center',
    color: Colors.textForPrimaryBgColor,
  },
  text6: {
    fontWeight: '200',
    fontSize: 15,
  },
  signUpImage: {
    height: heightRatio * 300,
    width: widthRatio * 330,
  },
  button: {
    height: 50 * heightRatio,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: Colors.primaryBtnBgColor,
    backgroundColor: Colors.primaryBtnBgColor,
    borderWidth: 1,
    borderRadius: 25 * heightRatio,
  },
  img: {
    width: widthRatio * 40,
    height: heightRatio * 24,
    borderRadius: 10,
  },
});

export const signInStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1891D0',
  },
  white: {
    color: '#FFFFFF',
  },
  welcomeHd: {
    //fontWeight: '600',
    color: '#FFFFFF',
    fontSize: RFValue(30),
    fontFamily: FontNames.bold,
    //width: '70%',
  },
  inputContainer: {
    marginVertical: 5,
  },
  inputLabel: {
    fontSize: RFValue(18),
    fontFamily: FontNames.regular,
    color: '#FFFFFF',
    opacity: 0.7,
  },
  inputItem: {
    borderColor: '#FFFFFF',
    color: '#FFFFFF',
    borderWidth: 1,
    fontSize: 22,
    marginTop: 3,
    paddingHorizontal: 10,
    height: 47,
    marginTop: 13,
    fontFamily: 'Nunito-Regular',
  },
  inputText: {
    color: '#FFFFFF',
    opacity: 0.9,
  },
  submitBtn: {
    backgroundColor: '#FFFFFF',
    borderRadius: 4,
    marginTop: 40,
    marginBottom: 30,
  },
  btnText: {
    color: '#030E1A',
    fontSize: RFValue(18),
    fontFamily: FontNames.semibold,
    //letterSpacing: 0.5,
  },
  signUp: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerMenu: {
    // bottom: 20,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  actionContainer: {
    flex: 1,
    marginTop: 30,
  },
  actionTitle: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontSize: 16,
    fontFamily: FontNames.regular,
  },
  actionColView: {
    alignSelf: 'center',
    backgroundColor: '#FFF5',
    padding: 40,
    borderRadius: 70,
  },
  bgimg: { position: 'absolute', bottom: 0, height: RFPercentage(70), zIndex: -100, width: '100%' },
  actionImageSize: { width: RFValue(35), height: RFValue(33) },
  actionText: {
    color: '#FFFFFF',
    fontSize: 12,
    paddingTop: RFValue(12),
    fontFamily: FontNames.semibold,
    textAlign: 'center',
  },
});
