import { StyleSheet, Platform } from 'react-native';
import { heightRatio, widthRatio } from '../../utils/styleSheetGuide';
import Colors from '../../utils/colors';

export default StyleSheet.create({
  map: {
    height: 250 * heightRatio,
    width: 440 * widthRatio,
  },
  card: Platform.select({
    ios: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: Colors.flatListItemBgColor,
      shadowColor: Colors.primaryShadowColor,
      shadowOffset: { width: 0.3, height: 0.3 },
      shadowOpacity: 0.3,
      shadowRadius: 1,
      marginTop: 10 * heightRatio,
    },
    android: {
      flex: 1,
      alignItems: 'center',
      borderWidth: 1,
      backgroundColor: Colors.flatListItemBgColor,
      borderColor: Colors.primaryBorderColor,
      marginTop: 10 * heightRatio,
    },
  }),
  icon: { paddingBottom: 0 * heightRatio, alignSelf: 'flex-start' },
  addButton: {
    height: 50 * heightRatio,
    width: 220 * widthRatio,
    backgroundColor: Colors.primaryColor,
    alignItems: 'center',
    borderRadius: 25 * heightRatio,
    justifyContent: 'center',
  },
  contLoc: {
    width: '100%',
    paddingLeft: 15 * widthRatio,
    marginBottom: 10 * heightRatio,
  },
  typeText: {
    fontWeight: 'bold',
    marginBottom: 8 * heightRatio,
    letterSpacing: 0.4,
  },
  locationNameText: { letterSpacing: 0.4, lineHeight: 18 },
  container: { flex: 1 },
  btnContainer: {
    alignItems: 'center',
    paddingTop: 10 * heightRatio,
    paddingBottom: 20 * heightRatio,
  },
});
