const Colors = {
  // * Primary Theme colors
  // primaryColor: '#35d8fb',
  // secondaryColor: '#fca487',
  primaryColor: '#1891D0',
  secondaryColor: '#081c24',
  themeBackgroundColor: '#FFF',
  highlightedContainer: '#f2f2f2',
  flatListItemBgColor: '#FFF',
  flatListPlaceholderColor: '#eeeeee',

  // * Border colors
  primaryBorderColor: 'lightgrey',
  secondaryBorderColor: 'silver',
  modalBorderColor: 'rgba(0, 0, 0, 0.05)',
  borderWithPrimaryBgColor: 'white',

  // * Special colors
  searchBarColor: '#f2f2f2',
  signUpContainer: '#F2F3F3',
  onBoardHeadingColor: '#2C3435',
  homeHeaderData: '#696969',
  mapPinStoke: 'black',
  profileHighlightedContainer: '#F7F3F3',
  welcomeScreenText: '#374042',

  // * Modal's colors
  modalBackgroundColor: '#00000040',
  modalContainerColor: '#FFF',

  // * Constant colors
  white: 'white',
  black: 'black',
  transparent: 'transparent',
  lightgrey: 'lightgrey',

  // * Font colors
  // -----------> Font colors for containers
  textForPrimaryBgColor: 'white',
  textForModalContainerColor: 'black',
  textForThemeBgColor: 'black',
  textForHighlightedContainer: 'black',

  // -----------> Font colors for buttons
  textForPrimaryBtnBgColor: 'white',
  textForThemeBgBtn: 'black',

  // -----------> Font colors general
  placeHolderTextColor: 'grey',
  placeHolderTextColorForPrimaryBg: 'rgb(200, 252, 230)',
  secondarySubHeadingVerifyScreen: '#A6A4A3',

  // * Button colors
  primaryBtnBgColor: '#35d8fb',
  secondaryBtnBgColor: '#fca487',

  // * Shadow colors
  primaryShadowColor: 'grey',
  secondaryShadowColor: '#000',

  // * Toast colors
  toastBgColor: 'black',
  toastTextColor: 'orange',
};

export default Colors;
