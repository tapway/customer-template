/* eslint-disable react/jsx-filename-extension */
/* eslint-disable react/no-unescaped-entities */
import React, { Component } from 'react';
import CodeInput from 'react-native-confirmation-code-input';
import Toast from 'react-native-easy-toast';
import { connect } from 'react-redux';
import {
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';
import Header from '../../component/HeaderBar';
import styles from './style';
import setUser from '../../redux/action/verify';
import Loader from '../../component/Loader';
import verify from '../../apiServices/verify';
import resendOtp from '../../apiServices/resendOtp';
import { RFValue } from "react-native-responsive-fontsize"
import { homeResetAction } from '../../navigation/resetNavigation';
import Colors from '../../utils/colors';

class Verify extends Component {
  constructor() {
    super();
    this.state = {
      verificationCode: '',
      imageLoader: false,
    };
  }

  onVerificationCode = phoneNumber => {
    const { state } = this;
    this.setState({ imageLoader: true });
    verify(
      phoneNumber,
      state.verificationCode,
      this.handleError,
      this.handleSuccess
    );
  };

  handleSuccess = res => {
    const { props } = this;

    const { user, jwtAccessToken } = res.data;
    props.setUserDispatcher(user, jwtAccessToken);
    props.navigation.dispatch(homeResetAction);
    this.setState({ imageLoader: false });
    props.navigation.navigate('scenes');
  };

  handleError = () => {
    const { refs } = this;
    this.setState({ imageLoader: false });
    refs.toast.show('Wrong Verification Code');
  };
  onSubmit = (code, phone) => {
    this.setState({ verificationCode: code });
    setTimeout(() => {
      this.onVerificationCode(phone)
    }, 100);
  }
  resendOtp = (phoneNumber) => {
    resendOtp(phoneNumber)
    this.refs.toast.show('OTP has been resent!');
  }
  render() {
    const { props, state } = this;
    const phoneNumber = props.navigation.getParam('phoneNumber');
    return (
      <View style={styles.fullFlex}>
        <Header onlyBack back={() => props.navigation.goBack()} style={{ borderBottomWidth: 0, marginTop: RFValue(30) }} />
        <TouchableWithoutFeedback
          style={styles.fullFlex}
          onPress={Keyboard.dismiss}>
          <KeyboardAvoidingView
            style={styles.container}
            behavior="padding"
            enabled={Platform.OS === 'ios'}>
            <Loader loading={state.imageLoader} />
            <View style={styles.headings}>
              <Text style={styles.primarySubHeading}>
                Enter 4 digit code sent to the email address provided
              </Text>
            </View>
            <View style={styles.verificationCodeInput}>
              <CodeInput
                ref="codeInputRef1"
                secureTextEntry
                keyboardType="numeric"
                className="border-box"
                space={30}
                codeLength={4}
                size={40}
                codeInputStyle={styles.codeinput}
                inputPosition="left"
                activeColor={Colors.black}
                inactiveColor="grey"
                onFulfill={code => this.onSubmit(code, phoneNumber)}
                containerStyle={styles.codecontainer}
              />
            </View>
            <View style={styles.nextBtnContainer}>
              <TouchableOpacity
                style={styles.nextBtn}
                onPress={() => this.onVerificationCode(phoneNumber)}>
                <Text style={styles.nextText}>VERIFY</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.resendContainer}>
              <Text style={styles.resendCodeText}>Didn’t recieve a verification code? </Text>
              <TouchableOpacity onPress={() => this.resendOtp(phoneNumber)}>
                <Text style={styles.boldResend}>Resend Code</Text>
              </TouchableOpacity>
            </View>
          </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
        <Toast
          ref="toast"
          style={{ backgroundColor: Colors.toastBgColor }}
          position="bottom"
          positionValue={400}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: Colors.toastTextColor, fontSize: 15 }}
        />
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setUserDispatcher: (user, jwtAccessToken) => {
      dispatch(setUser(user, jwtAccessToken));
    },
  };
};
Verify.propTypes = {
  setUserDispatcher: PropTypes.func.isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  null,
  mapDispatchToProps
)(Verify);
