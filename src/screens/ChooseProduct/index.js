/* eslint-disable react/no-unused-state */
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, BackHandler } from 'react-native';
import { connect } from 'react-redux';
import Toast from 'react-native-easy-toast';
import _ from 'lodash';
import PropTypes from 'prop-types';
import Header from '../../component/HeaderBar';
import styles from './style';
import ListBuilder from './ListBuilder';
import { store } from '../../redux/configureStore';
import ClearCart from '../../component/ClearCartModal';
import { fetchItemsByShopWithFilters } from '../../apiServices/fetchItemsInShop';
import fetchFilterRequirements from '../../apiServices/fetchFilterRequirements';
import Loading from './Loading';
import TopComponent from './TopComponent';
import FilterModal from './FilterModal';
import SearchBar from '../../component/SearchBar';
import Colors from '../../utils/colors';
import { CLEAR_CART } from '../../redux/actionTypes';

class ChooseProduct extends Component {
  loadingComponent: Promise<React.Element<*>>;

  constructor(props) {
    super(props);
    fetchFilterRequirements(
      props.navigation.getParam('shopDetails')._id,
      this.handleResultFilterRequirements
    );
    this.state = {
      list: [],
      shop: props.navigation.getParam('shopDetails'),

      clearCartModal: false,
      filterModal: false,

      loaded: false,
      moreLoading: false,
      noMoreItems: false,
      scroll: false,

      query: '',

      brandList: [],
      minPrice: 0,
      maxPrice: 1000,
      filters: {
        range: [0, 1000],
        sort: 0,
        brands: [],
      },
    };
  }

  componentDidMount = () => {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.androidBack);
  };

  componentWillUpdate = (nextProps, nextState) => {
    const { state } = this;
    if (state.filters !== nextState.filters)
      fetchItemsByShopWithFilters(
        10,
        0,
        'Product',
        nextState.shop._id,
        this.fillItems,
        nextState.query,
        nextState.filters
      );
  };

  componentWillUnmount() {
    this.backHandler.remove();
  }

  androidBack = () => {
    this.handleBack(); // works best when the goBack is async
    return true;
  };

  handleResultFilterRequirements = res => {
    const { data } = res;
    this.setState({
      brandList: data.brandList,
      minPrice: data.minPrice,
      maxPrice: data.maxPrice,
      filters: {
        range: [data.minPrice, data.maxPrice],
        sort: 0,
        brands: [],
      },
    });
  };

  loadMore = () => {
    const { state } = this;
    fetchItemsByShopWithFilters(
      10,
      state.list.length,
      'Product',
      state.shop._id,
      this.addMoreItems,
      state.query,
      state.filters
    );
  };

  handleStateChange = payload => {
    this.setState(payload);
  };

  addMoreItems = res => {
    const { data: result } = res;
    if (result.length === 0) {
      this.setState({ noMoreItems: true });
    }
    this.setState(
      prevState => {
        const list = _.uniqBy([...prevState.list, ...result], item => {
          return item._id;
        });
        return {
          list,
        };
      },
      () => {
        this.setState(prevState => {
          return {
            moreLoading: !prevState.moreLoading,
          };
        });
      }
    );
  };

  fillItems = res => {
    const { data: result } = res;
    this.setState({
      list: result,
      loaded: true,
    });
  };

  handleQueryChange = str => {
    this.setState({ query: str, noMoreItems: false, loaded: false }, () => {
      const { state } = this;
      fetchItemsByShopWithFilters(
        10,
        0,
        'Product',
        state.shop._id,
        this.fillItems,
        state.query,
        state.filters
      );
    });
  };

  handleApplyFilters = filters => {
    this.setState({ filters, noMoreItems: false, loaded: false, scroll: false }, () => {
      const { state } = this;
      fetchItemsByShopWithFilters(
        10,
        0,
        'Product',
        state.shop._id,
        this.fillItems,
        state.query,
        state.filters
      );
    });
  };

  handleBack = () => {
    const { props } = this;
    if (props.cart.length !== 0) this.setClearCartModal();
    else {
      props.navigation.goBack();
    }
  };

  clear = () => {
    const { props } = this;
    this.setClearCartModal();
    props.clearCartDispatcher();
    props.navigation.goBack();
  };

  setClearCartModal = () => {
    this.setState(prevState => {
      return { clearCartModal: !prevState.clearCartModal };
    });
  };

  submit = () => {
    const { refs, props, state } = this;
    if (store.getState().cart.length === 0) refs.toast.show('Select atleast 1 item!');
    else {
      props.navigation.navigate('confirmOrder', { shop: state.shop });
    }
  };

  render() {
    const { state } = this;
    return (
      <View style={styles.mainView}>
        <Header
          plusFilter
          title="Choose product"
          back={this.handleBack}
          handleOpenFilter={() => this.handleStateChange({ filterModal: true })}
        />
        <ClearCart
          clearCartModal={state.clearCartModal}
          setClearCartModal={this.setClearCartModal}
          clear={this.clear}
        />
        <FilterModal
          filterModal={state.filterModal}
          filters={state.filters}
          brandList={state.brandList}
          minPrice={state.minPrice}
          maxPrice={state.maxPrice}
          handleStateChange={this.handleStateChange}
          handleApplyFilters={this.handleApplyFilters}
        ></FilterModal>
        <View style={styles.fullFlex}>
          <TopComponent
            shop={state.shop}
            scroll={state.scroll}
            itemList={state.list}
            handleStateChange={this.handleStateChange}
          ></TopComponent>
          <View style={styles.itemList}>
            <SearchBar handleQueryChange={this.handleQueryChange} value={state.query} />
            {state.loaded ? (
              <ListBuilder
                searchList={state.searchList}
                scroll={state.scroll}
                noMoreItems={state.noMoreItems}
                moreLoading={state.moreLoading}
                itemList={state.list}
                handleStateChange={this.handleStateChange}
                loadMore={this.loadMore}
                handleQueryChange={this.handleQueryChange}
              />
            ) : (
              <Loading
                loadingComponent={this.loadingComponent}
                handleQueryChange={this.handleQueryChange}
              />
            )}
          </View>
          <View style={styles.footer}>
            <TouchableOpacity
              style={styles.footerButton}
              onPress={() => {
                this.submit();
              }}
            >
              <Text style={styles.footerText}>Continue</Text>
            </TouchableOpacity>
          </View>
        </View>
        <Toast
          ref="toast"
          style={{ backgroundColor: Colors.toastBgColor }}
          position="bottom"
          positionValue={400}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: Colors.toastTextColor, fontSize: 15 }}
        />
      </View>
    );
  }
}

ChooseProduct.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  cart: PropTypes.arrayOf(
    PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number]))
  ).isRequired,
  clearCartDispatcher: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
  return { cart: state.cart };
};
const mapDispatchToProps = dispatch => {
  return {
    clearCartDispatcher: () => {
      dispatch({ type: CLEAR_CART });
    },
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChooseProduct);
