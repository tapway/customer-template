import SERVER from '../config/config';
// import HelperFetch from './HelperFetch';

/**
 * Resend the otp to requested Phone Number
 * @return {success}
 */

const resendOtp = async mobileNumber => {
  // const handleSuccess = () => {};
  // const handleError = () => {};

  // const extension = `/endUserAuth/resendOtp`;

  // await HelperFetch(
  //   {
  //     extension,
  //     post: true,
  //     body: {
  //       phoneNumber: mobileNumber,
  //     },
  //   },
  //   handleSuccess,
  //   handleError
  // );
  // eslint-disable-next-line no-undef
  await fetch(`${SERVER}/api/endUserAuth/resendOtp`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      phoneNumber: mobileNumber,
    }),
  })
    .then(res => res.json())
    .then(res => res)
    .catch(err => err);
};

export default resendOtp;
