import { StyleSheet, Platform } from 'react-native';

import { heightRatio, widthRatio } from '../../utils/styleSheetGuide';
import Colors from '../../utils/colors';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    paddingHorizontal: 17.5 * widthRatio,
    paddingTop: 15 * heightRatio,
  },
  placeholder: {
    height: 8,
    marginTop: 6,
    marginLeft: 15,
    alignSelf: 'flex-start',
    justifyContent: 'center',
    backgroundColor: Colors.flatListPlaceholderColor,
  },
  placeholderSingleContainer: Platform.select({
    ios: {
      width: widthRatio * 330,
      flexDirection: 'row',
      marginHorizontal: widthRatio * 2,
      marginVertical: heightRatio * 5,
      padding: heightRatio * 10,
      paddingRight: 0,

      backgroundColor: Colors.flatListItemBgColor,
      shadowColor: Colors.primaryShadowColor,
      shadowOffset: { width: 0.3, height: 0.3 },
      shadowOpacity: 0.5,
      shadowRadius: 1,
    },
    android: {
      width: widthRatio * 330,
      flexDirection: 'row',
      marginHorizontal: widthRatio * 2,
      marginVertical: heightRatio * 5,
      padding: heightRatio * 10,
      paddingRight: 0,
      backgroundColor: Colors.flatListItemBgColor,
      borderWidth: 1,
      borderColor: Colors.primaryBorderColor,
    },
  }),
  swiper: { height: 160 * heightRatio },
  fullFlex: {
    flex: 1,
  },
  ActivityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  swiperImg: { flex: 1, width: '100%' },
  itemContainer: Platform.select({
    ios: {
      flex: 1,
      padding: 10 * heightRatio,
      marginVertical: 5 * heightRatio,
      marginHorizontal: 2,
      flexDirection: 'column',
      backgroundColor: Colors.flatListItemBgColor,
      shadowColor: Colors.primaryShadowColor,
      shadowOffset: { width: 0.3, height: 0.3 },
      shadowOpacity: 0.5,
      shadowRadius: 1,
    },
    android: {
      flex: 1,
      padding: 10 * heightRatio,
      marginVertical: 5 * heightRatio,
      flexDirection: 'column',
      backgroundColor: Colors.flatListItemBgColor,
      borderWidth: 1,
      borderColor: Colors.primaryBorderColor,
    },
  }),
  nameContainer: { marginVertical: 2.5 * heightRatio },
  addressContainer: {
    marginTop: 2.5 * heightRatio,
    marginBottom: 13 * heightRatio,
  },
  timeContainer: {
    flexDirection: 'row',
    marginBottom: 13 * heightRatio,
    justifyContent: 'center',
  },
  deliveryTypeContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  nameText: { fontSize: 14 * heightRatio },
  addressText: { fontSize: 10 * heightRatio, fontWeight: '300' },
  timeImage: {
    height: '100%',
    width: 16 * heightRatio,
  },
  placeholderTimeImage: {
    height: 12 * heightRatio,
    width: 12 * heightRatio,
  },
  timeTextContainer: {
    flex: 1,
    marginLeft: 11 * widthRatio,
    justifyContent: 'center',
  },
  timeText: { fontSize: 10 * heightRatio },
  placeholderTimeText: { height: 12 * heightRatio },
  deliveryImg: {
    height: '100%',
    width: 18 * heightRatio,
  },
  deliveryTextContainer: {
    flex: 1,
    marginLeft: 9 * widthRatio,
    justifyContent: 'center',
  },
  deliveryText: { fontSize: 10 * heightRatio },
  placeholderRestaurantName: {
    height: 18 * heightRatio,
    width: widthRatio * 280,
  },
  placeholderAddress: {
    height: 15 * heightRatio,
    width: widthRatio * 260,
  },
  placeholderTime: {
    height: 14 * heightRatio,
    marginTop: heightRatio * 10,
    width: widthRatio * 150,
  },
});
