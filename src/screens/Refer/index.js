import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './style';
import Header from '../../component/HeaderBar';

const Refer = props => {
  return (
    <View style={styles.mainContainer}>
      <Header plusTitle title="Refer a friend" back={() => props.navigation.goBack()}></Header>
      <View style={styles.mainTextConatiner}>
        <Text style={styles.mainText}>Share code for 50% off</Text>
        <Text style={styles.text}>
          Your friend gets 50% off on first order. You get 50% off on your next order. You can earn
          this coupon upto 5 times.
        </Text>
      </View>
      <View style={styles.referalContainer}>
        <Text style={styles.referralText}>YOUR REFERRAL CODE</Text>
        <View style={styles.referalCodeContainer}>
          <Text style={styles.code}>AS12EQ</Text>
        </View>
      </View>
    </View>
  );
};

Refer.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default Refer;
