/* eslint-disable no-undef */
/* eslint-disable prefer-promise-reject-errors */
//import { AccessToken, LoginManager } from 'react-native-fbsdk';
import { Platform } from 'react-native';

const getFbUrl = token =>
  `https://graph.facebook.com/me?access_token=${token}&fields=name,first_name,last_name,email`;

const loginWithFacebook = async () =>
  new Promise(async (resolve, reject) => {
    try {
      if (Platform.OS === 'android') {
        LoginManager.setLoginBehavior('web_only');
      }
      const result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);
      if (result.isCancelled) {
        return reject({ token: null, type: 'cancel' });
      }

      const data = await AccessToken.getCurrentAccessToken();
      if (data.accessToken) {
        // Fetching user-details
        return resolve({ token: data.accessToken.toString(), type: 'success' });
      }

      return reject({ token: null, type: 'error in fetching access token' });
    } catch (error) {
      return reject({ token: null, type: error });
    }
  });

export default async function requestFbLogin(signup) {
  return new Promise(async resolve => {
    try {
      const res = await loginWithFacebook();
      const { token, type } = res;
      const loginSuccessful = type && token && type !== 'cancel' && token.length > 0;
      if (!loginSuccessful || type !== 'success') {
        return resolve({ successFb: false });
      }
      const fbUrl = getFbUrl(token);
      const response = await fetch(fbUrl); // eslint-disable-line
      const user = await response.json();
      if (!user || !user.email) {
        // eslint-disable-next-line no-alert
        return alert('Email not found');
      }
      const credentials = {
        name: user.name,
        givenName: user.first_name,
        familyName: user.last_name,
        email: user.email,
        profileImage: user.picture,
      };
      signup(credentials);
      return credentials;
    } catch (e) {
      throw e;
    }
  });
}
