import { get } from 'lodash';
import { FETCH_CURRENT_USER, SET_REQUEST, DUMP_REQUEST } from '../actionTypes';

const initialState = {
  id: '',
  userId: '',
  category: '',
  deliveryInstructions: {},
  processingStatus: '',
  paymentMode: '',
  paymentAmount: '',
  endUserGpsLocation: '',
  deliveryLocation: '',
  pickupLocation: '',
  deliveryAddress: '',
  pickupAddress: '',
  type: '',
};

export default function(state = initialState, action) {
  // debugger;
  const returnProperty = (property, payload) => {
    return get(payload, property, get(state, property));
  };
  switch (action.type) {
    case FETCH_CURRENT_USER: {
      return { ...state };
    }
    case SET_REQUEST: {
      return {
        id: returnProperty('id', action.payload),
        userId: returnProperty('userId', action.payload),
        category: returnProperty('category', action.payload),
        processingStatus: returnProperty('processingStatus', action.payload),
        paymentMode: returnProperty('paymentMode', action.payload),
        paymentAmount: returnProperty('paymentAmount', action.payload),
        endUserGpsLocation: returnProperty('endUserGpsLocation', action.payload),
        deliveryLocation: returnProperty('deliveryLocation', action.payload),
        pickupLocation: returnProperty('pickupLocation', action.payload),
        deliveryAddress: returnProperty('deliveryAddress', action.payload),
        pickupAddress: returnProperty('pickupAddress', action.payload),
        type: returnProperty('type', action.payload),
        deliveryInstructions: returnProperty('deliveryInstructions', action.payload),
      };
    }
    case DUMP_REQUEST: {
      return initialState;
    }
    default: {
      return state;
    }
  }
}
