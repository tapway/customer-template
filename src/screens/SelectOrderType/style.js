import { Dimensions, StyleSheet } from 'react-native';
import Colors from '../../utils/colors';

const { height, width } = Dimensions.get('window');
const heightRatio = height / 667.0;
const widthRatio = width / 375.0;
const styles = StyleSheet.create({
  mainContainer: { flex: 1 },
  containerList: {
    marginHorizontal: 17 * widthRatio,
    marginBottom: 6 * heightRatio,
  },
  card: {
    height: 100 * heightRatio,
    width: 341 * widthRatio,
    paddingTop: 18 * heightRatio,
    paddingBottom: 14 * heightRatio,
    paddingLeft: 21 * widthRatio,
    paddingRight: 18 * widthRatio,
    backgroundColor: Colors.primaryColor,
    borderRadius: 4,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 9 * heightRatio,
  },
  img: {
    width: 64 * heightRatio,
  },
  itemNameTxt: {
    color: Colors.white,
    textAlign: 'right',
    fontSize: 15 * heightRatio,
  },
  backOrange: {
    backgroundColor: Colors.secondaryColor,
  },
  textView: { flex: 1, height: '100%', justifyContent: 'center' },
});
export default styles;
