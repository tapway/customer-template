import React from 'react';
import { TouchableOpacity, Text, ImageBackground } from 'react-native';
import PropTypes from 'prop-types';
import styles from './style';

const Catagory = props => {
  const { press, image, type } = props;
  return (
    <TouchableOpacity onPress={() => press()} style={styles.orderType}>
      <ImageBackground source={image} style={styles.orderTypeImage}>
        <Text style={styles.imageText}>{type}</Text>
      </ImageBackground>
    </TouchableOpacity>
  );
};

Catagory.propTypes = {
  press: PropTypes.func.isRequired,
  image: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
};

export default Catagory;
