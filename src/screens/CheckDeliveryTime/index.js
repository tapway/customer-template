/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, TouchableNativeFeedback, Image } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Header,
  Left,
  Icon,
  Body,
  Right,
  Content,
  Button,
  Card,
  Item,
  Input,
  Col,
  Picker,
  TouchableOpacity
  // Form,
  // CardItem,
} from 'native-base';
// import { Actions } from 'react-native-router-flux';
import { Grid, Row } from 'react-native-easy-grid';
import styles from './styles';

import { getInitialState, handleSetAddress, getRegisterOrderObject, getDist, getEstimate, getAmountEstimate, getTimeEstimate } from '../components/HomePage/helper';
import { getDistance } from 'geolib';

import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import RNGooglePlaces from 'react-native-google-places';
import getImage from '../../utils/getImage';
import { RFValue } from 'react-native-responsive-fontsize';

export class CheckDeliveryTime extends Component {
  constructor(props) {
    super(props);

    this.state = {
      itemQuality: 0,
      selectedCard: '',
      checkPriceOrder: false,
      searchLocationModalVisible: false,
      getDetailsModalVisible: false,
      confirmOrderModalVisible: false,
      tripEstimate: false,

      itemDetails: [],
      detailAddress: '',
      detailAddress2: '',

      origin: 'Search here...',
      destination: 'Search here...',
      data: {},
      details: {},


      isPackageDetailsVisible: false,
      fromLocation: '',
      fromLocationSet: false,
      toLocation: '',
      toLocationSet: false,
      predictions: [],
      currentInput: '',
      region: {
        latitude: 6.599033,
        longitude: 3.3411348,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
      id: 0,
      marker: [
        {
          title: 'pickup',
          coordinates: {
            latitude: 0,
            longitude: 0,
          },
        },
        {
          title: 'drop',
          coordinates: {
            latitude: 0,
            longitude: 0,
          },
        },
      ],
    };
  }

  togglecheckPriceOrder = () => {
    const { checkPriceOrder } = this.state;
    this.setState({ checkPriceOrder: !checkPriceOrder });
  };

  handleSelectedCard = selectedCard => {
    this.setState({ selectedCard });
  };

  getPredictions = place => {
    RNGooglePlaces.getAutocompletePredictions(place, {
      //  type: 'cities',
      country: 'NG',
    })
      .then(results =>
        this.setState(
          { predictions: results },
          () => { console.log('Predictions', results) }
          //console.log('Predictions', results),
        )
      )
      .catch(error => console.log('google error', error.message));
  };

  openSearchModal() {
    RNGooglePlaces.openAutocompleteModal()
      .then((place) => {
        console.log(place);
        // place represents user's selection from the
        // suggestions and it is a simplified Google Place object.
      })
      .catch(error => console.log(error.message));  // error is a Javascript Error object
  }

  clearPredictions = () => {
    this.setState({ predictions: [] });
  };

  setLocationPlace = (place, locationType) => {
    if (locationType === 'from') {
      RNGooglePlaces.lookUpPlaceByID(place.placeID)
        .then(results => {
          console.log('logging lookUpPlaceByID', results);
          const { latitude, longitude, viewport } = results.location;
          this.setState(
            {
              fromLocationSet: true,
              fromLocation: results.name,
              fromLocation_lat: latitude,
              fromLocation_lng: longitude,
              region: {
                latitude,
                longitude: this.state.toLocation_lng
                  ? this.state.toLocation_lng
                  : longitude,
                latitudeDelta: 0.3699098778279124,
                longitudeDelta: 0.1909481361508365,
              },
            },
            () => {
              // console.log('region', JSON.stringify(region));
              this.clearPredictions();
            }
          );
        })
        .catch(error => console.log(error.message));
    } else {
      RNGooglePlaces.lookUpPlaceByID(place.placeID)
        .then(results => {
          const { latitude, longitude } = results.location;
          this.setState(
            {
              toLocationSet: true,
              toLocation: results.name,
              toLocation_lat: latitude,
              toLocation_lng: longitude,
              region: {
                latitude: this.state.fromLocation_lat
                  ? this.state.fromLocation_lat
                  : latitude,
                longitude,
                latitudeDelta: 0.3699098778279124,
                longitudeDelta: 0.1909481361508365,
              },
            },
            () => {
              this.clearPredictions();
            }
          );
        })
        .catch(error => console.log(error.message));
    }
  };

  placesLists = (place, locationType) => {
    console.log('placeeeeeee', place);

    if (locationType === 'from') {
      this.setState(
        {
          locationType,
          fromLocation: place,
          fromLocationSet: false,
          currentInput: locationType,
        },
        () => {
          this.getPredictions(place, locationType);
        }
      );
    } else {
      this.setState(
        {
          locationType,
          toLocationSet: false,
          toLocation: place,
          currentInput: locationType,
        },
        () => {
          this.getPredictions(place, locationType);
        }
      );
    }
  };

  render() {
    const {
      toLocation,
      toLocationSet,
      toLocation_lat,
      toLocation_lng,

      fromLocation,
      fromLocationSet,
      fromLocation_lat,
      fromLocation_lng,

      predictions,
      currentInput,
      region,
    } = this.state;
    console.log(this.state.fromLocation);
    const { itemQuality, selectedCard, checkPriceOrder } = this.state;
    return (
      <Container>
        <Header
          style={{ backgroundColor: '#FFFFFF', marginTop: 35 }}
          androidStatusBarColor="#FFFFFF"
          iosBarStyle="dark-content">
          <Left>
            <Button transparent onPress={() => this.props.navigation.pop()}>
              <Icon name="arrow-back" style={{ color: '#000000' }} />
            </Button>
          </Left>
          <Body style={styles.headtxtdiv}>
            <Text
              numberOfLines={1}
              style={{
                fontSize: 18,
                color: '#030E1A',
                fontFamily: 'Nunito-SemiBold',
                width: '100%',
                textAlign: 'center',
              }}>
              Check Delivery Time
            </Text>
          </Body>
          <Right />
        </Header>
        <Content contentContainerStyle={{ padding: 10 }}>

          <Card transparent>
            <View style={styles.inputContainer}>
              <Text style={styles.inputLabel}>Pickup Location</Text>
              <Item regular style={styles.inputItem}>
                <Icon
                  type="FontAwesome"
                  name="map-marker"
                  style={{ color: '#1B2E5A', fontSize: 20 }}
                />
                <Input
                  placeholder="Enter Location"
                  placeholderTextColor="#383838"
                  style={styles.inputText}

                  value={`${fromLocation}`}
                  keyboardType="default"
                  onChangeText={text => this.placesLists(text, 'from')}
                  clearButtonMode="always"
                  onBlur={this.clearPredictions}
                  maxLength={50}
                />
              </Item>
            </View>

            <View style={styles.inputContainer}>
              <Text style={styles.inputLabel}>Drop Location</Text>
              <Item regular style={styles.inputItem}>
                <Icon
                  type="FontAwesome"
                  name="map-marker"
                  style={{ color: '#3FAF5D', fontSize: 20 }}
                />
                <Input
                  placeholder="Enter Location"
                  placeholderTextColor="#383838"
                  style={styles.inputText}
                />
              </Item>
            </View>
            <View style={styles.inputContainer}>
              <Text style={styles.inputLabel}>Select size of package</Text>
              <Grid>
                <Row>
                  <Col style={{ margin: 5 }}>
                    <Card
                      style={[
                        styles.packageCard,
                        selectedCard === 'small' ? styles.selectedCard : {},
                      ]}>
                      <TouchableNativeFeedback
                        onPress={() => this.handleSelectedCard('small')}>
                        <View style={{ alignItems: 'center' }}>
                          <Image source={getImage.smallsize} resizeMode='contain' style={{ width: RFValue(32), height: RFValue(40), marginTop: RFValue(6) }} />
                          {selectedCard == 'small' ? <Image source={getImage.checkselect} resizeMode='contain' style={{ position: 'absolute', width: RFValue(30), height: RFValue(30), right: 5 }} /> : null}
                          <Text style={styles.packageSize}>Small</Text>
                          <Text style={styles.packageWeight}>Up to 10kg</Text>
                        </View>
                      </TouchableNativeFeedback>
                    </Card>
                  </Col>
                  <Col style={{ margin: 5 }}>
                    <Card
                      style={[
                        styles.packageCard,
                        selectedCard === 'medium' ? styles.selectedCard : {},
                      ]}>
                      <TouchableNativeFeedback
                        onPress={() => this.handleSelectedCard('medium')}>
                        <View style={{ alignItems: 'center' }}>
                          <Image source={getImage.mediumsize} resizeMode='contain' style={{ width: RFValue(40), height: RFValue(40), marginTop: RFValue(6) }} />
                          {selectedCard == 'medium' ? <Image source={getImage.checkselect} resizeMode='contain' style={{ position: 'absolute', width: RFValue(30), height: RFValue(30), right: 5 }} /> : null}
                          <Text style={styles.packageSize}>Medium</Text>
                          <Text style={styles.packageWeight}>Up to 20kg</Text>
                        </View>
                      </TouchableNativeFeedback>
                    </Card>
                  </Col>
                  <Col style={{ margin: 5 }}>
                    <Card
                      style={[
                        styles.packageCard,
                        selectedCard === 'large' ? styles.selectedCard : {},
                      ]}>
                      <TouchableNativeFeedback
                        onPress={() => this.handleSelectedCard('large')}>
                        <View style={{ alignItems: 'center' }}>
                          <Image source={getImage.largesize} resizeMode='contain' style={{ width: RFValue(40), height: RFValue(40), marginTop: RFValue(6) }} />
                          {selectedCard == 'large' ? <Image source={getImage.checkselect} resizeMode='contain' style={{ position: 'absolute', width: RFValue(30), height: RFValue(30), right: 5 }} /> : null}
                          <Text style={styles.packageSize}>Large</Text>
                          <Text style={styles.packageWeight}>Up to 30kg</Text>
                        </View>
                      </TouchableNativeFeedback>
                    </Card>
                  </Col>
                </Row>
              </Grid>
            </View>
            <View style={styles.inputContainer}>
              <Text style={styles.inputLabel}>Select size of package</Text>
              <Item regular style={styles.inputItem}>
                <Picker
                  note
                  mode="dropdown"
                  // style={{ width: 120 }}
                  selectedValue={itemQuality}
                  onValueChange={item => this.setState({ itemQuality: item })}>
                  <Picker.Item label="Select Number" value="0" />
                  <Picker.Item label="1" value="1" />
                  <Picker.Item label="2" value="2" />
                  <Picker.Item label="3" value="3" />
                  <Picker.Item label="4" value="4" />
                  <Picker.Item label="5" value="5" />
                </Picker>
              </Item>
            </View>
          </Card>

          <Button
            block
            primary
            style={styles.submitBtn}
            onPress={() => this.togglecheckPriceOrder()}>
            <Text style={styles.btnText}>CHECK DELIVERY TIME</Text>
          </Button>
        </Content>
        {checkPriceOrder && (
          <View
            style={{
              position: 'absolute',
              top: 0,
              bottom: 0,
              right: 0,
              left: 0,
              backgroundColor: '#1B2E5A',
            }}>
            <Header
              style={{ backgroundColor: '#1B2E5A', marginTop: 35 }}
              iosBarStyle="light-content"
              androidStatusBarColor="#1B2E5A">
              <Left>
                <Button
                  transparent
                  onPress={() => this.togglecheckPriceOrder()}>
                  <Icon name="arrow-back" style={{ color: '#ffffff' }} />
                </Button>
              </Left>
              <Body>
                <Text
                  numberOfLines={1}
                  style={{
                    fontSize: 18,
                    color: '#FFFFFF',
                    fontFamily: 'Nunito-SemiBold',
                  }}>
                  Check Price
                </Text>
              </Body>
              <Right />
            </Header>
            <Content padder>
              <Card style={{ paddingHorizontal: 10 }}>
                <View style={[styles.inputContainer, styles.containerBorder]}>
                  <Text style={styles.inputLabel2}>Pickup Location</Text>
                  <Text style={styles.inputText2}>
                    5 Ogunbamila Street, Lady Lak, Bariga
                  </Text>
                </View>
                <View style={[styles.inputContainer, styles.containerBorder]}>
                  <Text style={styles.inputLabel2}>Drop Location</Text>
                  <Text style={styles.inputText2}>
                    Akilo Street, Lady Lak, Bariga
                  </Text>
                </View>
                <View style={[styles.inputContainer, styles.containerBorder]}>
                  <Grid>
                    <Row>
                      <Col>
                        <Text style={styles.inputLabel2}>Size of Package</Text>
                        <Card transparent>
                          <View style={{ flexDirection: 'row' }}>
                            <Icon
                              type="AntDesign"
                              name="inbox"
                              style={styles.packageIcon}
                            />
                            <View>
                              <Text style={styles.packageSize}>Large</Text>
                              <Text style={styles.packageWeight}>
                                Up to 30kg
                              </Text>
                            </View>
                          </View>
                        </Card>
                      </Col>
                      <Col>
                        <Text style={styles.inputLabel2}>Quantity</Text>
                        <Card transparent>
                          <View>
                            <Text style={styles.inputText2}>2</Text>
                          </View>
                        </Card>
                      </Col>
                    </Row>
                  </Grid>
                </View>
                <View style={[styles.inputContainer, styles.containerBorder]}>
                  <Grid>
                    <Row>
                      <Col>
                        <Text style={styles.inputLabel2}>Total Distance</Text>
                        <Card transparent>
                          <View>
                            <Text style={styles.inputText2}>5.4 km</Text>
                          </View>
                        </Card>
                      </Col>
                      <Col>
                        <Text style={styles.inputLabel2}>
                          Est. Delivery time
                        </Text>
                        <Card transparent>
                          <View>
                            <Text style={styles.inputText2}>45 min</Text>
                          </View>
                        </Card>
                      </Col>
                    </Row>
                  </Grid>
                </View>
                <View style={[styles.inputContainer]}>
                  <View>
                    <Text style={styles.inputLabel2}>Size of Package</Text>
                  </View>
                  <Text
                    style={{
                      alignSelf: 'center',
                      fontSize: 30,
                      color: '#030E1A',
                    }}>
                    ₦24,000
                  </Text>
                </View>

                <Button
                  block
                  primary
                  style={styles.submitBtn}
                  onPress={() => this.props.navigation.navigate('login')}>
                  <Text style={styles.btnText}>LOGIN TO MAKE ORDER</Text>
                </Button>
              </Card>
            </Content>
          </View>
        )}
      </Container>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CheckDeliveryTime);
