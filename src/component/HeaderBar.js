/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable no-use-before-define */
import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  TextInput,
  Platform,
  TouchableOpacity,
} from 'react-native';
import { debounce } from 'lodash';
import IconI from 'react-native-vector-icons/Ionicons';
import IconFA from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types';
import Back from './BackButton';
import SearchIcon from './SearchIcon';
import getImage from '../utils/getImage';
import Colors from '../utils/colors';

const title = text => {
  return <Text style={styles.text}>{text}</Text>;
};

export default class HeaderBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchActive: false,
      modalVisible: false,
    };
  }

  setSearchStatus = () => {
    const { state } = this;
    this.setState({
      searchActive: !state.searchActive,
    });
  };

  setModalVisible = () => {
    const { state } = this;
    this.setState({
      modalVisible: !state.modalVisible,
    });
  };

  renderSearchBar() {
    const { props } = this;
    return (
      <View style={styles.searchBarContainer}>
        <TextInput
          style={styles.searchBar}
          autoFocus
          onChangeText={debounce(text => props.handleQueryChange(text), 500)}
          placeholder="Search"
        />
        <TouchableOpacity
          onPress={() => {
            props.handleQueryChange('');
            this.setSearchStatus();
          }}
        >
          <View style={styles.crossContainer}>
            <Image resizeMode="contain" source={getImage.crossBlack} style={styles.crossImg} />
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    const { props, state } = this;
    if (props.onlyBack === true) {
      return (
        <View style={[styles.containerHeader, styles.backOnly, props.style ? props.style : {}]}>
          <Back back={props.back} />
        </View>
      );
    }
    if (props.plusTitle === true) {
      return (
        <View style={[styles.containerHeader, styles.plusTitleContainer]}>
          <Back back={props.back} />
          {title(props.title)}
        </View>
      );
    }
    if (props.plusSearch === true) {
      return (
        <View style={[styles.containerHeader, styles.plusSearchContainer]}>
          {state.searchActive ? (
            this.renderSearchBar()
          ) : (
            <React.Fragment>
              <Back back={props.back} />
              {title(props.title)}
              <SearchIcon pressHandler={this.setSearchStatus} />
            </React.Fragment>
          )}
        </View>
      );
    }
    if (props.plusFilter === true) {
      return (
        <View style={[styles.containerHeader, styles.plusSearchContainer]}>
          <Back back={props.back} />
          {title(props.title)}
          <TouchableOpacity
            onPress={() => {
              props.handleOpenFilter();
            }}
          >
            <View style={styles.btnContainer}>
              <IconFA name="filter" size={20} color={Colors.secondaryColor} />
            </View>
          </TouchableOpacity>
        </View>
      );
    }
    if (props.home === true) {
      return (
        <View style={[styles.containerHeader, styles.homeContainer]}>
          <View style={styles.deliverToContainer}>
            <Image resizeMode="contain" source={getImage.placeholder} style={styles.pinImg} />
            <View style={styles.location}>
              <Text style={styles.deliverToText}>Deliver to</Text>

              <TouchableOpacity
                style={styles.locationContainer}
                onPress={() => {
                  props.toggleSearchLocationModal();
                }}
              >
                <View style={styles.locationContainer}>
                  <Text numberOfLines={1} ellipsizeMode="tail" style={styles.locationText}>
                    {props.location}
                  </Text>
                  <IconI name="ios-arrow-down" size={18} color={Colors.homeHeaderData} />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      );
    }
    return <View style={[styles.containerHeader, styles.onlyTitle]}>{title(props.title)}</View>;
  }
}
HeaderBar.propTypes = {
  handleQueryChange: PropTypes.func,
  style: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number])),
  onlyBack: PropTypes.bool,
  plusFilter: PropTypes.bool,
  plusTitle: PropTypes.bool,
  plusSearch: PropTypes.bool,
  home: PropTypes.bool,
  back: PropTypes.func,
  title: PropTypes.string,
  handleOpenFilter: PropTypes.func,
  toggleSearchLocationModal: PropTypes.func,
  location: PropTypes.string,
};
HeaderBar.defaultProps = {
  handleQueryChange: () => {},
  handleOpenFilter: () => {},
  toggleSearchLocationModal: () => {},
  back: () => {},
  style: undefined,
  title: undefined,
  onlyBack: false,
  plusFilter: false,
  plusTitle: false,
  plusSearch: false,
  home: false,
  location: '',
};

const { height, width } = Dimensions.get('window');
const heightRatio = height / 667.0;
const widthRatio = width / 375.0;
const styles = StyleSheet.create({
  containerHeader: {
    height: Platform.OS === 'ios' ? 70 * heightRatio : 50 * heightRatio,
    paddingTop: Platform.OS === 'ios' ? 20 * heightRatio : 0,
    paddingLeft: 12 * widthRatio,
    paddingRight: 21.8 * widthRatio,
    alignItems: 'center',
    flexDirection: 'row',
  },
  text: { flex: 1, textAlign: 'center', fontSize: 24, fontWeight: '300' },
  onlyTitle: {
    justifyContent: 'center',
  },
  backOnly: {
    justifyContent: 'flex-start',
  },
  plusTitleContainer: {
    paddingRight: 37 * widthRatio,
    justifyContent: 'space-between',
  },
  plusSearchContainer: {
    justifyContent: 'space-between',
  },
  homeContainer: { justifyContent: 'space-between' },
  searchBarContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomColor: Colors.primaryBorderColor,
    borderBottomWidth: 1,
  },
  searchBar: {
    flex: 1,
    fontSize: 15 * heightRatio,
  },
  deliverToContainer: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    flex: 1,
  },
  pinImg: {
    height: 28 * heightRatio,
    width: 15 * heightRatio,
  },
  location: {
    marginLeft: 6 * widthRatio,
    width: 159 * widthRatio,
  },
  deliverToText: { fontSize: 11 * heightRatio },
  locationContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    height: 13 * heightRatio,
    width: 153 * widthRatio,
  },
  locationText: {
    fontSize: 11 * heightRatio,
    fontWeight: '300',
    color: Colors.homeHeaderData,
  },
  profileIcon: {
    height: 21 * heightRatio,
    width: 21 * heightRatio,
  },
  crossContainer: {
    height: 20 * heightRatio,
    width: 20 * heightRatio,
    borderRadius: 10 * heightRatio,
    justifyContent: 'center',
    alignItems: 'center',
  },
  crossImg: {
    height: 10 * heightRatio,
  },
  filterImg: {
    height: 15.5 * heightRatio,
    width: 15.5 * heightRatio,
  },
  btnContainer: {
    height: 31 * heightRatio,
    width: 31 * heightRatio,
    borderRadius: 15.5 * heightRatio,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
