import { CHANGE_DESTINATION } from '../actionTypes';

const initialState = {
  destinationName: '',
  destinationCoords: [],
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CHANGE_DESTINATION: {
      return Object.assign(
        {},
        {
          destinationName: action.payload.name,
          destinationCoords: [action.payload.coords.lng, action.payload.coords.lat],
        }
      );
    }
    default: {
      return state;
    }
  }
}
