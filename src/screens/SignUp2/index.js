/* eslint-disable react/prop-types */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Button,
  Item,
  Input,
  Text,
  // FooterTab,
  Icon,
  Header,
  Left,
  Body,
  Right,
  Spinner,
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import PhoneInput from 'react-native-phone-input';
// import { Actions } from 'react-native-router-flux';
import Toast from 'react-native-easy-toast';
import { inputValidation } from '../../helper';
import { checkUserExistanceDb } from '../../apiServices/signUp';
import styles from './styles';
import Loader from '../../component/Loader';

export class SignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      lastName: '',
      address: '',
      phoneNumber: '+234',
      email: '',
      profileImage: '',
      isLoading: false,
    };
  }

  onInputChange = (value, text) => {
    this.setState({ [value]: text });
  };

  gotoTermsOfService = () => {
    const { props } = this;
    props.navigation.navigate('termsOfService');
  };

  gotoPrivacyPolicy = () => {
    const { props } = this;
    props.navigation.navigate('privacyPolicy');
  };

  onSubmitSignUp = () => {
    const { refs, state } = this;
    const { firstName, lastName, email, address, phoneNumber } = state;
    const checkInput = inputValidation({
      firstName,
      lastName,
      address,
      phoneNumber,
      email,
    });
    if (this.phone.isValidNumber()) {
      if (checkInput.result) {
        this.setState({ isLoading: true }, () => {
          this.accept();
        });
      } else {
        refs.toast.show(checkInput.message);
      }
    } else {
      refs.toast.show('Phone number invalid');
    }
  };

  stopLoader = () => {
    this.setState({ isLoading: false });
  };

  handleError = () => {
    const { refs } = this;
    refs.toast.show('Number already Exists');
    this.stopLoader();
  };

  accept() {
    const { refs, props, state } = this;
    const { firstName, lastName, email, phoneNumber, address } = state;
    if (this.phone.isValidNumber()) {
      const obj = {
        fullName: `${firstName} ${lastName}`,
        firstName,
        lastName,
        phoneNumber,
        email,
        address,
        profileImage: '',
      };
      checkUserExistanceDb(
        phoneNumber,
        obj,
        this.handleError,
        props,
        this.stopLoader
      );
    } else {
      refs.toast.show('Invalid Number');
    }
  }

  render() {
    const {
      firstName,
      lastName,
      address,
      phoneNumber,
      email,
      isLoading,
    } = this.state;
    const { navigation } = this.props;
    return (
      <Container style={styles.container}>
        <Header transparent androidStatusBarColor="#0003">
          <Left style={{ paddingHorizontal: 5 }}>
            <Button transparent onPress={() => navigation.goBack()}>
              <Icon name="close" style={styles.closeIcon} />
            </Button>
          </Left>
          <Body />
          <Right />
        </Header>
        <Loader loading={isLoading} />
        <Content
          keyboardShouldPersistTaps="never"
          padder
          style={{
            flex: 1,
            alignContent: 'space-between',
            padding: 10,
          }}>
          <View style={{ flex: 1 }}>
            <Text style={styles.welcomeHd}>Create account </Text>
          </View>

          <Grid>
            <Row style={{ justifyContent: 'space-between' }}>
              <Col style={{ width: '48%' }}>
                <Text style={styles.textLabel}>First Name</Text>
                <Item regular style={styles.textInput}>
                  <Input
                    placeholder="Enter Firstname"
                    value={firstName}
                    onChangeText={text => this.onInputChange('firstName', text)}
                  />
                </Item>
              </Col>
              <Col style={{ width: '48%' }}>
                <Text style={styles.textLabel}>Last Name</Text>
                <Item regular style={styles.textInput}>
                  <Input
                    placeholder="Enter Lastname"
                    value={lastName}
                    onChangeText={text => this.onInputChange('lastName', text)}
                  />
                </Item>
              </Col>
            </Row>
            <Row>
              <Col>
                <Text style={styles.textLabel}>Email ID</Text>
                <Item regular style={styles.textInput}>
                  <Input
                    placeholder="Enter Email"
                    value={email}
                    onChangeText={text => this.onInputChange('email', text)}
                  />
                </Item>
              </Col>
            </Row>
            <Row>
              <Col>
                <Text style={styles.textLabel}>Phone Number</Text>
                <Item regular style={styles.textInput}>
                  <PhoneInput
                    ref={ref => {
                      this.phone = ref;
                    }}
                    initialCountry="ng"
                    value={phoneNumber}
                    onChangePhoneNumber={text => {
                      this.onInputChange('phoneNumber', text);
                    }}
                    textStyle={{ fontSize: 18 }}
                    textProps={{
                      placeholder: 'Enter Phone Number',
                      autoFocus: true,
                    }}
                    autoFormat
                  // style={signInStyles.phoneInput}
                  />
                </Item>
              </Col>
            </Row>
            <Row>
              <Col>
                <Text style={styles.textLabel}>Address</Text>
                <Item regular style={styles.textInput}>
                  <Input
                    placeholder="Enter Home Address"
                    value={address}
                    onChangeText={text => this.onInputChange('address', text)}
                  />
                </Item>
              </Col>
            </Row>
            <Row>
              <Col>
                <Button
                  disabled={isLoading}
                  block
                  primary
                  style={styles.submitBtn}
                  onPress={() => this.onSubmitSignUp()}>
                  {(!isLoading && (
                    <Text style={styles.btnText}>SIGN UP</Text>
                  )) || <Spinner />}
                </Button>
              </Col>
            </Row>
          </Grid>

          <View style={{ flex: 1, bottom: 0 }}>
            <Text
              style={{
                color: '#000000',
                textAlign: 'center',
                marginTop: 30,
                marginBottom: 10,
              }}>
              <Text style={[styles.regularFont]}>
                By signing up, you agree with the{' '}
              </Text>
              <Text style={[styles.greenColor, styles.regularFont]} onPress={this.gotoTermsOfService}>
                Terms of Service{' '}
              </Text>
              <Text style={[styles.regularFont]}>and </Text>
              <Text style={[styles.greenColor, styles.regularFont]} onPress={this.gotoPrivacyPolicy}>
                Privacy Policy{' '}
              </Text>
            </Text>
          </View>
        </Content>
        <Toast
          ref="toast"
          style={{ backgroundColor: 'red' }}
          position="bottom"
          positionValue={100}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: '#ffffff', fontSize: 15 }}
        />
      </Container>
    );
  }
}

// const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  // mapStateToProps,
  null,
  mapDispatchToProps
)(SignUp);
