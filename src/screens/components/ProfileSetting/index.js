/* eslint-disable lines-between-class-members */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, Alert } from 'react-native';
import { connect } from 'react-redux';

import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Segment,
  Content,
  Thumbnail,
  Item,
  Input,
  Card,
  // Spinner,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import getImage from '../../../utils/getImage';
import styles from './styles';
import LogoutModal from '../../LogOut';
import Loader from '../../../component/Loader';
import editUser from '../../../apiServices/editUser';
import verify from '../../../apiServices/verify'
import setUser from '../../../redux/action/verify';
import { homeResetAction } from '../../../navigation/resetNavigation';

export class ProfileSetting extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      profileSegment: 1,
      logoutModal: false,
      firstName: '',
      lastName: '',
      phoneNumber: '',
      address: '',
      email: '',
      password: '',
      disableInput: true,
    };
  }

  componentDidMount() {
    const { state, props } = this;
    const { user } = props;
    this.setState({ ...state, ...user });
  }

  setLogoutModal = () => {
    this.setState(prevState => {
      return { logoutModal: !prevState.logoutModal };
    });
  };

  edit = () => {
    this.setState(prevState => {
      return { disableInput: !prevState.disableInput }
    })
  }

  editUser = () => {
    const { props, state } = this;
    this.setState({ isLoading: true });
    editUser(
      {
        firstName: state.firstName,
        lastName: state.lastName,
        phoneNumber: state.phoneNumber,
        address: state.address
      },
      props.user.id,
      props.user.jwt,
      this.handleResult
    );
  };

  handleResult = res => {
    const { props } = this;
    this.setState({ isLoading: false });
    this.updateCurrentUser(res)
    Alert.alert('Swoooosh', 'Profile updated succesfully', [
      {
        text: 'OK',
        onPress: () => Actions.HomePage(),
      },
    ]);
  };

  updateCurrentUser = (user) => {
    const { state } = this;
    verify(
      user.phoneNumber,
      user.otp,
      this.handleError,
      this.handleUserUpdate
    );
  };

  handleUserUpdate = res => {
    const { props } = this;
    const { user, jwtAccessToken } = res.data;
    props.setUserDispatcher(user, jwtAccessToken);

  };


  profileInput = (type, value) => {
    this.setState({ [type]: value }, () =>
      console.log('this.state', this.state)
    );
  };

  toggleSegment = num => this.setState({ profileSegment: num });

  handleSuccess = res => {
    const { props, refs } = this;
    this.setState({ isLoading: false });
    refs.toast.show('Wrong Verification Code');
  };

  handleError = () => {
    const { refs } = this;
    this.setState({ isLoading: false });
    refs.toast.show('Unable to update');
  };

  render() {
    const { state, props } = this;
    const {
      profileSegment,
      firstName,
      lastName,
      phoneNumber,
      address,
      email,
      password,
      disableInput,
    } = state;
    return (
      <Container>
        <Header
          transparent
          hasTabs
          style={{
            backgroundColor: '#FFFFFF',
            borderBottomColor: '#0001',
            borderBottomWidth: 1.5,
          }}
          androidStatusBarColor="#0001"
          iosBarStyle="dark-content">
          <Left>
            <Button transparent onPress={() => Actions.HomePage()}>
              <Icon
                style={{
                  paddingHorizontal: 10,
                  color: '#000000',
                }}
                name="arrow-back"
              />
            </Button>
          </Left>
          <Body style={styles.headtxtdiv}>
            <Text
              style={{
                fontSize: 18,
                color: '#030E1A',
                fontFamily: 'Nunito-SemiBold',
                width: '100%',
                textAlign: 'center',
              }}>
              Profile Setting
            </Text>
          </Body>
          <Right />
        </Header>
        <Segment style={{ backgroundColor: 'transparent', marginVertical: 10 }}>
          <Button
            first
            onPress={() => this.toggleSegment(1)}
            style={{
              borderColor: 'black',
              backgroundColor: profileSegment === 1 ? '#030E1A' : '#FFFFFF',
              justifyContent: 'center',
              borderTopLeftRadius: 5,
              borderBottomLeftRadius: 5,
              width: 150,
            }}>
            <Text
              style={{
                color: profileSegment === 1 ? '#FFFFFF' : '#030E1A',
              }}>
              Profile
            </Text>
          </Button>
          <Button
            onPress={() => this.toggleSegment(2)}
            style={{
              backgroundColor: profileSegment === 2 ? '#030E1A' : '#FFFFFF',
              justifyContent: 'center',
              borderColor: 'black',
              borderTopRightRadius: 5,
              borderBottomRightRadius: 4,
              width: 150,
            }}
            last>
            <Text
              style={{
                color: profileSegment === 2 ? '#FFFFFF' : '#030E1A',
              }}>
              Security Settings
            </Text>
          </Button>
        </Segment>
        <LogoutModal
          navigate={props.navigation}
          logoutModal={state.logoutModal}
          setLogoutModal={this.setLogoutModal}
        />
        <Content padder contentContainerStyle={{ padding: 16 }}>
          {(profileSegment === 1 && (
            <View style={{ flex: 1 }}>
              <View
                style={{
                  alignItems: 'center',
                }}>
                <View>
                  {/* <Thumbnail square large source={getImage.man} /> */}
                  <View>
                    <Button
                      block
                      style={{ backgroundColor: '#030E1A' }}
                      onPress={() => this.edit()}>
                      {/* <Text
                        style={{
                          color: '#FFFFFF',
                          fontFamily: 'Nunito-SemiBold',
                        }}>
                        EDIT
                    </Text> */}
                      <Icon
                        type="AntDesign"
                        name="edit"
                        style={{
                          color: '#FFFFFF',
                          fontSize: 16,
                          padding: 5,
                        }}
                      />
                    </Button>

                  </View>
                </View>
              </View>
              <View style={{ marginVertical: 30 }}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={[styles.inputContainer, { marginRight: 2 }]}>
                    <Text style={styles.inputLabel}>First Name</Text>
                    <Item regular style={styles.inputItem}>
                      <Input
                        disabled={disableInput}
                        placeholder="First Name"
                        placeholderTextColor="#38383888"
                        style={styles.inputText}
                        value={firstName}
                        onChangeText={text =>
                          this.profileInput('firstName', text)
                        }
                      />
                    </Item>
                  </View>
                  <View style={[styles.inputContainer, { marginLeft: 2 }]}>
                    <Text style={styles.inputLabel}>Last Name</Text>
                    <Item regular style={styles.inputItem}>
                      <Input
                        disabled={disableInput}
                        placeholder="Last Name"
                        placeholderTextColor="#38383888"
                        style={styles.inputText}
                        value={lastName}
                        onChangeText={text =>
                          this.profileInput('lastName', text)
                        }
                      />
                    </Item>
                  </View>
                </View>

                <View style={styles.inputContainer}>
                  <Text style={styles.inputLabel}>Phone number</Text>
                  <Item regular style={styles.inputItem}>
                    <Input
                      disabled={disableInput}
                      placeholder="Mobile number"
                      placeholderTextColor="#38383888"
                      style={styles.inputText}
                      value={phoneNumber}
                      onChangeText={text =>
                        this.profileInput('phoneNumber', text)
                      }
                    />
                  </Item>
                </View>
                <View style={styles.inputContainer}>
                  <Text style={styles.inputLabel}>Home Address</Text>
                  <Item regular style={styles.inputItem}>
                    <Input
                      disabled={disableInput}
                      placeholder="Address"
                      placeholderTextColor="#38383888"
                      style={styles.inputText}
                      value={address}
                      onChangeText={text => this.profileInput('address', text)}
                    />
                  </Item>
                </View>
                {!disableInput && (
                  <View style={{ marginVertical: 30 }}>
                    <Button
                      onPress={() => this.editUser()}
                      block success>
                      <Loader loading={state.isLoading} />
                      <Text
                        style={{
                          color: '#FFFFFF',
                          fontFamily: 'Nunito-SemiBold',
                        }}>
                        UPDATE
                      </Text>
                    </Button>
                  </View>
                )}
                {disableInput && (
                  <View>
                    <Button
                      block
                      style={{ backgroundColor: 'red' }}
                      onPress={() => this.setLogoutModal()}>
                      <Text
                        style={{
                          color: '#FFFFFF',
                          fontFamily: 'Nunito-SemiBold',
                        }}>
                        LOGOUT
                    </Text>
                      <Icon type="AntDesign" name="logout" />
                    </Button>
                  </View>
                )}


              </View>
            </View>
          )) || (
              <View>
                <View>
                  <Card
                    transparent
                    style={{ backgroundColor: '#F1F2F5', padding: 30 }}>
                    <Text style={{ color: '#858585' }}>
                      Your verification information helps us secure your account.
                  </Text>
                  </Card>
                </View>

                <View style={{ justifyContent: 'center', marginVertical: 20 }}>
                  <View style={styles.inputContainer}>
                    <Text style={styles.inputLabel}>Email ID</Text>
                    <Item regular style={styles.inputItem}>
                      <Input
                        disabled={disableInput}
                        placeholder="Email"
                        placeholderTextColor="#38383888"
                        style={styles.inputText}
                        value={email}
                        onChangeText={text => this.profileInput('email', text)}
                      />
                    </Item>
                  </View>
                  {!disableInput && (
                    <View style={styles.inputContainer}>
                      <Text style={styles.inputLabel}>Password</Text>
                      <Item regular style={styles.inputItem}>
                        <Input
                          placeholder="password"
                          placeholderTextColor="#38383888"
                          secureTextEntry
                          style={styles.inputText}
                          value={password}
                          onChangeText={text =>
                            this.profileInput('password', text)
                          }
                        />
                      </Item>
                    </View>
                  )}
                </View>

                {!disableInput && (
                  <View style={{ marginVertical: 30 }}>
                    <Button block success>
                      <Text
                        style={{
                          color: '#FFFFFF',
                          fontFamily: 'Nunito-SemiBold',
                        }}>
                        UPDATE
                    </Text>
                    </Button>
                  </View>
                )}
              </View>
            )}
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  user: state.currentUser,
});

const mapDispatchToProps = dispatch => {
  return {
    setUserDispatcher: (user, jwtAccessToken) => {
      dispatch(setUser(user, jwtAccessToken));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileSetting);
