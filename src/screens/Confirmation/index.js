import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import PropTypes from 'prop-types';
import styles from './style';
import getImage from '../../utils/getImage';

const Confirmation = props => {
  const select = () => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'home' })],
    });
    props.navigation.dispatch(resetAction);
  };
  return (
    <View style={styles.container}>
      <View style={styles.imgContainer}>
        <Image resizeMode="contain" source={getImage.smiling} style={styles.smileImg} />
      </View>
      <View style={styles.thanksContainer}>
        <Text style={styles.thanksText}>Thank you for your order</Text>
      </View>
      <View style={styles.confirmContainer}>
        <Text style={styles.confirmationText}>You will receive an email confirmation</Text>

        <Text style={styles.emailText}>shortly at info@yourid.com</Text>
      </View>
      <View style={styles.btnContainer}>
        <TouchableOpacity
          style={styles.homeBtn}
          onPress={() => {
            select();
          }}
        >
          <Text style={styles.homeBtnText}>Go To Home</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.trackBtn}
          onPress={() => {
            props.navigation.navigate('trackOrder');
          }}
        >
          <Text style={styles.trackBtnText}>Track your order</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
Confirmation.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};
export default Confirmation;
