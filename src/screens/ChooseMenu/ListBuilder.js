/* eslint-disable array-callback-return */
/* eslint-disable no-use-before-define */
import React, { Component } from 'react';
import { View, Text, StyleSheet, ActivityIndicator, FlatList, Platform } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import CountButton from '../../component/CountButton';
import { store } from '../../redux/configureStore';
import { heightRatio } from '../../utils/styleSheetGuide';

import Add from '../../component/AddButton';
import Empty from '../../component/EmptyList';
import Colors from '../../utils/colors';

const _ = require('lodash');

class ListBuilder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openStatus: true,
    };
    this.scrollFlag = false;
  }

  handleScroll = event => {
    const { scroll } = this.props;
    const { props } = this;
    if (event.nativeEvent.contentOffset.y > 100) {
      if (!scroll) props.handleStateChange({ scroll: true });
    } else if (scroll) props.handleStateChange({ scroll: false });
  };

  renderButton = item => {
    if (
      !_.find(store.getState().cart, {
        _id: item._id,
      })
    ) {
      return <Add item={item} />;
    }
    return <CountButton item={item} />;
  };

  render() {
    const { props, state } = this;
    const {
      vegStatus,
      vegMenu,
      nonVegMenu,
      noMoreVegItems,
      noMoreNonVegItems,
      moreVegLoading,
      moreNonVegLoading,
    } = props;
    return (
      <View style={styles.list}>
        <FlatList
          data={vegStatus ? vegMenu : nonVegMenu}
          keyExtractor={item => {
            return item._id;
          }}
          ListEmptyComponent={<Empty items></Empty>}
          showsVerticalScrollIndicator={false}
          extraData={props.cart}
          onEndReached={() => {
            if (this.scrollFlag) {
              if (vegStatus && !noMoreVegItems) {
                props.handleStateChange({ moreVegLoading: true });
                props.loadMore();
              }
              if (!vegStatus && !noMoreNonVegItems) {
                props.handleStateChange({ moreNonVegLoading: true });
                props.loadMore();
              }
              this.scrollFlag = false;
            }
          }}
          onScroll={this.handleScroll}
          onScrollBeginDrag={() => {
            this.scrollFlag = true;
          }}
          onEndReachedThreshold={0.5}
          ListFooterComponent={
            vegStatus ? (
              !noMoreVegItems && moreVegLoading ? (
                <ActivityIndicator size="small"></ActivityIndicator>
              ) : null
            ) : !noMoreNonVegItems && moreNonVegLoading ? (
              <ActivityIndicator size="small"></ActivityIndicator>
            ) : null
          }
          contentContainerStyle={{ paddingTop: 10 * heightRatio }}
          renderItem={({ item, index }) => {
            return (
              <View style={styles.menuItem} key={`key-${index}`}>
                <View style={styles.fullFlex}>
                  <Text style={styles.itemName}>{item.name}</Text>
                  <Text style={styles.itemPrice}>${item.price}</Text>
                </View>
                {state.openStatus ? this.renderButton(item) : null}
              </View>
            );
          }}
        ></FlatList>
      </View>
    );
  }
}

ListBuilder.propTypes = {
  scroll: PropTypes.bool.isRequired,
  handleStateChange: PropTypes.func.isRequired,
  loadMore: PropTypes.func.isRequired,
  vegStatus: PropTypes.bool.isRequired,
  vegMenu: PropTypes.arrayOf(
    PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number]))
  ).isRequired,
  nonVegMenu: PropTypes.arrayOf(
    PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number]))
  ).isRequired,
  noMoreNonVegItems: PropTypes.bool.isRequired,
  noMoreVegItems: PropTypes.bool.isRequired,
  moreVegLoading: PropTypes.bool.isRequired,
  moreNonVegLoading: PropTypes.bool.isRequired,
  cart: PropTypes.arrayOf(
    PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number]))
  ).isRequired,
};

const mapStateToProps = state => {
  return { cart: state.cart };
};
export default connect(
  mapStateToProps,
  null
)(ListBuilder);

const styles = StyleSheet.create({
  list: {
    flex: 1,
  },
  fullFlex: { flex: 1 },
  menuItem: Platform.select({
    ios: {
      flexDirection: 'row',
      marginVertical: 5 * heightRatio,
      marginHorizontal: 1,
      alignItems: 'center',
      padding: 10 * heightRatio,

      backgroundColor: Colors.flatListItemBgColor,
      shadowColor: Colors.primaryShadowColor,
      shadowOffset: { width: 0.3, height: 0.3 },
      shadowOpacity: 0.5,
      shadowRadius: 1,
    },
    android: {
      flexDirection: 'row',
      marginVertical: 5 * heightRatio,
      alignItems: 'center',
      padding: 10 * heightRatio,
      backgroundColor: Colors.flatListItemBgColor,
      borderWidth: 1,
      borderColor: Colors.primaryBorderColor,
    },
  }),
  itemName: {
    color: Colors.textForThemeBgColor,
    fontSize: heightRatio * 12,
    fontWeight: '500',
    marginBottom: 3 * heightRatio,
    letterSpacing: 0.4,
  },
  itemPrice: {
    fontSize: heightRatio * 10,
    color: Colors.textForThemeBgColor,
    fontWeight: '200',
    letterSpacing: 0.4,
  },
});
