import { Alert } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import HelperFetch from './HelperFetch';
/**
 * Get App Config
 * @returns {Response:true}
 */

const handleFailure = err => {
  SplashScreen.hide();
  Alert.alert('Connection Error', 'Unable connect to server...check your network connection', [
    {
      text: 'OK',
      onPress: () => { },
    },
  ]);
  throw err;
};
const appConfig = async handleSuccess => {
  const extension = `/appConfig/`;
  await HelperFetch({ extension, get: true }, handleSuccess, handleFailure);
};

export default appConfig;
