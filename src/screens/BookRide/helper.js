import Geocoder from 'react-native-geocoding';
import { getDistance } from 'geolib';

const getInitialState = () => {
  return {
    arrayData: [],
    searchLocationModalVisible: false,
    paymentModalVisible: false,
    estimate: 0,
    origin: 'Search here...',
    destination: 'Search here...',
    cabDetailId: null,
    region: {
      latitude: 12.91074,
      longitude: 77.601825,
      latitudeDelta: 0.05,
      longitudeDelta: 0.05,
    },
    id: 0,
    marker: [
      {
        title: 'pickup',
        coordinates: {
          latitude: 0,
          longitude: 0,
        },
      },
      {
        title: 'drop',
        coordinates: {
          latitude: 0,
          longitude: 0,
        },
      },
    ],
    payMethod: 'Cash',
  };
};

const pickUpInitializer = (apiKey, marker, handleResult) => {
  Geocoder.init(apiKey);
  // eslint-disable-next-line no-undef
  navigator.geolocation.getCurrentPosition(
    position => {
      const region = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
        latitudeDelta: 0.05,
        longitudeDelta: 0.05,
      };
      const mark = marker;
      mark[0] = {
        title: 'pickup',
        coordinates: {
          latitude: region.latitude,
          longitude: region.longitude,
        },
      };
      Geocoder.from([position.coords.latitude, position.coords.longitude])
        .then(json => {
          handleResult({
            origin: json.results[0].formatted_address,
            marker: mark,
            region,
          });
        })
        .catch(err => {
          throw err;
        });
    },
    error => {
      throw error;
    }
  );
};

const getRegisterOrderObject = (props, state) => {
  const obj = {
    userId: props.user.id,
    paymentMode: 'Cash',
    paymentAmount: state.estimate,
    endUserGpsLocation: state.marker[0].coordinates,
    deliveryLocation: [state.marker[1].coordinates.longitude, state.marker[1].coordinates.latitude],
    deliveryInstructions: null,
    pickUpLocation: [state.marker[0].coordinates.longitude, state.marker[0].coordinates.latitude],
    deliveryAddress: state.destination,
    pickUpAddress: state.origin,
    category: 'Cab',
  };
  return obj;
};

const handleSetAddress = (name, coords, marker, id, handleStateChange) => {
  const mark = marker;
  mark[id].coordinates = {
    latitude: coords.lat,
    longitude: coords.lng,
  };
  let dist = getDistance(marker[0].coordinates, {
    latitude: coords.lat,
    longitude: coords.lng,
  });
  dist = (dist * 1.6) / 1000;
  let delta = 0;
  if (id === 0) {
    delta = 0.05;
    handleStateChange({
      origin: name,
      marker: mark,
      id: 1,
    });
  }
  if (id === 1) {
    delta = dist * 0.018;
    handleStateChange({
      destination: name,
      marker: mark,
      id: 2,
    });
  }
  handleStateChange({
    region: {
      latitude: coords.lat,
      longitude: coords.lng,
      latitudeDelta: delta,
      longitudeDelta: delta,
    },
  });
};
export { pickUpInitializer, getRegisterOrderObject, getInitialState, handleSetAddress };
