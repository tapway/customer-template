// import { get } from 'lodash';
import { SET_TRIP_INFO, DUMP_TRIP_INFO, UPDATE_DRIVER_LOCATION } from '../actionTypes';
import returnProperty from '../helper';

const initialState = {
  _id: '',
  fullName: '',
  phoneNumber: 0,
  profileImage: '',
  otp: '',
  rating: 0,
  carNumber: '',
  driverGpsLocation: [],
  deliveryLocation: [],
  pickUpLocation: [],
  catagory: '',
  processingStatus: '',
};

export default function(state = initialState, action) {
  // debugger;

  switch (action.type) {
    case SET_TRIP_INFO: {
      const temp = {
        _id: returnProperty('_id', action.payload, state),
        fullName: returnProperty('fullName', action.payload, state),
        otp: returnProperty('otp', action.payload, state),
        rating: returnProperty('rating', action.payload, state),
        phoneNumber: returnProperty('phoneNumber', action.payload, state),
        profileImage: returnProperty('profileImage', action.payload, state),
        carNumber: returnProperty('carNumber', action.payload, state),
        driverGpsLocation: returnProperty('driverGpsLocation', action.payload, state),
        pickUpLocation: returnProperty('pickUpLocation', action.payload, state),
        deliveryLocation: returnProperty('deliveryLocation', action.payload, state),
        catagory: returnProperty('catagory', action.payload, state),
        processingStatus: returnProperty('processingStatus', action.payload, state),
      };
      return temp;
    }
    case UPDATE_DRIVER_LOCATION: {
      const copyState = JSON.parse(JSON.stringify(state));
      copyState.driverGpsLocation = action.payload.driverGpsLocation;
      return copyState;
    }
    case DUMP_TRIP_INFO: {
      return initialState;
    }
    default: {
      return state;
    }
  }
}
