import { FETCH_CONFIG, SET_CONFIG, CLEAR_CONFIG } from '../actionTypes/index';

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_CONFIG: {
      return { ...state };
    }
    case SET_CONFIG: {
      return action.payload.config;
    }
    case CLEAR_CONFIG: {
      return {};
    }
    default: {
      return state;
    }
  }
}
