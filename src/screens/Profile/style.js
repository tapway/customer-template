import { StyleSheet, Dimensions } from 'react-native';
import { heightRatio, widthRatio } from '../../utils/styleSheetGuide';
import Colors from '../../utils/colors';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.themeBackgroundColor,
  },
  profile: {
    height: 130 * heightRatio,
    flexDirection: 'row',
    backgroundColor: Colors.profileHighlightedContainer,
  },
  imgContainer: {
    width: width / 3.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  profilePic: {
    height: 64 * heightRatio,
    borderRadius: (64 / 2) * heightRatio,
    width: 64 * heightRatio,
  },
  details: {
    flex: 1,
    justifyContent: 'center',
  },
  name: {
    marginTop: 5 * heightRatio,
    fontSize: 14 * heightRatio,
    fontWeight: 'bold',
  },
  email: {
    fontSize: 10 * heightRatio,
    marginTop: 5 * heightRatio,
    marginBottom: 5 * heightRatio,
    fontWeight: '200',
  },
  phoneNumber: {
    fontSize: 10 * heightRatio,
    fontWeight: '200',
  },
  bottomContainer: {
    paddingTop: 30 * heightRatio,
    paddingLeft: 18 * widthRatio,
  },
  listText: {
    fontSize: heightRatio * 16,
    margin: heightRatio * 1.8,
    paddingBottom: heightRatio * 10,
    color: Colors.textForThemeBgColor,
  },
  signOutText: {
    fontSize: 16 * heightRatio,
    color: Colors.secondaryColor,
    margin: 1.8 * heightRatio,
    paddingBottom: 10 * heightRatio,
    fontWeight: '300',
  },
});
