// import SERVER from '../config/config';
import HelperFetch from './HelperFetch';

/**
 * Payment Capture
 * @return {success}
 */

const paymentCapture = async (
  jwtAccessToken,
  paymentId,
  amount,
  invoiceId,
  type,
  handleResult,
  handleFailure
) => {
  const handleSuccess = res => {
    if (res.success) {
      handleResult(type);
    } else {
      handleFailure();
    }
  };

  const handleError = () => {};

  const extension = `/payment/capture`;
  await HelperFetch(
    {
      extension,
      post: true,
      body: {
        paymentId,
        amount,
        invoiceId,
        type,
      },
      Authorization: jwtAccessToken,
    },
    handleSuccess,
    handleError
  );
  // eslint-disable-next-line no-undef
  // await fetch(`${SERVER}/api/payment/capture`, {
  //   method: 'POST',
  //   headers: {
  //     Accept: 'application/json',
  //     'Content-Type': 'application/json',
  //     Authorization: jwtAccessToken,
  //   },
  //   body: JSON.stringify({
  //     paymentId,
  //     amount,
  //     invoiceId,
  //     type,
  //   }),
  // })
  //   .then(res => res.json())
  //   .then(res => {
  //     if (res.success) {
  //       handleResult(type);
  //     } else {
  //       handleFailure();
  //     }
  //   })
  //   .catch(err => err);
};

export default paymentCapture;
