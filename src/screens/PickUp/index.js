import React, { PureComponent } from 'react';
import Toast from 'react-native-easy-toast';
import { connect } from 'react-redux';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import Header from '../../component/HeaderBar';
import { requestTrip } from '../../socket/index';
import styles from './style';
import GetDetailsModal from './GetDetailsModal';
import ConfirmOrderModal from './ConfirmOrderModal';
import SearchLocationModal from '../../component/SearchLocationModal';
import Map from '../../component/MapView';
import Colors from '../../utils/colors';
import { DUMP_TRIP_INFO } from '../../redux/actionTypes';
import { getInitialState, handleSetAddress, getRegisterOrderObject } from './helper';
import TopComponent from './TopComponent';
import FooterComponent from './FooterComponent';

class PickUp extends PureComponent {
  constructor(props) {
    super(props);
    this.state = getInitialState();
  }

  toggleSearchLocationModal = () => {
    this.setState(prevState => {
      return { searchLocationModalVisible: !prevState.searchLocationModalVisible };
    });
  };

  toggleGetDetailsModal = () => {
    this.setState(prevState => {
      return { getDetailsModalVisible: !prevState.getDetailsModalVisible };
    });
  };

  toggleConfirmOrderModal = () => {
    const { state, refs } = this;
    if (state.origin === 'Search here...') {
      refs.toast.show('Enter Pickup Location!');
    } else {
      this.setState(prevState => {
        return {
          getDetailsModalVisible: false,
          confirmOrderModalVisible: !prevState.confirmOrderModalVisible,
        };
      });
    }
  };

  handleLocation = id => {
    const { state } = this;
    this.setState({ id }, () => {
      if (state.confirmOrderModalVisible) this.toggleConfirmOrderModal();
      this.toggleSearchLocationModal();
    });
  };

  setLocation = (data, details) => {
    const { state } = this;
    const { id } = state;
    if (details.vicinity) {
      handleSetAddress(
        details.vicinity,
        details.geometry.location,
        state.marker,
        id,
        this.handleStateChange
      );
    } else {
      handleSetAddress(
        data.address,
        details.geometry.location,
        state.marker,
        id,
        this.handleStateChange
      );
    }
    if (state.id === 1) {
      this.toggleGetDetailsModal();
    } else if (state.id === 2) {
      this.toggleConfirmOrderModal();
    }
  };

  handleStateChange = payload => {
    this.setState(payload);
  };

  registerOrder = () => {
    const { props, state } = this;
    requestTrip(getRegisterOrderObject(props, state));
    this.toggleConfirmOrderModal();
    props.clearTripInfoDispatcher();
    props.navigation.navigate('successfullyBooked');
  };

  render() {
    const { state, props } = this;
    return (
      <View style={styles.mainContainer}>
        <GetDetailsModal
          handleStateChange={this.handleStateChange}
          getDetailsModalVisible={state.getDetailsModalVisible}
          toggleGetDetailsModal={this.toggleGetDetailsModal}
          toggleConfirmOrderModal={this.toggleConfirmOrderModal}
        />
        <ConfirmOrderModal
          confirmOrderModalVisible={state.confirmOrderModalVisible}
          origin={state.origin}
          destination={state.destination}
          tripEstimate={state.tripEstimate}
          marker={state.marker}
          toggleConfirmOrderModal={this.toggleConfirmOrderModal}
          handleLocation={this.handleLocation}
          handleStateChange={this.handleStateChange}
          registerOrder={this.registerOrder}
        />
        <SearchLocationModal
          setLocation={this.setLocation}
          searchLocationModalVisible={state.searchLocationModalVisible}
          activeModalId={state.id}
          toggleSearchLocationModal={this.toggleSearchLocationModal}
          toggleGetDetailsModal={this.toggleGetDetailsModal}
          toggleConfirmOrderModal={this.toggleConfirmOrderModal}
        />

        <Header
          plusTitle
          title={state.confirmOrderModalVisible ? 'Confirmation' : 'Select pickup'}
          back={() => props.navigation.goBack()}
        />
        <View style={styles.container}>
          <TopComponent origin={state.origin} handleLocation={this.handleLocation} />
          <Map region={state.region} marker={state.marker} />
          <FooterComponent
            id={state.id}
            getDetailsModalVisible={state.getDetailsModalVisible}
            toggleGetDetailsModal={this.toggleGetDetailsModal}
            toggleConfirmOrderModal={this.toggleConfirmOrderModal}
          />
        </View>
        <Toast
          ref="toast"
          style={{ backgroundColor: Colors.toastBgColor }}
          position="bottom"
          positionValue={400}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: Colors.toastTextColor, fontSize: 15 }}
        />
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    clearTripInfoDispatcher: () => {
      dispatch({ type: DUMP_TRIP_INFO });
    },
  };
}

PickUp.propTypes = {
  clearTripInfoDispatcher: PropTypes.func.isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  null,
  mapDispatchToProps
)(PickUp);
