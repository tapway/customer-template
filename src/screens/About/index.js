/* eslint-disable no-undef */
/* eslint-disable no-alert */
import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import Header from '../../component/HeaderBar';
import styles from './style';

const About = props => {
  return (
    <View style={styles.mainContainer}>
      <Header plusTitle title="About" back={() => props.navigation.goBack()} />
      <View style={styles.keyValueContainer}>
        <TouchableOpacity
          onPress={() => {
            alert('privacy');
          }}
        >
          <Text style={styles.key}>Privacy Policy</Text>
        </TouchableOpacity>
        <View style={styles.bottomLine} />
        <TouchableOpacity
          onPress={() => {
            alert('TOS');
          }}
        >
          <Text style={styles.key}>TOS</Text>
        </TouchableOpacity>
        <View style={styles.bottomLine} />
        <TouchableOpacity
          onPress={() => {
            alert('Contact');
          }}
        >
          <Text style={styles.key}>Contact</Text>
        </TouchableOpacity>
        <View style={styles.bottomLine} />
        <View style={styles.container}>
          <Text style={styles.key}>Version</Text>

          <Text style={styles.value}>v1.0.0</Text>
        </View>
        <View style={styles.bottomLastLine} />
      </View>
    </View>
  );
};
About.propTypes = { navigation: PropTypes.objectOf(PropTypes.any).isRequired };
export default About;
