import { StyleSheet } from 'react-native';
import { RFPercentage, RFValue } from 'react-native-responsive-fontsize';
const styles = StyleSheet.create({
  inputContainer: {
    flex: 1,
    marginVertical: 10,
    left: 6.13, right: 10.13
  },
  headtxtdiv: { position: 'absolute', left: 0, right: 0, top: RFPercentage(5.5), zIndex: -100 },
  //accord: { position: 'absolute', left: 6.13, right: 10.13, top: 25.49, bottom: 68.1 },
});

export default styles;
