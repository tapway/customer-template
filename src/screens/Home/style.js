import { StyleSheet } from 'react-native';
import { height, heightRatio, widthRatio } from '../../utils/styleSheetGuide';
import Colors from '../../utils/colors';

export default StyleSheet.create({
  swiperContainer: {
    height: 189 * heightRatio,
  },
  container: { flex: 1 },
  mainContainer: { flex: 7.5 },
  activityIndicator: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: height / 10,
  },
  orderTypeContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  orderType: {
    flex: 1,
    marginHorizontal: 10 * widthRatio,
    marginTop: 20 * heightRatio,
    marginBottom: 10 * heightRatio,
    borderRadius: 5,
    overflow: 'hidden',
  },
  orderTypeImage: {
    height: '100%',
    width: '100%',
    borderRadius: 100,
  },
  imageText: {
    fontWeight: '500',
    textAlign: 'right',
    color: Colors.white,
    fontSize: 12 * heightRatio,
    zIndex: 1,
    position: 'absolute',
    bottom: 18 * heightRatio,
    right: 9 * widthRatio,
  },
});
