import { StyleSheet } from 'react-native';
import { heightRatio } from '../../utils/styleSheetGuide';
import Colors from '../../utils/colors';
import {RFValue} from "react-native-responsive-fontsize"
import { FontNames } from '../../../theme';

export default StyleSheet.create({
  fullFlex: { flex: 1 },
  container: {
    padding: RFValue(25),
    paddingTop: 40 * heightRatio,
    flex: 1,
  },
  headings: {
    marginTop: RFValue(-20),
  },
  verificationCodeInput: {
    width:'100%',
    marginTop: RFValue(10),
    //backgroundColor:'red',
    flexDirection: 'row',
    justifyContent:'space-between',
    justifyContent: 'center',
  },
  codecontainer:{marginBottom:RFValue(15), width:'100%',flexDirection:'row',justifyContent:'space-between'},
  codeinput:{ width:RFValue(58),height:RFValue(50), borderColor: 'silver',backgroundColor:'#EFF4F5',  borderWidth: 0,borderRadius:5 },

  nextBtnContainer: {
    //alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
  },
  resendCodeText:{
    fontSize: RFValue(14),
    fontFamily: FontNames.regular,
    color: '#1B2E5A'
  },
  primarySubHeading: {
    fontFamily: FontNames.bold,
    fontSize: RFValue(24),
    color:'#030E1A'
  },
  secondarySubHeading: {
    marginTop: 10 * heightRatio,
    lineHeight: 18 * heightRatio,
    fontWeight: '100',
    fontSize: 12 * heightRatio,
    color: Colors.secondarySubHeadingVerifyScreen,
  },
  subHeading3: {
    fontSize: 17 * heightRatio,
    fontWeight: '200',
  },
  nextText: {
    fontSize: 16,
    textAlign: 'center',
    fontFamily: FontNames.semibold,
    color: Colors.textForPrimaryBtnBgColor,
  },
  nextBtn: {
    backgroundColor: '#3FAF5D',
    borderRadius: 4,
    height:RFValue(50),
    elevation:0,
    marginTop: 40,
    alignItems: 'center',
    justifyContent:'center',
    marginBottom: 30,
  },
  resendContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: 20 * heightRatio,
  },
  boldResend: {
    fontFamily:FontNames.medium,
    color: '#3FAF5D',
  },
});
