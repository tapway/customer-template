/* eslint-disable no-use-before-define */
import React, { PureComponent } from 'react';
import MapView from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styles from './style';
import { pickUpPin, destinationPin, driverLocationPin } from '../../component/MapPins';

class MapClass extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      region: {
        latitude: 12.91074,
        longitude: 77.601825,
        latitudeDelta: 0.05,
        longitudeDelta: 0.05,
      },
    };
    this.mapRef = null;
  }

  componentDidUpdate(prevProps) {
    const { props } = this;
    const { tripAccepted } = prevProps;
    if (
      props.tripAccepted.driverGpsLocation[0] !== tripAccepted.driverGpsLocation[0] ||
      props.tripAccepted.driverGpsLocation[1] !== tripAccepted.driverGpsLocation[1]
    ) {
      this.mapRef.fitToElements(true);
    }
  }

  render() {
    const { props, state } = this;
    const { tripAccepted } = props;
    return (
      <MapView
        // eslint-disable-next-line no-return-assign
        ref={c => (this.mapRef = c)}
        style={styles.map}
        provider={MapView.PROVIDER_GOOGLE}
        initialRegion={state.region}
        showsBuildings={false}
        region={state.region}
        zoomEnabled
        moveOnMarkerPress
        scrollEnabled
        mapType="standard"
      >
        {tripAccepted._id ? (
          <React.Fragment>
            {props.orderStatus >= 1 ? (
              <MapView.Marker
                coordinate={{
                  latitude: tripAccepted.driverGpsLocation[1],
                  longitude: tripAccepted.driverGpsLocation[0],
                }}
              >
                {driverLocationPin()}
              </MapView.Marker>
            ) : null}
            {props.orderStatus < 1 ? (
              <MapView.Marker
                coordinate={{
                  latitude: tripAccepted.pickUpLocation[1],
                  longitude: tripAccepted.pickUpLocation[0],
                }}
              >
                {pickUpPin()}
              </MapView.Marker>
            ) : null}
            <MapView.Marker
              coordinate={{
                latitude: tripAccepted.deliveryLocation[1],
                longitude: tripAccepted.deliveryLocation[0],
              }}
            >
              {destinationPin()}
            </MapView.Marker>
            {props.orderStatus >= 1 ? (
              <MapViewDirections
                origin={{
                  latitude: tripAccepted.driverGpsLocation[1],
                  longitude: tripAccepted.driverGpsLocation[0],
                }}
                destination={{
                  latitude: tripAccepted.deliveryLocation[1],
                  longitude: tripAccepted.deliveryLocation[0],
                }}
                apikey={props.config.googleMapsApiKey}
                strokeWidth={3}
                strokeColor="black"
              />
            ) : null}
          </React.Fragment>
        ) : null}
      </MapView>
    );
  }
}

const mapStateToProps = state => {
  return {
    config: state.config,
  };
};
MapClass.propTypes = {
  tripAccepted: PropTypes.objectOf(PropTypes.any).isRequired,
  orderStatus: PropTypes.number.isRequired,
  config: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ).isRequired,
};
export default connect(
  mapStateToProps,
  null
)(MapClass);
