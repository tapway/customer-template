/* eslint-disable no-use-before-define */
import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, ActivityIndicator, Platform } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { heightRatio } from '../../utils/styleSheetGuide';
import { store } from '../../redux/configureStore';
import CountButton from '../../component/CountButton';
import Add from '../../component/AddButton';
import Empty from '../../component/EmptyList';
import Colors from '../../utils/colors';

const _ = require('lodash');

class ListBuilder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openStatus: true,
    };
    this.scrollFlag = false;
  }

  renderButton = item => {
    if (
      !_.find(store.getState().cart, {
        _id: item._id,
      })
    ) {
      return <Add item={item} />;
    }
    return <CountButton item={item} />;
  };

  handleScroll = event => {
    const { props } = this;
    if (event.nativeEvent.contentOffset.y > 100) {
      if (!props.scroll) props.handleStateChange({ scroll: true });
    } else if (props.scroll) props.handleStateChange({ scroll: false });
  };

  render() {
    const { props, state } = this;
    const { itemList, moreLoading, noMoreItems } = props;
    return (
      <View style={styles.list}>
        <FlatList
          data={itemList}
          keyExtractor={item => {
            return item._id;
          }}
          bounces
          ListEmptyComponent={<Empty items></Empty>}
          showsVerticalScrollIndicator={false}
          extraData={props.cart}
          onEndReached={() => {
            if (!noMoreItems && this.scrollFlag) {
              props.handleStateChange({ moreLoading: true });
              props.loadMore();
              this.scrollFlag = false;
            }
          }}
          onEndReachedThreshold={0.5}
          onScroll={this.handleScroll}
          onScrollBeginDrag={() => {
            this.scrollFlag = true;
          }}
          ListFooterComponent={
            !noMoreItems && moreLoading ? (
              <ActivityIndicator size="small"></ActivityIndicator>
            ) : null
          }
          contentContainerStyle={{ paddingTop: 10 * heightRatio }}
          renderItem={({ item, index }) => {
            return (
              <View style={styles.menuItem} key={`key-${index}`}>
                <View style={styles.fullFlex}>
                  <Text style={styles.itemName}>{item.name}</Text>

                  <View style={styles.rowFlex}>
                    <Text style={styles.itemBrand}>Brand :</Text>
                    <Text style={styles.itemBrandTag}> {item.brand}</Text>
                  </View>
                  <View style={styles.rowFlex}>
                    <Text style={styles.itemBrand}>Price :</Text>
                    <Text style={styles.itemPrice}> ${item.price}</Text>
                  </View>
                </View>
                {state.openStatus ? this.renderButton(item) : null}
              </View>
            );
          }}
        ></FlatList>
      </View>
    );
  }
}

ListBuilder.propTypes = {
  scroll: PropTypes.bool.isRequired,
  handleStateChange: PropTypes.func.isRequired,
  itemList: PropTypes.arrayOf(
    PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number]))
  ).isRequired,
  cart: PropTypes.arrayOf(
    PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number]))
  ).isRequired,
  moreLoading: PropTypes.bool.isRequired,
  noMoreItems: PropTypes.bool.isRequired,
  loadMore: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
  return { cart: state.cart };
};
export default connect(
  mapStateToProps,
  null
)(ListBuilder);

const styles = StyleSheet.create({
  list: {
    flex: 1,
  },
  fullFlex: {
    flex: 1,
  },
  menuItem: Platform.select({
    ios: {
      flexDirection: 'row',
      marginTop: 10 * heightRatio,
      marginHorizontal: 1,
      alignItems: 'center',
      padding: 10 * heightRatio,
      backgroundColor: Colors.flatListItemBgColor,
      shadowColor: Colors.primaryShadowColor,
      shadowOffset: { width: 0.3, height: 0.3 },
      shadowOpacity: 0.3,
      shadowRadius: 1,
    },
    android: {
      flexDirection: 'row',
      marginTop: 10 * heightRatio,
      alignItems: 'center',
      padding: 10 * heightRatio,
      backgroundColor: Colors.flatListItemBgColor,
      borderWidth: 1,
      borderColor: Colors.primaryBorderColor,
    },
  }),
  rowFlex: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
  },
  itemName: {
    color: Colors.textForThemeBgColor,
    fontSize: 12 * heightRatio,
    fontWeight: '500',
    marginBottom: 3 * heightRatio,
    letterSpacing: 0.4,
  },
  itemBrand: {
    fontSize: heightRatio * 10,
  },
  itemBrandTag: {
    fontSize: heightRatio * 10,
    fontWeight: '400',
  },
  itemPrice: {
    fontSize: heightRatio * 10,
    color: Colors.textForThemeBgColor,
    marginTop: 4 * heightRatio,
    fontWeight: '400',
  },
});
