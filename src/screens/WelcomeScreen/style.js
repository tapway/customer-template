import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../utils/colors';
import {FontNames} from "../../../theme/"
import { RFValue } from 'react-native-responsive-fontsize';

const { height, width } = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textView: {
    flex: 0.6,
    justifyContent: 'flex-end',
  },
  imageView: {
    flex: 1,
    justifyContent: 'center',
  },
  buttonView: {
    flex: 0.6,
    justifyContent: 'flex-start',
  },
  name: {
    textAlign: 'center',
    fontSize: 50,
    color: colors.black,
  },
  getStartedButton: {
    height: height / 15,
    width: width / 2,
    justifyContent: 'center',
    borderColor: colors.primaryColor,
    borderWidth: 1,
    borderRadius: 50,
  },
  buttonText: {
    textAlign: 'center',
    fontSize: 25,
  },
  activityIndicator: {
    marginTop: height / 8.33,
  },
});

export const welcomeStyles = StyleSheet.create({
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB',
    padding: 15,
  },
  title: {
    fontFamily: FontNames.semibold,
    color: '#FFFFFF',
    fontSize:  RFValue(25),
    textAlign: 'center',
    marginTop: 20,
    marginVertical: 10,
  },
  // image: {
  //   marginBottom: 30,
  // },
  text: {
    fontFamily: FontNames.regular,
    color: '#FFFFFF',
    fontSize: RFValue(18),
    opacity: 0.7,
    textAlign: 'center',
    marginVertical: 10,
  },
});
