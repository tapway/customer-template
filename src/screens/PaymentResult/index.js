import React, { PureComponent } from 'react';
import { View, Text, Image } from 'react-native';
import PropTypes from 'prop-types';
import getImage from '../../utils/getImage';
import styles from './style';

export default class PaymentResult extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
    this.timeOut = null;
  }

  componentDidMount() {
    const { props } = this;
    const successfull = props.navigation.getParam('successfull');
    if (successfull) {
      this.timeOut = setTimeout(() => {
        props.navigation.navigate('successfullyBooked');
      }, 3000);
    } else {
      this.timeOut = setTimeout(() => {
        props.navigation.goBack();
      }, 3000);
    }
  }

  componentWillUnmount() {
    clearTimeout(this.timeOut);
  }

  render() {
    const { props } = this;
    const successfull = props.navigation.getParam('successfull');
    return (
      <View style={styles.container}>
        <Image
          source={successfull ? getImage.successful : getImage.unsuccessful}
          resizeMode="contain"
          style={styles.image}
        />
        <Text style={styles.descriptionText}>Payment {successfull ? 'Successfull' : 'Failed'}</Text>
      </View>
    );
  }
}
PaymentResult.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};
