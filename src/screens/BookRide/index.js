import React, { Component } from 'react';
import Toast from 'react-native-easy-toast';
import { getDistance } from 'geolib';
import { View } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Header from '../../component/HeaderBar';
import estimate from '../../config/estimate';
import styles from './style';
import PaymentModal from './PaymentModal';
import SearchLocationModal from '../../component/SearchLocationModal';
import { requestTrip } from '../../socket/index';
import Map from '../../component/MapView';
import Colors from '../../utils/colors';
import { DUMP_TRIP_INFO } from '../../redux/actionTypes';
import TopComponent from './TopComponent';
import FooterComponent from './FooterComponent';
import {
  pickUpInitializer,
  getRegisterOrderObject,
  getInitialState,
  handleSetAddress,
} from './helper';

class BookRide extends Component {
  constructor(props) {
    super(props);
    this.state = getInitialState();
  }

  componentDidMount = async () => {
    const { state, props } = this;
    pickUpInitializer(props.config.googleMapsApiKey, state.marker, this.handleStateChange);
  };

  setLocation = (data, details) => {
    const { state } = this;
    const { id } = state;
    if (details.vicinity) {
      handleSetAddress(
        details.vicinity,
        details.geometry.location,
        state.marker,
        id,
        this.handleStateChange
      );
    } else {
      handleSetAddress(
        data.address,
        details.geometry.location,
        state.marker,
        id,
        this.handleStateChange
      );
    }
  };

  checkEstimate = () => {
    this.setState(prevState => {
      return {
        estimate:
          (estimate.perKm *
            getDistance(prevState.marker[0].coordinates, prevState.marker[1].coordinates)) /
          1000,
      };
    });
  };

  toggleSearchLocationModal = () => {
    this.setState(prevState => {
      return { searchLocationModalVisible: !prevState.searchLocationModalVisible };
    });
  };

  togglePaymentModal = () => {
    this.setState(prevState => {
      return { paymentModalVisible: !prevState.paymentModalVisible };
    });
  };

  handleLocation = id => {
    this.setState({ id });
    this.toggleSearchLocationModal();
  };

  handleStateChange = payload => {
    this.setState(payload);
  };

  registerOrder = () => {
    const { props, state } = this;
    requestTrip(getRegisterOrderObject(props, state));
    this.togglePaymentModal();
    props.clearTripInfoDispatcher();
    props.navigation.navigate('successfullyBooked', { type: 'Cab' });
  };

  payMethodChange = value => {
    this.setState({ payMethod: value });
  };

  render() {
    const { props, state, refs } = this;
    return (
      <View style={styles.fullFlex}>
        <PaymentModal
          paymentModalVisible={state.paymentModalVisible}
          togglePaymentModal={this.togglePaymentModal}
          estimate={state.estimate}
          marker={state.marker}
          payMethod={state.payMethod}
          payMethodChange={this.payMethodChange}
          handleStateChange={this.handleStateChange}
          registerOrder={this.registerOrder}
        />
        <SearchLocationModal
          pickupDrop
          setLocation={this.setLocation}
          searchLocationModalVisible={state.searchLocationModalVisible}
          handleStateChange={this.handleStateChange}
          toggleSearchLocationModal={this.toggleSearchLocationModal}
        />
        <Header plusTitle title="Book a ride" back={() => props.navigation.goBack()} />
        <View style={styles.mainContainer}>
          <TopComponent
            handleLocation={this.handleLocation}
            origin={state.origin}
            destination={state.destination}
          />
          <Map region={state.region} marker={state.marker} />
          <FooterComponent
            paymentModalVisible={state.paymentModalVisible}
            origin={state.origin}
            destination={state.destination}
            toast={refs.toast}
            checkEstimate={this.checkEstimate}
            togglePaymentModal={this.togglePaymentModal}
          />
        </View>
        <Toast
          ref="toast"
          style={{ backgroundColor: Colors.toastBgColor }}
          position="bottom"
          positionValue={400}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: Colors.toastTextColor, fontSize: 15 }}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    config: state.config,
  };
};
function mapDispatchToProps(dispatch) {
  return {
    clearTripInfoDispatcher: () => {
      dispatch({ type: DUMP_TRIP_INFO });
    },
  };
}

BookRide.propTypes = {
  clearTripInfoDispatcher: PropTypes.func.isRequired,
  config: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ).isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookRide);
