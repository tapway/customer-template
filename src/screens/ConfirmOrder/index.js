import React, { PureComponent } from 'react';
import { Text, View, Image, ScrollView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import PaymentMethod from './PaymentMethod';
import { DELIVERY_CHARGES, tax, MIN_DEL_CHARGES } from '../../config/estimate';
import styles from './style';
import ProductList from './ProductList';
import paymentPayPal from '../../config/paypal';
import MenuList from './MenuList';
import getImage from '../../utils/getImage';
import Header from '../../component/HeaderBar';
import payment from '../../config/payment';
import setRequest from '../../redux/action/request';
import { requestTrip } from '../../socket/index';
import Button from '../../component/Button';
import SearchLocationModal from '../../component/SearchLocationModal';
import setDestination from '../../redux/action/destination';
import { CLEAR_CART, DUMP_TRIP_INFO, DUMP_REQUEST } from '../../redux/actionTypes';

class ConfirmOrder extends PureComponent {
  constructor() {
    super();
    this.state = {
      paypalCheck: false,
      codCheck: false,
      searchLocationModalVisible: false,
      price: '',
      deliveryCharges: '',
      estimatePrice: '',
      type: 'confirmclass',
      othersCheck: false,
      deliverLocation: [],
      deliveryAddress: '',
    };
  }

  componentDidMount() {
    this.getPrice();
  }

  selectMethod = value => {
    this.setState({ paypalCheck: false, codCheck: false, othersCheck: false });
    if (value === 'Paypal') this.setState({ paypalCheck: true });
    if (value === 'Cash On Delivery') this.setState({ codCheck: true });
    if (value === 'Others') this.setState({ othersCheck: true });
  };

  handleResult = paymentMode => {
    const { props, state } = this;
    const shop = props.navigation.getParam('shop');
    const obj = {
      userId: props.user.id,
      paymentMode,
      paymentAmount: state.price,
      deliveryLocation: props.destination.destinationCoords,
      deliveryInstructions: { itemDetails: props.cart },
      pickUpLocation: shop.location,
      deliveryAddress: props.destination.destinationName,
      pickUpAddress: shop.address,
      category: shop.shopType,
    };
    requestTrip(obj);
    props.clearTripInfoDispatcher();
    props.clearCartDispatcher();
    props.navigation.navigate('paymentResult', { successfull: true });
  };

  handleFailure = () => {
    const { props } = this;
    props.navigation.navigate('paymentResult', { successfull: false });
  };

  registerOrder = paymentMode => {
    this.handleResult(paymentMode);
  };

  handlePaymentFailure = () => {
    const { props } = this;
    props.navigation.navigate('paymentResult', { successfull: false });
  };

  selectPayment = () => {
    const { paypalCheck, codCheck, othersCheck } = this.state;
    const { deliverLocation, deliveryAddress, price } = this.state;
    const { props, state } = this;
    const shop = props.navigation.getParam('shop');
    const { cart, user } = props;
    props.setRequestDispatcher(shop, cart, user.id, deliverLocation, deliveryAddress);
    if (paypalCheck === true) {
      const success = paymentPayPal(
        price,
        'USD',
        'Payment To DeliveryZ',
        this.registerOrder,
        props.clearCartDispatcher,
        this.handlePaymentFailure
      );
      if (success) {
        this.setState({ checked: true });
      }
    }
    if (codCheck === true) {
      this.handleResult('Cash');
    }
    if (othersCheck === true) {
      payment(
        this.registerOrder,
        state.type,
        props.clearCartDispatcher,
        this.handlePaymentFailure,
        price,
        'USD'
      );
    }
  };

  toggleSearchLocationModal = () => {
    this.setState(prevState => {
      return { searchLocationModalVisible: !prevState.searchLocationModalVisible };
    });
  };

  getPrice = () => {
    let price = 0;
    let deliveryCharges = 0;
    const { cart } = this.props;
    cart.map(item => {
      price += item.price * item.quantity;
      return item;
    });
    if (price > MIN_DEL_CHARGES) {
      deliveryCharges = 0;
    } else {
      deliveryCharges = DELIVERY_CHARGES;
    }
    price = price + (price * tax) / 100 + (price * DELIVERY_CHARGES) / 100;
    price = Math.round(price * 100) / 100;
    this.setState({ price, deliveryCharges });
  };

  render() {
    const { props, state } = this;
    const { cart } = props;
    const { paypalCheck, codCheck, othersCheck } = this.state;
    const shop = props.navigation.getParam('shop');
    return (
      <View style={styles.container}>
        <Header
          plusTitle
          title="Confirm Order"
          back={() => {
            props.navigation.goBack();
            props.dumpRequest();
          }}
        />
        <SearchLocationModal
          changeDestination
          searchLocationModalVisible={state.searchLocationModalVisible}
          setAddress={props.editDestination}
          toggleSearchLocationModal={this.toggleSearchLocationModal}
        />
        <View style={styles.locationView}>
          <View style={styles.pickUpConatiner}>
            <View style={styles.iconContainer}>
              <Image resizeMode="contain" style={styles.image} source={getImage.blackDot} />
            </View>
            <View style={styles.pickUpHeadingContainer}>
              <Text style={styles.headingPickUpText}>Pickup from</Text>
              <View style={styles.pickUpDescriptionContainer}>
                <Text numberOfLines={1} style={styles.pickUpDescriptionText}>
                  {shop.address}
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.pickUpConatiner}>
            <View style={styles.iconContainer}>
              <Image
                resizeMode="contain"
                style={styles.pickupImage}
                source={getImage.placeholder}
              />
            </View>
            <View style={styles.pickUpHeadingContainer}>
              <Text style={styles.headingPickUpText}>Drop location</Text>
              <TouchableOpacity
                style={styles.dropDescriptionContainer}
                onPress={() => this.toggleSearchLocationModal()}
              >
                <Text numberOfLines={1} style={styles.pickUpDescriptionText}>
                  {props.destination.destinationName}
                </Text>
                <Image resizeMode="contain" source={getImage.search} style={styles.pencilImg} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <ScrollView style={styles.mainContainer} showsVerticalScrollIndicator={false}>
          <View style={styles.productsContainer}>
            {shop.shopType === 'Food' ? (
              <React.Fragment>
                <Text style={styles.productsHeading}>Menu List</Text>
                <MenuList data={cart} />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <Text style={styles.productsHeading}>Ordered Products</Text>
                <ProductList data={cart} />
              </React.Fragment>
            )}
          </View>
          <View style={styles.invoiceConatiner}>
            <Text style={styles.productsHeading}>Invoice</Text>
            <View style={styles.partnerChargesContainer}>
              <Text style={styles.partnerChargesText}>Partner Delivery Charges :</Text>
              <Text style={styles.partnerChargesText}>
                {state.deliveryCharges ? `${state.deliveryCharges}%` : 'Free'}
              </Text>
            </View>
            <View style={styles.partnerChargesContainer}>
              <Text style={styles.partnerChargesText}>Tax :</Text>
              <Text style={styles.partnerChargesText}>{tax}%</Text>
            </View>
            <View style={styles.grandTotalContainer}>
              <Text style={styles.partnerChargesText}>Item Total :</Text>
              <View style={styles.paidConatiner}>
                <Text style={styles.paidTextSecond}>${state.price}</Text>
              </View>
            </View>
            <PaymentMethod label="Paypal" selected={paypalCheck} selectMethod={this.selectMethod} />
            <PaymentMethod
              label="Cash On Delivery"
              selected={codCheck}
              selectMethod={this.selectMethod}
            />
            <PaymentMethod label="Others" selected={othersCheck} selectMethod={this.selectMethod} />
          </View>
        </ScrollView>
        <View style={styles.estimateContainer}>
          <Button title="Confirm" submit={this.selectPayment} />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    cart: state.cart,
    destination: state.destination,
    user: state.currentUser,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setRequestDispatcher: (shop, cart, id, loc, add) => {
      dispatch(setRequest(shop, cart, id, loc, add));
    },
    dumpRequest: () => {
      dispatch({ type: DUMP_REQUEST });
    },
    editDestination: (name, coords) => {
      dispatch(setDestination(name, coords));
    },
    clearTripInfoDispatcher: () => {
      dispatch({ type: DUMP_TRIP_INFO });
    },
    clearCartDispatcher: () => {
      dispatch({ type: CLEAR_CART });
    },
  };
};

ConfirmOrder.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  user: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.array, PropTypes.bool])
  ).isRequired,
  destination: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.array]))
    .isRequired,
  cart: PropTypes.arrayOf(
    PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number]))
  ).isRequired,
  clearCartDispatcher: PropTypes.func.isRequired,
  clearTripInfoDispatcher: PropTypes.func.isRequired,
  setRequestDispatcher: PropTypes.func.isRequired,
  dumpRequest: PropTypes.func.isRequired,
  editDestination: PropTypes.func.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfirmOrder);
