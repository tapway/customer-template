import { StackActions, NavigationActions } from 'react-navigation';

const homeResetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'scenes' })],
});

const loginResetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'login' })],
});

export { homeResetAction, loginResetAction };
