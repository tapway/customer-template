import { StyleSheet, Platform } from 'react-native';
import { heightRatio } from '../../utils/styleSheetGuide';
import Colors from '../../utils/colors';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  mainContainer: { flex: 8, margin: 50 },
  headings: {
    height: heightRatio * 100,
    justifyContent: 'flex-end',
    bottom: 20,
  },
  mobileNumberInput: {
    height: heightRatio * 300,
    justifyContent: 'flex-start',
  },
  nextBtnContainer: {
    justifyContent: 'flex-end',
    flex: 1,
    alignItems: 'center',
  },
  mobileNumberText: {
    fontWeight: '200',
    marginBottom: heightRatio * 2,
    fontSize: 22,
  },
  nextBtnText: {
    fontSize: 20,
    textAlign: 'center',
    color: Colors.textForPrimaryBtnBgColor,
  },
  input: {
    fontWeight: '200',
    height: 40,
    borderBottomColor: Colors.primaryBorderColor,
    borderBottomWidth: 1,
    fontSize: Platform.OS === 'ios' ? 23 : 18,
    marginTop: 10 * heightRatio,
  },
  button1: {
    height: 50,
    width: 330,
    justifyContent: 'center',
    borderColor: Colors.primaryBtnBgColor,
    backgroundColor: Colors.primaryBtnBgColor,
    borderWidth: 1,
    borderRadius: 25,
  },
  modalInputContainer: {
    flex: 1,
  },
});
