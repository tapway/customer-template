import React, { PureComponent } from 'react';
import { View, Text, Image, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types';
import getImage from '../../utils/getImage';
import styles from './style';
import FoodTypeBtn from '../../component/FoodTypeBtn';
import { heightRatio } from '../../utils/styleSheetGuide';

class TopComponent extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      openStatus: true,
    };
    this.animatedHeight = new Animated.Value(1);
    this.animatedOpacity = new Animated.Value(1);
  }

  componentWillMount() {
    const { props } = this;
    const { shop } = props;
    const currentHr = new Date().getHours();
    const currentMin = new Date().getMinutes();
    const { hr: shopOpeningHr, min: shopOpeningMin } = shop.openingTime;
    const { hr: shopClosingHr, min: shopClosingMin } = shop.closingTime;
    if (currentHr > shopOpeningHr && currentHr < shopClosingHr) {
      return 'Open';
    }
    if (currentHr === shopOpeningHr && currentMin > shopOpeningMin) {
      return 'Open';
    }
    if (currentHr === shopClosingHr && currentMin < shopClosingMin) {
      return 'Open';
    }
    this.setState({ openStatus: false });
    props.handleStateChange({ openStatus: false });
    return 'Closed';
  }

  componentWillUpdate(nextProps) {
    const { props } = this;
    const { scroll, vegStatus, vegMenu, nonVegMenu } = props;
    if (vegStatus && vegMenu.length > 7) {
      if (scroll && !nextProps.scroll) {
        this.animateIn();
      }
      if (!scroll && nextProps.scroll) {
        this.animateOut();
      }
    }
    if (!vegStatus && nonVegMenu.length > 7) {
      if (scroll && !nextProps.scroll) {
        this.animateIn();
      }
      if (!scroll && nextProps.scroll) {
        this.animateOut();
      }
    }
  }

  animateIn = () => {
    this.animatedHeight.setValue(0);
    this.animatedOpacity.setValue(0);
    Animated.parallel([
      Animated.timing(this.animatedHeight, {
        toValue: 1,
        duration: 500,
        easing: Easing.ease,
      }),
      Animated.timing(this.animatedOpacity, {
        toValue: 1,
        duration: 500,
        easing: Easing.ease,
        delay: 500,
      }),
    ]).start();
  };

  animateOut = () => {
    this.animatedHeight.setValue(1);
    this.animatedOpacity.setValue(1);
    Animated.parallel([
      Animated.timing(this.animatedHeight, {
        toValue: 0,
        duration: 500,
        easing: Easing.ease,
        delay: 500,
      }),
      Animated.timing(this.animatedOpacity, {
        toValue: 0,
        duration: 500,
        easing: Easing.ease,
      }),
    ]).start();
  };

  render() {
    const { props, state } = this;
    const { shop, vegStatus } = props;
    const interpolatedHeight = this.animatedHeight.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 170 * heightRatio],
    });
    return (
      <Animated.View style={{ height: interpolatedHeight, opacity: this.animatedOpacity }}>
        <View style={styles.restaurantDetails}>
          <Text style={styles.resName}>{shop.name}</Text>
          <Text style={[styles.secondaryColor, styles.resAddress]}>{shop.address}</Text>

          <View style={styles.bottomLine} />
          <View style={styles.deliveryDetails}>
            <Image resizeMode="contain" source={getImage.deliveryTime} style={styles.iconTime} />

            <Text style={[styles.secondaryColor, styles.delType]}>
              {`${state.openStatus ? 'Open' : 'Closed'}, Timings : ${
                shop.openingTime.hr
              } AM - ${shop.closingTime.hr - 12} PM`}
            </Text>
          </View>
          <View style={styles.deliveryDetails}>
            <View>
              <Image
                resizeMode="contain"
                source={getImage.deliveryTruck}
                style={styles.iconTruck}
              />
            </View>
            <View>
              <Text style={[styles.secondaryColor, styles.delType]}>Free Delivery</Text>
            </View>
          </View>
        </View>
        <View style={styles.itemList}>
          <View style={styles.foodType}>
            <FoodTypeBtn vegStatus={vegStatus} changeFoodType={props.changeFoodType} />
          </View>
        </View>
      </Animated.View>
    );
  }
}
TopComponent.propTypes = {
  changeFoodType: PropTypes.func.isRequired,
  shop: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.array, PropTypes.object])
  ).isRequired,
  handleStateChange: PropTypes.func.isRequired,
  scroll: PropTypes.bool.isRequired,
  vegStatus: PropTypes.bool.isRequired,
  vegMenu: PropTypes.arrayOf(
    PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number]))
  ).isRequired,
  nonVegMenu: PropTypes.arrayOf(
    PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number]))
  ).isRequired,
};
export default Animated.createAnimatedComponent(TopComponent);
