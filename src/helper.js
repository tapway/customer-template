/* eslint-disable no-useless-escape */
/* eslint-disable no-param-reassign */
/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */

const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

function inputValidation(input) {
  for (const property1 in input) {
    if (input[property1] === undefined) {
      return { message: `${property1} is undefined`, result: false };
    }
    if (input[property1] === '') {
      return { message: `${property1} cannot be Empty`, result: false };
    }
    if (property1.includes('Name' || 'name') && input[property1].length > 15) {
      return {
        message: `${property1} Must be 15 characters or less`,
        result: false,
      };
    }
    if (!mailformat.test(input.email)) {
      return {
        message: `Invalid email address`,
        result: false,
      };
    }
  }
  return { message: 'complete', result: true };
}

function objectReset(obj) {
  for (const property in obj) {
    obj[property] = '';
  }
  return obj;
}

export { inputValidation, objectReset };
