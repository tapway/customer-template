import { SET_CONFIG } from '../actionTypes';

/**
 * Set Config To Redux
 */

const setConfig = config => {
  return {
    type: SET_CONFIG,
    payload: {
      config,
    },
  };
};
export default setConfig;
