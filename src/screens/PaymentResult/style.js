import { StyleSheet } from 'react-native';
import { heightRatio, widthRatio } from '../../utils/styleSheetGuide';
import Colors from '../../utils/colors';

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.themeBackgroundColor,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    height: heightRatio * 200,
    width: widthRatio * 200,
  },
  descriptionText: {
    textAlign: 'center',
    fontSize: heightRatio * 25,
    color: Colors.textForThemeBgColor,
    marginTop: heightRatio * 20,
    fontWeight: '200',
  },
});

export default styles;
