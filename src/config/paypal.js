import PayPal from 'react-native-paypal-wrapper';
import { store } from '../redux/configureStore';

// Required for Future Payments
const options = {
  merchantName: 'GeekyAnts',
  merchantPrivacyPolicyUri: 'https://geekyants.com/',
  merchantUserAgreementUri: 'https://geekyants.com/',
};

const payment = async (
  price,
  currency,
  description,
  registerOrder,
  clearCartDispatcher,
  handlePaymentFailure
) => {
  const { PAY_PAL_CLIENT_ID } = store.getState().config;
  PayPal.initializeWithOptions(PayPal.NO_NETWORK, PAY_PAL_CLIENT_ID, options);
  PayPal.pay({
    price,
    currency,
    description,
  })
    .then(confirm => {
      registerOrder('Paypal', confirm);
      clearCartDispatcher();
      return true;
    })
    .catch(() => {
      handlePaymentFailure();
      return false;
    });
};

export default payment;
