import React from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import styles from './style';
import getImage from '../../utils/getImage';

const FooterComponent = props => {
  const { origin, destination, paymentModalVisible, toast } = props;
  return (
    <View style={styles.footer}>
      {!paymentModalVisible && (
        <TouchableOpacity
          onPress={() => {
            if (origin === 'Search here...') toast.show('Pickup Location is required!');
            else if (destination === 'Search here...') toast.show('Drop Location is required');
            else {
              props.checkEstimate();
              props.togglePaymentModal();
            }
          }}
        >
          <Image
            resizeMode="contain"
            source={getImage.footerNavigationIcon}
            style={styles.footerImg}
          />
        </TouchableOpacity>
      )}
    </View>
  );
};
FooterComponent.propTypes = {
  origin: PropTypes.string.isRequired,
  destination: PropTypes.string.isRequired,
  paymentModalVisible: PropTypes.bool.isRequired,
  checkEstimate: PropTypes.func.isRequired,
  togglePaymentModal: PropTypes.func.isRequired,
  toast: PropTypes.objectOf(PropTypes.object),
};
FooterComponent.defaultProps = {
  toast: undefined,
};
export default FooterComponent;
