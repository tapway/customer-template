import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { uniqBy } from 'lodash';
import PropTypes from 'prop-types';
import Header from '../../component/HeaderBar';
import styles from './style';
import setOrderHistory from '../../redux/action/orderHistory';
import Details from './Details';
import fetchOrderHistoryDb from '../../apiServices/orderHistory';
import Loading from './Loading';
import { DUMP_TRIP_INFO } from '../../redux/actionTypes';

const fetchCount = 10;

class OrderHistory extends Component {
  loadingComponent: Promise<React.Element<*>>;

  constructor(props) {
    super(props);
    this.state = {
      orderHistory: [],
      loaded: true,
      noMoreHistory: false,
    };
    this.trackOrder = this.trackOrder.bind(this);
  }

  async componentDidMount() {
    const { props, state } = this;
    this.loadingComponent = new Promise(resolve => {
      fetchOrderHistoryDb(
        props.user.jwt,
        props.user.id,
        fetchCount,
        state.orderHistory.length,
        this.setOrderHistory,
        this.loader,
        resolve
      );
    });
  }

  loadMoreHistory = async () => {
    const { props, state } = this;
    await fetchOrderHistoryDb(
      props.user.jwt,
      props.user.id,
      fetchCount,
      state.orderHistory.length,
      this.setOrderHistory,
      this.loader
    );
  };

  setOrderHistory = payload => {
    if (payload.length < fetchCount) {
      this.setState({ noMoreHistory: true });
    }
    this.setState(prevState => {
      const order = prevState.orderHistory;
      const arr = uniqBy([...order, ...payload], '_id');
      return { orderHistory: arr };
    });
  };

  loader = () => {
    this.setState({
      loaded: false,
    });
  };

  trackOrder = id => {
    const { props } = this;
    props.clearTripInfoDispatcher();
    props.navigation.navigate('successfullyBooked', { id });
  };

  render() {
    const { props, state } = this;
    return (
      <View style={styles.container}>
        <Header plusTitle title="Orders" back={() => props.navigation.goBack()} />
        {state.loaded ? (
          <Loading loadingComponent={this.loadingComponent} />
        ) : (
          <View style={styles.container}>
            <Details
              history={state.orderHistory}
              trackOrder={this.trackOrder}
              loaded={state.loaded}
              noMoreHistory={state.noMoreHistory}
              loadMore={this.loadMoreHistory}
            />
          </View>
        )}
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    user: state.currentUser,
    orders: state.orderHistory,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    clearTripInfoDispatcher: () => {
      dispatch({ type: DUMP_TRIP_INFO });
    },
    setOrderHistoryDispatcher: data => {
      dispatch(setOrderHistory(data));
    },
  };
}

OrderHistory.propTypes = {
  user: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.array, PropTypes.bool])
  ).isRequired,
  clearTripInfoDispatcher: PropTypes.func.isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderHistory);
