import React from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import IconI from 'react-native-vector-icons/Ionicons';
import { phonecall } from 'react-native-communications';
import { Rating } from 'react-native-elements';
import PropTypes from 'prop-types';
import styles from './style';
import Colors from '../../utils/colors';
import { heightRatio } from '../../utils/styleSheetGuide';

const TopCard = props => {
  const { tripAccepted } = props;
  return (
    <View style={styles.topCard}>
      <View style={styles.driverDetails}>
        <TouchableOpacity
          style={styles.cancelRideContainer}
          onPress={() => {
            phonecall(`+${tripAccepted.phoneNumber}`, true);
          }}
        >
          <ImageBackground
            source={{ uri: tripAccepted.profileImage }}
            style={styles.image}
            imageStyle={{ borderRadius: 25 * heightRatio }}
          >
            <View style={styles.callIconContainer}>
              <IconI name="ios-call" style={styles.callIcon}></IconI>
            </View>
          </ImageBackground>
        </TouchableOpacity>
        <View style={styles.driverDetailsInfo}>
          <Text style={styles.name} numberOfLines={1}>
            {tripAccepted.fullName}
          </Text>
          <Rating
            imageSize={16}
            type="custom"
            readonly
            // value={tripAccepted.rating}
            ratingColor={Colors.secondaryColor}
            startingValue={tripAccepted.rating}
            style={styles.rating}
          />
        </View>
      </View>
      <View style={styles.otpContainer}>
        <View style={styles.otp}>
          <Text>OTP - {tripAccepted.otp}</Text>
        </View>
      </View>
    </View>
  );
};

TopCard.propTypes = {
  tripAccepted: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default TopCard;
