import React from 'react';
import { View, Text, Modal, Image } from 'react-native';
import { connect } from 'react-redux';
// import { GoogleSignin } from 'react-native-google-signin';
import PropTypes from 'prop-types';
import { loginResetAction } from '../../navigation/resetNavigation';
import styles from './style';
import getImage from '../../utils/getImage';
import { heightRatio } from '../../utils/styleSheetGuide';
import { CLEAR_ORDER_HISTORY, USER_SIGNOUT } from '../../redux/actionTypes';
import Button from '../../component/Button';

const LogOut = props => {
  // const googlesignOut = async () => {
  //   try {
  //     await GoogleSignin.revokeAccess();
  //     await GoogleSignin.signOut();
  //   } catch (error) {
  //     throw error;
  //   }
  // };

  const signOutUser = () => {
    const { navigate } = props;
    props.logOut();
    navigate.dispatch(loginResetAction);
    // navigate.navigate('login');
    // this.googlesignOut();
  };
  const { logoutModal } = props;
  return (
    <Modal
      animated
      animationType="fade"
      transparent
      visible={logoutModal}
      onRequestClose={() => {
        props.setLogoutModal();
      }}>
      <View style={styles.backgroundContainer}>
        <View style={styles.mainContainer}>
          <Image
            resizeMode="contain"
            source={getImage.logout}
            style={styles.signOutImg}
          />
          <Text style={styles.sureText}>
            Are you Sure that you want to Sign out?
          </Text>
          <Button
            submit={() => signOutUser()}
            title="Yes"
            style={[
              styles.btns,
              styles.primaryColor,
              { marginTop: 20 * heightRatio },
            ]}></Button>
          <Button
            submit={() => props.setLogoutModal()}
            title="No"
            style={[styles.btns, { marginTop: 10 * heightRatio }]}></Button>
        </View>
      </View>
    </Modal>
  );
};
const mapDispatchToProps = dispatch => {
  return {
    logOut: () => {
      dispatch({
        type: USER_SIGNOUT,
      });
      dispatch({
        type: CLEAR_ORDER_HISTORY,
      });
    },
  };
};

LogOut.propTypes = {
  navigate: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.func, PropTypes.object])
  ).isRequired,
  logOut: PropTypes.func.isRequired,
  logoutModal: PropTypes.bool.isRequired,
  setLogoutModal: PropTypes.func.isRequired,
};

export default connect(
  null,
  mapDispatchToProps
)(LogOut);
