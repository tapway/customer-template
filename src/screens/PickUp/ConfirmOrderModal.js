/* eslint-disable no-use-before-define */
import Toast from 'react-native-easy-toast';
import React, { Component } from 'react';
import { getDistance } from 'geolib';
import {
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  StyleSheet,
  Image,
  Modal,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';
import { heightRatio, widthRatio } from '../../utils/styleSheetGuide';
import getImage from '../../utils/getImage';
import Colors from '../../utils/colors';

export default class ConfirmOrderModal extends Component {
  render() {
    const { props } = this;
    return (
      <Modal
        animated
        animationType="fade"
        visible={props.confirmOrderModalVisible}
        transparent
        onRequestClose={() => {
          props.toggleConfirmOrderModal();
        }}
      >
        <TouchableWithoutFeedback
          onPress={() => {
            props.toggleConfirmOrderModal();
          }}
        >
          <View style={styles.mainContainer}>
            <TouchableWithoutFeedback>
              <View style={styles.detailContainer}>
                <View style={styles.locationContainer}>
                  <Image
                    resizeMode="contain"
                    source={getImage.LocationPoint2}
                    style={styles.dropImg}
                  />

                  <View style={styles.locationDetails}>
                    <Text style={styles.locationText}>Pickup location</Text>
                    <TouchableOpacity
                      onPress={() => props.handleLocation(0)}
                      style={styles.chooseLocation}
                    >
                      <Text
                        numberOfLines={1}
                        style={
                          props.origin === 'Search here...' ? styles.locPlaceholder : styles.locText
                        }
                      >
                        {props.origin}
                      </Text>
                      <Image
                        resizeMode="contain"
                        source={getImage.editBlack}
                        style={styles.pencilImg}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.locationContainer}>
                  <Image
                    resizeMode="contain"
                    source={getImage.LocationPoint1}
                    style={styles.dropImg}
                  />
                  <View style={styles.locationDetails}>
                    <Text style={styles.locationText}>Drop location</Text>
                    <TouchableOpacity
                      onPress={() => props.handleLocation(1)}
                      style={styles.chooseLocation}
                    >
                      <Text
                        numberOfLines={1}
                        style={
                          props.destination === 'Search here...'
                            ? styles.locPlaceholder
                            : styles.locText
                        }
                      >
                        {props.destination}
                      </Text>
                      <Image
                        resizeMode="contain"
                        source={getImage.editBlack}
                        style={styles.pencilImg}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </TouchableWithoutFeedback>
            <Toast
              ref="toast"
              style={{ backgroundColor: Colors.toastBgColor }}
              position="bottom"
              fadeOutDuration={2000}
              opacity={0.8}
              textStyle={{ color: Colors.toastTextColor }}
            />
          </View>
        </TouchableWithoutFeedback>
        <View style={styles.footerContainer}>
          {!props.tripEstimate ? (
            <TouchableOpacity
              onPress={() => {
                if (props.destination !== 'Search here...') {
                  props.handleStateChange({
                    tripEstimate: !props.tripEstimate,
                  });
                } else {
                  this.refs.toast.show('Enter Drop Location');
                }
              }}
            >
              <Text style={styles.tripEstText}>Trip Estimate</Text>
            </TouchableOpacity>
          ) : (
            <View style={styles.footerContent}>
              <View style={styles.amountContainer}>
                <Text style={styles.amountText}>Total Amount:</Text>
                <Text style={styles.amountDetails}>
                  Rs.
                  {(getDistance(props.marker[0].coordinates, props.marker[1].coordinates) * 20) /
                    1000}
                </Text>
              </View>
              <View style={styles.confirmBtnContainer}>
                <TouchableOpacity onPress={() => props.registerOrder()}>
                  <Text style={styles.confirmBtnText}>Confirm</Text>
                </TouchableOpacity>
              </View>
            </View>
          )}
        </View>
        <Toast
          ref="toast"
          style={{ backgroundColor: Colors.toastBgColor }}
          position="bottom"
          positionValue={400}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: Colors.toastTextColor, fontSize: 15 }}
        />
      </Modal>
    );
  }
}

ConfirmOrderModal.propTypes = {
  confirmOrderModalVisible: PropTypes.bool.isRequired,
  toggleConfirmOrderModal: PropTypes.func.isRequired,
  handleLocation: PropTypes.func.isRequired,
  origin: PropTypes.string.isRequired,
  destination: PropTypes.string.isRequired,
  tripEstimate: PropTypes.bool.isRequired,
  handleStateChange: PropTypes.func.isRequired,
  marker: PropTypes.arrayOf(
    PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.object]))
  ).isRequired,
  registerOrder: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  fullFlex: { flex: 1 },
  mainContainer: { flex: 1, position: 'relative' },

  detailContainer: {
    paddingRight: 28 * widthRatio,
    paddingLeft: 14 * widthRatio,
    paddingVertical: 20 * heightRatio,
    height: 180 * heightRatio,
    width: 333 * widthRatio,
    backgroundColor: Colors.modalContainerColor,
    position: 'absolute',
    bottom: 10 * heightRatio,
    borderRadius: 6 * widthRatio,
    marginHorizontal: 20.5 * widthRatio,
    flexDirection: 'column',
    shadowColor: Colors.primaryShadowColor,
    shadowOffset: { width: 1, height: 5 },
    shadowOpacity: 0.1,
    shadowRadius: 5,
    elevation: 3,
  },
  locationContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  pickUpImg: {
    height: 18 * heightRatio,
  },

  dropImg: {
    height: 16 * heightRatio,
    marginTop: 3 * heightRatio,
    marginRight: 7 * widthRatio,
  },
  locationDetails: { flex: 1 },
  locationText: {
    fontSize: 14 * heightRatio,
    color: Colors.textForModalContainerColor,
  },
  chooseLocation: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: Colors.secondaryBorderColor,
    borderBottomWidth: 1,
    marginTop: 12 * heightRatio,
    paddingBottom: 6 * heightRatio,
  },
  pencilImg: {
    height: 12 * heightRatio,
    width: 12 * heightRatio,
    marginLeft: 10 * widthRatio,
  },
  locPlaceholder: {
    flex: 1,
    fontSize: 12 * heightRatio,
    color: Colors.placeHolderTextColor,
    textAlign: 'left',
  },

  locText: {
    flex: 1,
    fontSize: 12 * heightRatio,
    textAlign: 'left',
    color: Colors.textForModalContainerColor,
  },
  tripEstText: {
    fontSize: 16 * heightRatio,
    color: Colors.themeBackgroundColor,
    textAlign: 'center',
  },
  footerContainer: {
    height: Platform.OS === 'ios' ? 57 * heightRatio : 48 * heightRatio,
    backgroundColor: Colors.primaryColor,
    paddingBottom: Platform.OS === 'ios' ? 5 * heightRatio : null,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerContent: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    paddingHorizontal: 23 * widthRatio,
    paddingBottom: 10 * heightRatio,
  },
  amountText: {
    color: Colors.textForModalContainerColor,
  },
  amountDetails: {
    fontSize: 14 * heightRatio,
    color: Colors.textForModalContainerColor,
    textAlign: 'left',
    fontWeight: '500',
  },
  confirmBtnContainer: {
    flex: 1,
    height: '80%',
    borderColor: Colors.borderWithPrimaryBgColor,
    borderWidth: 1,
    borderRadius: 20 * widthRatio,
    alignItems: 'center',
    justifyContent: 'center',
  },
  confirmBtnText: {
    fontSize: 16 * heightRatio,
    color: Colors.textForPrimaryBtnBgColor,
    textAlign: 'right',
  },
  amountContainer: {
    width: '30%',
    marginRight: 120 * widthRatio,
    height: '100%',
    paddingTop: 5 * heightRatio,
    justifyContent: 'space-between',
  },
});
