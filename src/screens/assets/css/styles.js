import { StyleSheet } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  headerText: {
    fontSize: 40,
    fontWeight: '600',
  },
});

const commonStyles = StyleSheet.create({
  greenColor: {
    color: '#3FAF5D',
  },
  regularFont: {
    fontFamily: 'Nunito-Regular',
  },
});

const loginStyles = StyleSheet.create({
  container: {
    // flex: 1,
    // flexGrow: 1,
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#1891D0',
  },
  white: {
    color: '#FFFFFF',
  },
  welcomeHd: {
    fontWeight: '600',
    color: '#FFFFFF',
    fontSize: 30,
    fontFamily: 'Nunito-Bold',
    width: '70%',
  },
  textLabel: {
    marginTop: 17,
    fontSize: 18,
    fontWeight: '100',
    color: '#FFFFFF',
  },
  textInput: {
    backgroundColor: '#1891D0',
    borderColor: '#FFFFFF',
    color: '#FFFFFF',
    borderWidth: 1,
    fontSize: 22,
    marginTop: 3,
    paddingLeft: 10,
    height: 45,
    fontFamily: 'Nunito-Regular',
  },
  submitBtn: {
    backgroundColor: '#FFFFFF',
    borderRadius: 4,
    marginTop: 40,
    marginBottom: 30,
  },
  btnText: {
    color: '#000000',
    fontSize: 18,
    fontWeight: '100',
  },
  signUp: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerMenu: {
    // bottom: 20,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
});

const signUpStyles = StyleSheet.create({
  container: {
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#FFFFFF',
  },
  welcomeHd: {
    fontWeight: '600',
    color: '#1B2E5A',
    fontSize: 30,
    fontFamily: 'Nunito-Bold',
    width: '70%',
  },
  textLabel: {
    marginTop: 17,
    fontSize: 16,
    fontWeight: '100',
    color: '#858F99',
    fontFamily: 'Nunito-Regular',
  },
  textInput: {
    backgroundColor: '#EFF4F5',
    borderColor: '#FFFFFF',
    color: '#FFFFFF',
    borderWidth: 1,
    fontSize: 22,
    marginTop: 3,
    paddingLeft: 10,
    height: 45,
    fontFamily: 'Nunito-Regular',
  },
  submitBtn: {
    backgroundColor: '#3FAF5D',
    borderRadius: 4,
    marginTop: 40,
    marginBottom: 30,
  },
  btnText: {
    color: '#FFFFFF',
    fontSize: 18,
    fontWeight: '100',
    fontFamily: 'Nunito-Regular',
  },
});

export { styles, commonStyles, loginStyles, signUpStyles };
