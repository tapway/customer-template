import HelperFetch from './HelperFetch';
/**
 * Resend the otp to requested Phone Number
 * @return {success}
 */

const resendOtp = async mobileNumber => {
  const handleSuccess = () => {};
  const handleError = () => {};

  const extension = `/otpVerification/resendOtp`;

  await HelperFetch({
    extension,
    post: true,
    body: {
      phoneNumber: mobileNumber,
    },
    handleSuccess,
    handleError,
  });
};

export default resendOtp;
