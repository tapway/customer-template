import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { CheckBox } from 'native-base';
import PropTypes from 'prop-types';
import styles from './style';
import Colors from '../../utils/colors';

const PaymentMethod = props => {
  const { label, selected } = props;
  return (
    <TouchableOpacity onPress={() => props.selectMethod(label)}>
      <View style={styles.partnerChargesContainer}>
        <Text style={styles.partnerChargesText}>{label}</Text>
        <CheckBox
          style={styles.partnerChargesText}
          checked={selected}
          color={Colors.primaryColor}
          onPress={() => props.selectMethod(props.label)}
        />
      </View>
    </TouchableOpacity>
  );
};

PaymentMethod.propTypes = {
  label: PropTypes.string.isRequired,
  selected: PropTypes.bool.isRequired,
  selectMethod: PropTypes.func.isRequired,
};

export default PaymentMethod;
