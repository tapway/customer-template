/* eslint-disable no-underscore-dangle */
/* eslint-disable no-use-before-define */
import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { store } from '../redux/configureStore';
import Colors from '../utils/colors';
import { increaseQuantity, decreaseQuantity } from '../redux/action/cart';

const _ = require('lodash');

const CountButton = props => {
  const { item } = props;
  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => {
          props.decreaseDispatcher(item);
        }}
        style={styles.changeContainer}
      >
        <Text style={[styles.decrease, styles.primaryColor]}>-</Text>
      </TouchableOpacity>
      <View>
        <Text style={[styles.primaryColor, styles.count]}>
          {_.find(store.getState().cart, { _id: item._id }).quantity}
        </Text>
      </View>
      <TouchableOpacity
        onPress={() => {
          props.increaseDispatcher(item);
        }}
        style={styles.changeContainer}
      >
        <Text style={[styles.primaryColor, styles.increase]}>+</Text>
      </TouchableOpacity>
    </View>
  );
};

const mapStateToProps = state => {
  return { cart: state.cart };
};
const mapDispatchToProps = dispatch => {
  return {
    increaseDispatcher: item => {
      dispatch(increaseQuantity(item));
    },
    decreaseDispatcher: item => {
      dispatch(decreaseQuantity(item));
    },
  };
};
CountButton.propTypes = {
  item: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number])).isRequired,
  decreaseDispatcher: PropTypes.func.isRequired,
  increaseDispatcher: PropTypes.func.isRequired,
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CountButton);

const { height, width } = Dimensions.get('window');
const heightRatio = height / 667.0;
const widthRatio = width / 375.0;
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Colors.themeBackgroundColor,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: Colors.primaryColor,
    height: 30 * heightRatio,
    width: 80 * widthRatio,
  },
  primaryColor: { color: Colors.primaryColor },
  themeBackground: { color: Colors.themeBackgroundColor },
  decrease: { fontSize: 22 * heightRatio },
  count: { fontSize: 14 * heightRatio },
  increase: { fontSize: 18 * heightRatio },
  changeContainer: {
    flex: 1,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
