/* eslint-disable react/sort-comp */
/* eslint-disable no-use-before-define */
/* eslint-disable no-unused-vars */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable camelcase */
/* eslint-disable react-native/no-inline-styles */
import Geocoder from 'react-native-geocoding';
import { socketRiderInit } from '../../../socket';
import setDestination from '../../../redux/action/destination';
import Modal from "react-native-modal";
import { getInitialState, handleSetAddress, getRegisterOrderObject, getDist, getEstimate, getAmountEstimate, getTimeEstimate } from './helper';
import { getDistance } from 'geolib';
import { requestTrip, test } from '../../../socket/index';

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Alert,
  Image,
  KeyboardAvoidingView,
} from 'react-native';
import { connect } from 'react-redux';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import RNGooglePlaces from 'react-native-google-places';
import {
  Container,
  Header,
  Left,
  Icon,
  Body,
  Right,
  Content,
  Button,
  Card,
  Input,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import mapStyle from './mapStyle';
import { store } from '../../../redux/configureStore';
import distance from 'geo-dist';
import { RFValue } from 'react-native-responsive-fontsize';
import { FontNames } from '../../../../theme';
import getImage from '../../../utils/getImage';

export class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = getInitialState();

    socketRiderInit();
  }

  handleLocation = id => {
    const { state } = this;
    this.setState({ id }, () => {

    });
  };

  setLocation = (data, details) => {
    const { state } = this;
    const { id } = state;
    if (details.vicinity) {
      handleSetAddress(
        details.vicinity,
        details.geometry.location,
        state.marker,
        id,
        this.handleStateChange
      );
    } else {
      handleSetAddress(
        data.address,
        details.geometry.location,
        state.marker,
        id,
        this.handleStateChange
      );
    }

  };

  handleStateChange = payload => {
    this.setState(payload);
  };

  registerOrder = () => {
    const { props, state } = this;
    const { fromLocation_lat, fromLocation_lng, toLocation_lat, toLocation_lng, fromLocation, toLocation, } = state
    requestTrip({
      userId: props.user.id,
      paymentMode: 'Cash',
      paymentAmount:
        (getDistance(fromLocation_lat, fromLocation_lng, toLocation_lat, toLocation_lng) * 20) / 1000,
      endUserGpsLocation: { fromLocation_lat, fromLocation_lng },
      deliveryLocation: [toLocation_lng, toLocation_lat],
      deliveryInstructions: {
        fromLocation,
        toLocation,
        itemDetails,
      },
      pickUpLocation: [fromLocation_lng, fromLocation_lat],
      deliveryAddress: state.toLocation,
      pickUpAddress: state.fromLocation,
      category: 'Pickup',
    });
    // this.toggleConfirmOrderModal();
    // props.clearTripInfoDispatcher();
    props.navigation.navigate('successfullyBooked');
  };

  orderPickUp = async () => {
    const { fromLocation, toLocation, fromLocation_lat, fromLocation_lng, toLocation_lat, toLocation_lng } = this.state;
    const { props, state } = this;
    const dist = getDist(fromLocation_lat, fromLocation_lng, toLocation_lat, toLocation_lng)
    const est = getAmountEstimate(fromLocation_lat, fromLocation_lng, toLocation_lat, toLocation_lng)
    const time = getTimeEstimate(fromLocation_lat, fromLocation_lng, toLocation_lat, toLocation_lng)
    if (fromLocation.length < 0 || toLocation.length < 0) {
      Alert.alert('Select location from dropdown');
    } else {
      //test(getRegisterOrderObject(this.props, this.state))
      //getRegisterOrderObject(this.props,this.state)
      // requestTrip({
      //   userId: props.user.id,
      //   paymentMode: 'Cash',
      //   paymentAmount:
      //     (getDistance(fromLocation_lat, fromLocation_lng, toLocation_lat, toLocation_lng) * 20) / 1000,
      //   endUserGpsLocation: { fromLocation_lat, fromLocation_lng },
      //   deliveryLocation: [toLocation_lng, toLocation_lat],
      //   deliveryInstructions: {
      //     fromLocation,
      //     toLocation,
      //     itemDetails,
      //   },
      //   pickUpLocation: [fromLocation_lng, fromLocation_lat],
      //   deliveryAddress: state.toLocation,
      //   pickUpAddress: state.fromLocation,
      //   category: 'Pickup',
      // });
      // Alert.alert('Booked');
      //props.clearTripInfoDispatcher();

      Actions.PackageDetails({ fromLocation, toLocation, fromLocation_lat, fromLocation_lng, toLocation_lat, toLocation_lng, dist, time, est });
      // console.log(fromLocation);
      // Alert.alert(getDistance(fromLocation_lat, fromLocation_lng, toLocation_lat, toLocation_lng));

    }
  };

  checkInput = () => {
    const { fromLocationSet, toLocationSet } = this.state;
    let inputState = true;
    if (toLocationSet && fromLocationSet) {
      inputState = false;
    }
    return inputState;
  };

  togglePackageDetails = () => {
    this.setState({
      isPackageDetailsVisible: !this.state.isPackageDetailsVisible,
    });
  };

  getPredictions = place => {
    RNGooglePlaces.getAutocompletePredictions(place, {
      //  type: 'cities',
      country: 'NG',
    })
      .then(results =>
        this.setState(
          { predictions: results },
          () => { }
          // console.log('Predictions', results),
        )
      )
      .catch(error => console.log('google error', error.message));
  };

  clearPredictions = () => {
    this.setState({ predictions: [] });
  };

  setLocationPlace = (place, locationType) => {
    if (locationType === 'from') {
      RNGooglePlaces.lookUpPlaceByID(place.placeID)
        .then(results => {
          console.log('logging lookUpPlaceByID', results);
          const { latitude, longitude, viewport } = results.location;
          this.setState(
            {
              fromLocationSet: true,
              fromLocation: results.name,
              fromLocation_lat: latitude,
              fromLocation_lng: longitude,
              region: {
                latitude,
                longitude: this.state.toLocation_lng
                  ? this.state.toLocation_lng
                  : longitude,
                latitudeDelta: 0.3699098778279124,
                longitudeDelta: 0.1909481361508365,
              },
            },
            () => {
              // console.log('region', JSON.stringify(region));
              this.clearPredictions();
            }
          );
        })
        .catch(error => console.log(error.message));
    } else {
      RNGooglePlaces.lookUpPlaceByID(place.placeID)
        .then(results => {
          const { latitude, longitude } = results.location;
          this.setState(
            {
              toLocationSet: true,
              toLocation: results.name,
              toLocation_lat: latitude,
              toLocation_lng: longitude,
              region: {
                latitude: this.state.fromLocation_lat
                  ? this.state.fromLocation_lat
                  : latitude,
                longitude,
                latitudeDelta: 0.3699098778279124,
                longitudeDelta: 0.1909481361508365,
              },
            },
            () => {
              this.clearPredictions();
            }
          );
        })
        .catch(error => console.log(error.message));
    }
  };

  placesLists = (place, locationType) => {
    if (locationType === 'from') {
      this.setState(
        {
          locationType,
          fromLocation: place,
          fromLocationSet: false,
          currentInput: locationType,
        },
        () => {
          this.getPredictions(place, locationType);
        }
      );
    } else {
      this.setState(
        {
          locationType,
          toLocationSet: false,
          toLocation: place,
          currentInput: locationType,
        },
        () => {
          this.getPredictions(place, locationType);
        }
      );
    }
  };

  componentDidMount() {
    const { props } = this;
    // if (props) {

    // }
  }

  check = async () => {
    const userDetails = await store.getState();
    console.log('home userDetails', userDetails);
  };

  render() {
    const {
      toLocation,
      toLocationSet,
      toLocation_lat,
      toLocation_lng,

      fromLocation,
      fromLocationSet,
      fromLocation_lat,
      fromLocation_lng,

      predictions,
      currentInput,
      region,
    } = this.state;
    return (
      <Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
        <MapView
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          region={region}
          // onRegionChangeComplete={res => console.log('region change', res)}
          customMapStyle={mapStyle}>
          {fromLocationSet && (
            <Marker
              key={1}
              coordinate={{
                latitude: Number(fromLocation_lat),
                longitude: Number(fromLocation_lng),
              }}
              pinColor="#1B2E5A"
            />
          )}
          {toLocationSet && (
            <Marker
              key={2}
              coordinate={{
                latitude: Number(toLocation_lat),
                longitude: Number(toLocation_lng),
              }}
              pinColor="#ffb600"
            />
          )}
        </MapView>
        <Header transparent androidStatusBarColor="#0001">
          <Left>
            <Button transparent onPress={() => Actions.drawerOpen()}>
              <Icon
                style={{ paddingHorizontal: 10, color: '#000000' }}
                name="menu"
              />
            </Button>
          </Left>
          <Body />
          <Right>
            <TouchableOpacity
              onPress={() => this.check()}
              style={{
                backgroundColor: '#ffffff',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 6,
                },
                shadowOpacity: 0.37,
                shadowRadius: 7.49,

                elevation: 12,
              }}>
              <Icon
                style={{
                  paddingVertical: 8,
                  paddingHorizontal: 10,
                  color: '#3FAF5D',
                  fontSize: 18,
                }}
                type="FontAwesome"
                name="location-arrow"
              />
            </TouchableOpacity>
          </Right>
        </Header>
        <KeyboardAvoidingView style={{ flex: 1 }}>
          <View style={{ flex: 1, paddingHorizontal: 20 }}>
            <Card style={{ borderRadius: 10 }}>
              <View style={{ padding: 10 }}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flexDirection: 'column' }}>
                    <View
                      style={{
                        flex: 1,
                        justifyContent: 'center',
                      }}>
                      <Icon
                        type="MaterialCommunityIcons"
                        name="map-marker"
                        style={{
                          color: '#1B2E5A',
                          fontSize: 18,
                        }}
                      />
                    </View>
                    <Image source={require('../../../../assets/mapline.png')} resizeMode='contain' style={{ height: RFValue(35), width: RFValue(18), zIndex: 1000000, marginVertical: -5 }} />
                    <View
                      style={{
                        flex: 1,
                        justifyContent: 'center',
                      }}>
                      <Icon
                        type="MaterialCommunityIcons"
                        name="map-marker"
                        style={{
                          color: '#3FAF5D',
                          fontSize: 18,
                        }}
                      />
                    </View>
                  </View>

                  <View
                    style={{
                      flex: 1,
                      height: 80,
                      flexDirection: 'column',
                      marginLeft: 10,
                    }}>
                    <View
                      style={{
                        flex: 1,
                        height: 40,
                        borderBottomColor: '#E2E5E8',
                        borderBottomWidth: 1,
                      }}>
                      <Input
                        placeholder="Pickup location"
                        placeholderTextColor="#B1B1B1"
                        value={`${fromLocation}`}
                        keyboardType="default"
                        onChangeText={text => this.placesLists(text, 'from')}
                        clearButtonMode="always"
                        onBlur={this.clearPredictions}
                        maxLength={50}
                        style={{ fontFamily: FontNames.semibold }}
                      />
                    </View>
                    <View style={{ flex: 1, height: 40 }}>
                      <Input
                        placeholder="Dropoff location"
                        placeholderTextColor="#B1B1B1"
                        value={`${toLocation}`}
                        keyboardType="default"
                        onChangeText={text => this.placesLists(text, 'to')}
                        clearButtonMode="always"
                        onBlur={this.clearPredictions}
                        maxLength={50}
                        style={{ fontFamily: FontNames.semibold }}
                      />
                    </View>
                  </View>
                </View>
              </View>
            </Card>

            {predictions.length > 0 && (
              <Card style={{ borderRadius: 10, marginTop: 10 }}>
                <View
                  style={{
                    minheight: 50,
                    padding: 10,
                  }}>
                  {predictions.map(place => (
                    <TouchableOpacity
                      key={`${place.placeID}`}
                      onPress={() => this.setLocationPlace(place, currentInput)}
                      style={{
                        paddingBottom: 2,
                        // borderBottomColor: 'grey',
                        // borderBottomWidth: 0.5,
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}>
                        <Icon
                          type="MaterialCommunityIcons"
                          name="map-marker"
                          style={{
                            color: '#B1B1B1',
                            fontSize: 18,
                            padding: 5,
                          }}
                        />
                        <View
                          style={{
                            flex: 1,
                            borderBottomColor: '#E2E5E8',
                            borderBottomWidth: 1,
                            // padding: 10,
                          }}>
                          <Text style={{ fontSize: 16, padding: 10 }}>
                            {place.primaryText}
                          </Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                  ))}
                </View>
              </Card>
            )}
          </View>
        </KeyboardAvoidingView>
         <Modal
          isVisible={true}
          coverScreen={false}
          hasBackdrop={false}
          style={{ justifyContent: 'flex-end', margin: 0 }}>
          <View style={bottomModal.container}>
            <TouchableOpacity onPress={()=>this.setState({deliveryType:'bike'})} style={[bottomModal.chooseRow, {backgroundColor:this.state.deliveryType=='bike'?'#ebf7ee':'#fff'}]}>
              <Image source={getImage.deliverybike} resizeMode='contain' style={{width:50,height:50}}/>
              <Text style={bottomModal.chooseText}>Choose Bike</Text>
              {this.state.deliveryType=='bike'?<Icon
                style={bottomModal.checkbox}
                type="MaterialCommunityIcons"
                name="checkbox-marked-circle"
              />:null}
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>this.setState({deliveryType:'car'})} style={[bottomModal.chooseRow, {backgroundColor:this.state.deliveryType=='car'?'#ebf7ee':'#fff'}]}>
              <Image source={getImage.deliverycar} resizeMode='contain' style={{width:50,height:50}}/>
              <Text style={bottomModal.chooseText}>Choose Car</Text>
              {this.state.deliveryType=='car'?<Icon
                style={bottomModal.checkbox}
                type="MaterialCommunityIcons"
                name="checkbox-marked-circle"
              />:null}
            </TouchableOpacity>
            <Button
              disabled={this.checkInput()}
              block
              full
              style={{ elevation: 0, borderRadius: 4,width:'90%',marginTop:RFValue(15), alignSelf:'center', backgroundColor: this.checkInput() ? 'grey' : '#3FAF5D' }}
              onPress={() => this.orderPickUp()}>
              <Text
                style={{
                  color: '#ffffff',
                  fontFamily: 'Nunito-SemiBold',
                  fontSize: 16,
                }}>
                ORDER PICKUP
            </Text>
            </Button>           
          </View>
        </Modal>
       
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
 const bottomModal = StyleSheet.create({
  container: { minHeight: RFValue(250), backgroundColor: '#fff', justifyContent:'center', borderTopLeftRadius: 26, borderTopRightRadius: 26 },
  chooseRow:{flexDirection:'row',alignItems:'center',height:RFValue(80),paddingHorizontal:RFValue(30)},
  chooseText:{fontFamily:FontNames.bold,fontSize:RFValue(17),paddingLeft:RFValue(20)},
  checkbox:{paddingTop: 2,paddingLeft:12,color: '#3FAF5D',fontSize: 24,}
});
const mapStateToProps = state => ({});

function mapDispatchToProps(dispatch) {
  return {
    clearTripInfoDispatcher: () => {
      dispatch({ type: DUMP_TRIP_INFO });
    },
  };
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage);
