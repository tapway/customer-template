/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable lines-between-class-members */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableNativeFeedback, Image, TouchableOpacity, Alert, Linking } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Header,
  Left,
  Button,
  Icon,
  Body,
  Right,
  Content,
  Grid,
  Row,
  Col,
  Card,
  Item,
  Input,
  Picker,
  Thumbnail,
  CardItem,
  DatePicker
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import styles from './styles';
import { store } from '../../../redux/configureStore';
import { socketRiderInit } from '../../../socket';
import { requestTrip, futureTrip, test } from '../../../socket/index';
import { getInitialState, handleSetAddress, getRegisterOrderObject, getDist, getEstimate, getAmountEstimate, getTimeEstimate, getFutureOrderObject } from './helper';
import moment from 'moment'

import RNPaystack from 'react-native-paystack';
import getImage from '../../../utils/getImage';
import { RFValue } from 'react-native-responsive-fontsize';
import { FontNames } from '../../../../theme';

import { CreditCardInput, LiteCreditCardInput } from "react-native-credit-card-input";

RNPaystack.init({ publicKey: 'pk_test_1c926488ed110817b8a3d4e90aa93b79f8516a2c' });

const s = StyleSheet.create({
  container: {
    backgroundColor: "#030E1A",
    marginTop: 100,
    flex: 1,
    width: '100%',
    textAlign: 'center',
  },
  label: {
    color: "white",
    fontSize: 15,
  },
  input: {
    fontSize: 18,
    color: "green",
  },
});

const USE_LITE_CREDIT_CARD_INPUT = false;

class PackageDetails extends Component {
  constructor(props) {
    super(props);
    const [date, yo] = moment().toISOString().split('T')

    this.state = {
      itemQuantity: 0,
      selectedCard: '',
      checkPriceOrder: false,
      isReviewVisible: false,
      isPayCardVisible: false,
      isSuccessful: false,
      packageSize: { title: '', weight: 0 },
      addInfo: '',
      personInCharge: '',
      contactNumber: '',
      cardNumber: '',
      expiryDate: '',
      expiryYear: '',
      cvc: '',
      chosenDate: date,
      validity: false
    };

    this.setDate = this.setDate.bind(this);
    socketRiderInit();
  }

  toggleReviewVisible = () => {
    this.setState({
      isReviewVisible: !this.state.isReviewVisible,
      isPayCardVisible: false,
    });

  };
  togglePayCard = () => {
    this.setState({
      isPayCardVisible: !this.state.isPayCardVisible,
    });
  };
  handleCardNumber = number => {
    return number
      .replace(/\s?/g, "")
      .replace(/(\d{4})/g, "$1 ")
      .trim()
      ;
  };
  handleExpiryDate = text => {
    if (text.length === 2 && this.state.expiryDate.length === 1) {
      text += "/";
    }
    return text
  };
  setDate = async (newDate) => {
    const [date, yo] = moment(newDate).add(1, 'days').toISOString().split('T')
    this.setState({ chosenDate: date });
  }

  isEmpty = async (obj) => {
    return !obj || Object.keys(obj).length == 0;
  }

  toggleSuccessfulVisible = async () => {
    if (this.state.chosenDate === moment().format("YYYY-MM-DD")) {
      const { props, state } = this;
      let result = await this.makeOrder()

      if (result.reference) {
        test(getRegisterOrderObject(this.props, this.state))
        props.clearTripInfoDispatcher();
        props.navigation.navigate('successfullyBooked', { type: 'Cab' });
        this.setState({
          isSuccessful: !this.state.isSuccessful,
        });
      } else {
        Alert.alert(
          'Payment failed',
          (result),
          [{ text: 'OK', onPress: () => props.navigation.navigate('scenes') }]
        );
      }
    }
    else {
      if (result.reference) {
        futureTrip(getFutureOrderObject(this.props, this.state))
        props.clearTripInfoDispatcher();
        props.navigation.navigate('successfullyBooked', { type: 'Cab' });
        this.setState({
          isSuccessful: !this.state.isSuccessful,
        });
      } else {
        Alert.alert(
          'Payment failed',
          (result),
          [{ text: 'OK', onPress: () => props.navigation.navigate('scenes') }]
        );
      }
    }
  };

  _onChange = formData => {
    /* eslint no-console: 0 */
    const data = JSON.stringify(formData, null, " ")
    ///console.log('data', data);
    this.handleCardData(data)
  };

  _onFocus = field => {
    /* eslint no-console: 0 */
    //console.log('field', field);
  };

  makeOrder = () => {
    let month = '';
    let year = '';
    let result = ''
    if (this.state.expiryDate.includes('/')) {
      [month, year] = this.state.expiryDate.split('/');
    }
    else if (this.state.expiryDate.length) {
      month = this.state.expiryDate.substr(0, 2);
      year = this.state.expiryDate.substr(2, 6);
    }
    const est = getAmountEstimate(this.props.fromLocation_lat, this.props.fromLocation_lng, this.props.toLocation_lat, this.props.toLocation_lng)
    return new Promise((resolve, reject) => {
      RNPaystack.chargeCard({
        cardNumber: this.state.cardNumber.replace(/\s/g, ""), //4123450131001381
        expiryMonth: this.state.expiryDate.split("/")[0], // 12
        expiryYear: this.state.expiryDate.split("/")[1], // 19
        cvc: this.state.cvc, // 883
        email: store.getState().currentUser.email,
        amountInKobo: est * 100,
      })
        .then(response => {
          result = response
          resolve(result)
          // card charged successfully, get reference here
        })
        .catch(error => {
          result = error
          resolve(result.message)
          console.log(error); // error is a javascript Error object
          console.log(error.message);
          console.log(error.code);
        })

    })
  }

  _pressCall = () => {
    const url = 'tel://+123456789'
    Linking.openURL(url)
  }

  handleCardData = (data) => {
    let info = JSON.parse(data)
    this.setState({
      cardNumber: info.values.number,
      expiryDate: info.values.expiry,
      cvc: info.values.cvc,
      validity: info.values.valid
    });

  }

  handleSelectedCard = (selectedCard, weight) => {
    this.setState({
      selectedCard,
      packageSize: { title: selectedCard, weight },
    });
  };
  render() {
    const { fromLocation, toLocation, fromLocation_lat, fromLocation_lng, toLocation_lat, toLocation_lng, dist, time, est } = this.props;

    const {
      selectedCard,
      itemQuantity,
      isReviewVisible,
      isPayCardVisible,
      isSuccessful,
      addInfo,
      personInCharge,
      contactNumber,
      packageSize,
    } = this.state;

    var new_date = moment(new Date(), "DD-MM-YYYY").add(7, 'days');

    return (
      <Container>
        <Header
          transparent
          hasTabs
          style={{
            backgroundColor: '#FFFFFF',
            borderBottomColor: '#0001',
            borderBottomWidth: 0.7,
          }}
          androidStatusBarColor="#0001"
          iosBarStyle="dark-content">
          <Left>
            <Button transparent onPress={() => Actions.pop()}>
              <Icon
                style={{ paddingHorizontal: 5, color: '#000000' }}
                name="arrow-back"
              />
            </Button>
          </Left>
          <Body>
            <Text
              style={{
                fontSize: 18,
                fontFamily: FontNames.semibold,
                color: '#030E1A',
              }}>
              Package Details
            </Text>
          </Body>
          {/* <Right /> */}
        </Header>
        <Content contentContainerStyle={{ padding: 10, backgroundColor: '#fafafa' }}>
          <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Select size of package</Text>
            <Grid>
              <Row>
                <Col style={{ margin: 5 }}>
                  <Card
                    style={[
                      styles.packageCard,
                      selectedCard === 'small' ? styles.selectedCard : {},
                    ]}>
                    <TouchableNativeFeedback
                      onPress={() => this.handleSelectedCard('small', 10)}>
                      <View style={{ alignItems: 'center' }}>
                        <Image source={getImage.smallsize} resizeMode='contain' style={{ width: RFValue(32), height: RFValue(40), marginTop: RFValue(6) }} />
                        {selectedCard == 'small' ? <Image source={getImage.checkselect} resizeMode='contain' style={{ position: 'absolute', width: RFValue(30), height: RFValue(30), right: 5 }} /> : null}
                        <Text style={styles.packageSize}>Small</Text>
                        <Text style={styles.packageWeight}>Up to 10kg</Text>
                      </View>
                    </TouchableNativeFeedback>
                  </Card>
                </Col>
                <Col style={{ margin: 5 }}>
                  <Card
                    style={[
                      styles.packageCard,
                      selectedCard === 'medium' ? styles.selectedCard : {},
                    ]}>
                    <TouchableNativeFeedback
                      onPress={() => this.handleSelectedCard('medium', 20)}>
                      <View style={{ alignItems: 'center' }}>
                        <Image source={getImage.mediumsize} resizeMode='contain' style={{ width: RFValue(40), height: RFValue(40), marginTop: RFValue(6) }} />
                        {selectedCard == 'medium' ? <Image source={getImage.checkselect} resizeMode='contain' style={{ position: 'absolute', width: RFValue(30), height: RFValue(30), right: 5 }} /> : null}
                        <Text style={styles.packageSize}>Medium</Text>
                        <Text style={styles.packageWeight}>Up to 20kg</Text>
                      </View>
                    </TouchableNativeFeedback>
                  </Card>
                </Col>
                <Col style={{ margin: 5 }}>
                  <Card
                    style={[
                      styles.packageCard,
                      selectedCard === 'large' ? styles.selectedCard : {},
                    ]}>
                    <TouchableNativeFeedback
                      onPress={() => this.handleSelectedCard('large', 30)}>
                      <View style={{ alignItems: 'center' }}>
                        <Image source={getImage.largesize} resizeMode='contain' style={{ width: RFValue(40), height: RFValue(40), marginTop: RFValue(6) }} />
                        {selectedCard == 'large' ? <Image source={getImage.checkselect} resizeMode='contain' style={{ position: 'absolute', width: RFValue(30), height: RFValue(30), right: 5 }} /> : null}
                        <Text style={styles.packageSize}>Large</Text>
                        <Text style={styles.packageWeight}>Up to 30kg</Text>
                      </View>
                    </TouchableNativeFeedback>
                  </Card>
                </Col>
              </Row>
            </Grid>
          </View>
          <View style={[styles.inputContainer]}>
            <Text style={styles.inputLabel}>Additional Information</Text>
            <Item
              regular
              style={[
                styles.inputItem,
                {
                  alignContent: 'flex-start',
                  height: 80,
                  padding: 0,
                  margin: 0,
                },
              ]}>
              <Input
                placeholder=""
                multiline
                numberOfLines={10}
                maxLength={100}
                placeholderTextColor="#383838"
                style={[styles.inputText, { padding: 0 }]}
                onChangeText={text => this.setState({ addInfo: text })}
              />
            </Item>
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Quantity of item</Text>
            <Item regular style={styles.inputItem}>
              <Picker
                note
                mode="dropdown"
                // style={{ width: 120 }}
                selectedValue={itemQuantity}
                onValueChange={item => this.setState({ itemQuantity: item })}>
                <Picker.Item label="Select Number" value="0" />
                <Picker.Item label="1" value="1" />
                <Picker.Item label="2" value="2" />
                <Picker.Item label="3" value="3" />
                <Picker.Item label="4" value="4" />
                <Picker.Item label="5" value="5" />
              </Picker>
            </Item>
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Person/Shop in Charge</Text>
            <Item regular style={styles.inputItem}>
              <Input
                placeholder=""
                placeholderTextColor="#383838"
                style={styles.inputText}
                onChangeText={text => this.setState({ personInCharge: text })}
              />
            </Item>
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Delivery Date</Text>
            <Item regular style={styles.inputItem}>

              <DatePicker
                defaultDate={new Date()}
                minimumDate={new Date()}
                maximumDate={new Date(new Date(new_date).getFullYear(), new Date(new_date).getMonth(), new Date(new_date).getDate())}
                locale={"en"}
                timeZoneOffsetInMinutes={undefined}
                modalTransparent={false}
                animationType={"slide"}
                androidMode={"spinner"}
                onDateChange={this.setDate}
                disabled={false}
                required={true}
              />
              {/* <Text>
                {this.state.chosenDate.toString().substr(4, 12)}
              </Text> */}
            </Item>
          </View>

          <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Contact Phone Number</Text>
            <Item regular style={styles.inputItem}>
              <Input
                placeholder=""
                placeholderTextColor="#383838"
                style={styles.inputText}
                onChangeText={text => this.setState({ contactNumber: text })}
              />
            </Item>
          </View>
          <View style={styles.inputContainer}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 1, padding: 5 }}>
                <Button style={{ elevation: 0, backgroundColor: 'transparent' }} block full onPress={() => Actions.pop()}>
                  <Text
                    style={{
                      color: '#030E1A',
                      fontFamily: FontNames.semibold,
                      fontSize: 14,
                    }}>
                    Cancel
                  </Text>
                </Button>
              </View>
              <View style={{ flex: 1, padding: 5 }}>
                <Button
                  block
                  full
                  style={{ backgroundColor: '#3FAF5D', borderRadius: 5, elevation: 0 }}
                  onPress={() => this.toggleReviewVisible()}>
                  <Text
                    style={{
                      color: '#ffffff',
                      fontFamily: FontNames.semibold,
                      fontSize: 14,
                    }}>
                    CONTINUE
                  </Text>
                </Button>
              </View>
            </View>
          </View>
        </Content>
        {/* {isReviewVisible && } */}
        {isReviewVisible && (
          <View
            style={{
              position: 'absolute',
              top: 0,
              bottom: 0,
              right: 0,
              left: 0,
              backgroundColor: '#1891D0',
            }}>
            <Header
              style={{ backgroundColor: '#1891D0', marginTop: 35, elevation: 0, borderBottomColor: '#3f4f74', borderBottomWidth: 1 }}
              iosBarStyle="light-content"
              androidStatusBarColor="#1891D0">
              <Left>
                <Button transparent onPress={() => this.toggleReviewVisible()}>
                  <Icon name="arrow-back" style={{ color: '#ffffff' }} />
                </Button>
              </Left>
              <Body>
                <Text
                  numberOfLines={1}
                  style={{
                    fontSize: 18,
                    color: '#FFFFFF',
                    fontFamily: FontNames.semibold,
                  }}>
                  Review and Pay
                </Text>
              </Body>
            </Header>
            <Content padder>
              {!isPayCardVisible && (
                <Card style={{ paddingHorizontal: 10, borderRadius: 5 }}>
                  <View
                    style={[
                      styles.inputContainer,
                      {
                        backgroundColor: '#F4F4F4',
                        marginTop: 0,
                        marginHorizontal: -10,
                        borderRadius: 5,
                      },
                    ]}>
                    <Text style={[styles.inputLabel2, { padding: 10 }]}>
                      PICKUP RIDER & PRICE
                    </Text>
                    <View
                      style={{
                        flexDirection: 'row',
                        marginBottom: 5,
                        padding: 10,
                      }}>
                      <Left
                        style={{
                          flexDirection: 'row',
                        }}>
                        <View
                          style={{
                            backgroundColor: '#ffff',
                            borderRadius: 5,
                            padding: 2,
                            shadowColor: '#0000',
                            shadowOffset: {
                              width: 0,
                              height: 2,
                            },
                            shadowOpacity: 0.23,
                            shadowRadius: 2.0,
                            elevation: 4,
                          }}>
                          <Thumbnail
                            style={{ borderRadius: 5, height: 50, width: 50 }}
                            square
                            small
                            source={require('../../assets/icons/man_hat.png')}
                          />
                        </View>
                        <View
                          style={{
                            marginHorizontal: 20,
                            justifyContent: 'flex-end',
                            alignContent: 'flex-end',
                          }}>
                          <Text
                            style={{
                              color: '#030E1A',
                              marginBottom: RFValue(6),
                              fontSize: RFValue(16),
                              fontFamily: FontNames.semibold,
                            }}>
                            Gregory Samuel
                          </Text>
                          <TouchableOpacity
                            onPress={() => this._pressCall()}

                            style={{
                              backgroundColor: '#1B2E5A',
                              flexDirection: 'row',
                              borderRadius: 18,
                              paddingHorizontal: 10,
                              alignSelf: 'flex-start',
                              // margin: 2,
                              // paddingVertical: -10,
                            }}>
                            <Text
                              style={{
                                color: '#ffff',
                                fontSize: 16,
                                paddingVertical: RFValue(6),
                                alignSelf: 'center',
                                paddingHorizontal: 5,
                              }}>
                              Call
                            </Text>
                            <Icon
                              name="call"
                              style={{
                                color: '#ffffff',
                                fontSize: 16,
                                alignSelf: 'center',
                                paddingHorizontal: 5,
                              }}
                            />
                          </TouchableOpacity>
                        </View>
                      </Left>
                      <Right>
                        <Text
                          style={{
                            color: '#030E1A',
                            fontFamily: FontNames.semibold,
                            fontSize: 20,
                          }}>
                          ₦ {Math.round(est)}
                        </Text>
                      </Right>
                    </View>
                  </View>
                  <View style={[styles.inputContainer, styles.containerBorder]}>
                    <Text style={styles.inputLabel2}>PICKUP LOCATION</Text>
                    <Text style={styles.inputText2}>{fromLocation}</Text>
                  </View>
                  <View style={[styles.inputContainer, styles.containerBorder]}>
                    <Text style={styles.inputLabel2}>DROP LOCATION</Text>
                    <Text style={styles.inputText2}>{toLocation}</Text>
                  </View>
                  <View style={[styles.inputContainer, styles.containerBorder]}>
                    <Text style={styles.inputLabel2}>
                      ADDITIONAL INFORMATION
                    </Text>
                    <View style={{ flex: 1, marginVertical: 2 }}>
                      <Text
                        style={{
                          fontSize: 16,
                          color: '#030E1A',
                          fontFamily: FontNames.regular,
                        }}>
                        {addInfo}
                      </Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                      <View style={{ flex: 1 }}>
                        <Text style={{ fontFamily: FontNames.regular }}>Person in Charge:</Text>
                      </View>
                      <View style={{ flex: 1 }}>
                        <Text style={{ marginLeft: -50, fontFamily: FontNames.regular, fontSize: RFValue(15), color: '#030E1A' }}>
                          {personInCharge}
                        </Text>
                      </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                      <View style={{ flex: 1 }}>
                        <Text style={{ fontFamily: FontNames.regular }}>Phone Number</Text>
                      </View>
                      <View style={{ flex: 1 }}>
                        <Text style={{ marginLeft: -50, fontFamily: FontNames.regular, fontSize: RFValue(15), color: '#030E1A' }}>
                          {contactNumber}
                        </Text>
                      </View>
                    </View>
                  </View>
                  <View style={[styles.inputContainer, styles.containerBorder]}>
                    <Grid>
                      <Row>
                        <Col>
                          <Text style={styles.inputLabel2}>
                            SIZE OF PACKAGE
                          </Text>
                          <Card transparent>
                            <View style={{ flexDirection: 'row' }}>
                              <Image source={getImage.mediumsize} resizeMode='contain' style={{ width: RFValue(50), height: RFValue(50), marginTop: RFValue(6), marginRight: RFValue(13) }} />
                              <View>
                                <Text style={styles.packageSize}>
                                  {packageSize.title}
                                </Text>
                                <Text style={styles.packageWeight}>
                                  {`Up to ${packageSize.weight}kg`}
                                </Text>
                              </View>
                            </View>
                          </Card>
                        </Col>
                        <Col>
                          <Text style={styles.inputLabel2}>QUANTITY</Text>
                          <Card transparent>
                            <View>
                              <Text style={styles.inputText2}>
                                {itemQuantity}
                              </Text>
                            </View>
                          </Card>
                        </Col>
                      </Row>
                    </Grid>
                  </View>
                  <View style={[styles.inputContainer, styles.containerBorder]}>
                    <Grid>
                      <Row>
                        <Col>
                          <Text style={styles.inputLabel2}>TOTAL DISTANCE</Text>
                          <Card transparent>
                            <View>
                              <Text style={styles.inputText2}>{Math.round(dist)} km</Text>
                            </View>
                          </Card>
                        </Col>
                        <Col>
                          <Text style={styles.inputLabel2}>
                            EST. DELIVERY TIME
                          </Text>
                          <Card transparent>
                            <View>
                              <Text style={styles.inputText2}>{Math.round(time)} min</Text>
                            </View>
                          </Card>
                        </Col>
                      </Row>
                    </Grid>
                  </View>
                  <View style={styles.inputContainer}>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 1, padding: 5 }}>
                        <Button style={{ elevation: 0 }} block full onPress={this.toggleReviewVisible}>
                          <Text
                            style={{
                              color: '#030E1A',
                              fontFamily: FontNames.semibold,
                              fontSize: 14,
                            }}>
                            Cancel
                          </Text>
                        </Button>
                      </View>
                      <View style={{ flex: 1, padding: 5 }}>
                        <Button
                          block
                          full
                          style={{ backgroundColor: '#3FAF5D', elevation: 0, borderRadius: 5 }}
                          onPress={this.togglePayCard}>
                          <Text
                            style={{
                              color: '#ffffff',
                              fontFamily: FontNames.semibold,
                              fontSize: 14,
                            }}>
                            CONTINUE
                          </Text>
                        </Button>
                      </View>
                    </View>
                  </View>
                </Card>
              )}
              {isPayCardVisible && (
                <View style={s.container}>
                  {USE_LITE_CREDIT_CARD_INPUT ?
                    (<LiteCreditCardInput
                      autoFocus
                      inputStyle={s.input}

                      validColor={"black"}
                      invalidColor={"red"}
                      placeholderColor={"darkgray"}

                      onFocus={this._onFocus}
                      onChange={this._onChange} />) :
                    (<CreditCardInput
                      autoFocus

                      requiresName
                      requiresCVC
                      //requiresPostalCode

                      labelStyle={s.label}
                      inputStyle={s.input}
                      validColor={"green"}
                      invalidColor={"red"}
                      placeholderColor={"darkgray"}

                      onFocus={this._onFocus}
                      onChange={this._onChange} />

                    )
                  }

                </View>

              )}

              <View><Button
                disabled={this.state.validity}
                block
                primary
                style={styles.submitBtn}
                onPress={this.toggleSuccessfulVisible}>
                <Text style={styles.btnText}>PAY</Text>
              </Button></View>
            </Content>
          </View>
        )}
        {isSuccessful && (
          <View
            style={{
              position: 'absolute',
              top: 0,
              bottom: 0,
              right: 0,
              left: 0,
              backgroundColor: '#1B2E5A',
              zIndex: 10,
            }}>
            <Header
              style={{ backgroundColor: '#1B2E5A', marginTop: 35 }}
              iosBarStyle="light-content"
              androidStatusBarColor="#1B2E5A">
              {/* <Left /> */}
              <View style={{ justifyContent: 'center' }}>
                <Text
                  numberOfLines={1}
                  style={{
                    fontSize: 20,
                    color: '#FFFFFF',
                    fontFamily: 'Nunito-SemiBold',
                  }}>
                  Success
                </Text>
              </View>
              {/* <Right /> */}
            </Header>
            <Content padder>
              <Card style={{ borderRadius: 5 }}>
                <View style={styles.modalContainer}>
                  <View style={{ alignSelf: 'center' }}>
                    <Icon name="checkmark-circle" style={styles.modalIcon} />
                  </View>
                  <Card transparent>
                    <CardItem style={{ justifyContent: 'center' }}>
                      <Text style={styles.modalTitle}>
                        Order placed successfully!
                      </Text>
                    </CardItem>

                    <CardItem style={{ justifyContent: 'center' }}>
                      <Text style={styles.modalSubtitle}>
                        Your order with has been successfully request, you can
                        track the progress of the order in the with the parcel
                        ID generated.
                      </Text>
                    </CardItem>

                    <View style={{ padding: 16 }}>
                      <Button
                        block
                        full
                        onPress={() => Actions.HomePage()}
                        style={styles.modalButton}>
                        <Text style={styles.modalButtonText}>DONE</Text>
                      </Button>
                    </View>
                  </Card>
                </View>
              </Card>
            </Content>
          </View>
        )}
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

function mapDispatchToProps(dispatch) {
  return {
    clearTripInfoDispatcher: () => {
      dispatch({ type: DUMP_TRIP_INFO });
    },
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PackageDetails);
