const FontNames = {
    bold: 'Nunito-Bold',
    extrabold: 'Nunito-ExtraBold',
    semibold: 'Nunito-SemiBold',
    regular: 'Nunito-Regular',
    light: 'Nunito-Light',
    black: 'Nunito-Black'
  };
  
  export default FontNames;
  