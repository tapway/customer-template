/* eslint-disable react/no-string-refs */
/* eslint-disable no-use-before-define */
import React from 'react';
import Geocoder from 'react-native-geocoding';
import { View, Text, TouchableOpacity, StyleSheet, Image, Modal, Platform } from 'react-native';
import { getDistance } from 'geolib';
import PropTypes from 'prop-types';
import { heightRatio, widthRatio } from '../../utils/styleSheetGuide';
import PaymentMethod from '../../component/PaymentMethod';
import { store } from '../../redux/configureStore';
import getImage from '../../utils/getImage';
import Colors from '../../utils/colors';

Geocoder.init(store.getState().config.googleMapsApiKey);

const PaymentModal = props => {
  const { estimate, marker, payMethod, payMethodChange, paymentModalVisible } = props;
  return (
    <Modal
      animationType="slide"
      visible={paymentModalVisible}
      onRequestClose={() => {
        props.togglePaymentModal();
      }}
      transparent
      style={styles.fullFlex}
    >
      <View style={styles.container}>
        <View style={styles.topContainer}>
          <View style={styles.imgContainer}>
            <Image resizeMode="contain" source={getImage.taxi} style={styles.profileImg} />
          </View>
          <View style={styles.fareContainer}>
            <Text style={styles.fare}>${estimate}</Text>
            <Text style={styles.fareText}>Total Fare</Text>
          </View>
        </View>
        <View style={styles.bottomContainer}>
          <View style={styles.travelDetails}>
            <View style={styles.travelDetailContainer}>
              <Text>Estimated Distance : </Text>
              <Text style={styles.travelDetailText}>
                {`${getDistance(marker[0].coordinates, marker[1].coordinates) / 1000} km`}
              </Text>
            </View>
            <View style={styles.travelDetailContainer}>
              <Text>Estimate Time : </Text>
              <Text style={styles.travelDetailText}>42 mins</Text>
            </View>
          </View>
          <View style={styles.methodContainer}>
            <PaymentMethod payMethod={payMethod} payMethodChange={payMethodChange} />
          </View>
        </View>
      </View>
      <View style={styles.footer}>
        <TouchableOpacity
          style={styles.cancelButton}
          onPress={() => {
            props.togglePaymentModal();
          }}
        >
          <Text style={styles.buttonText}>Cancel</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.requestButton} onPress={() => props.registerOrder()}>
          <Text style={styles.primaryColorText}>Request</Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

PaymentModal.propTypes = {
  estimate: PropTypes.number.isRequired,
  marker: PropTypes.arrayOf(PropTypes.object).isRequired,
  payMethod: PropTypes.string.isRequired,
  payMethodChange: PropTypes.func.isRequired,
  paymentModalVisible: PropTypes.bool.isRequired,
  registerOrder: PropTypes.func.isRequired,
  togglePaymentModal: PropTypes.func.isRequired,
};

export default PaymentModal;
const styles = StyleSheet.create({
  fullFlex: {
    flex: 1,
  },
  container: {
    height: 140 * heightRatio,
    marginHorizontal: 17.5 * widthRatio,
    marginTop: 450 * heightRatio,
    backgroundColor: Colors.modalContainerColor,
    borderRadius: 6 * widthRatio,
    paddingHorizontal: 19.5 * widthRatio,
    paddingVertical: 16 * heightRatio,
  },
  topContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    paddingBottom: 10 * heightRatio,
    borderBottomColor: Colors.secondaryBorderColor,
    borderBottomWidth: 1,
  },
  bottomContainer: {
    flexDirection: 'row',
    marginTop: 5 * heightRatio,
    alignItems: 'center',
  },
  profileImg: {
    height: 30 * heightRatio,
    width: 30 * heightRatio,
  },
  imgContainer: {
    height: 50 * heightRatio,
    width: 50 * heightRatio,
    borderWidth: 1,
    borderColor: Colors.secondaryBorderColor,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25 * heightRatio,
  },
  driverDetailsContainer: {
    flex: 1,
    marginLeft: 11 * widthRatio,
    height: 55 * heightRatio,
    overflow: 'scroll',
  },
  driverName: { fontSize: 16 * heightRatio, fontWeight: '100' },
  driverNumber: {
    fontSize: 13 * heightRatio,
    paddingVertical: 2 * heightRatio,
  },
  fareContainer: {},
  fare: {
    color: Colors.textForModalContainerColor,
    fontSize: 16 * heightRatio,
    fontWeight: '500',
    textAlign: 'left',
  },
  fareText: {
    color: Colors.textForModalContainerColor,
    marginTop: 2 * heightRatio,
    fontSize: 12 * heightRatio,
    fontWeight: '100',
    textAlign: 'left',
  },
  travelDetails: {
    flexDirection: 'column',
    height: 48 * heightRatio,
    justifyContent: 'space-evenly',
  },
  methodContainer: {
    paddingTop: 4,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  travelDetailContainer: { flexDirection: 'row' },
  travelDetailText: { fontWeight: '600' },
  breakLine: {
    height: 40 * heightRatio,
    width: 1 * widthRatio,
    marginLeft: 30 * widthRatio,
    backgroundColor: Colors.primaryBorderColor,
  },
  footer: {
    height: Platform.OS === 'ios' ? 57 * heightRatio : 52 * heightRatio,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: Colors.themeBackgroundColor,
    justifyContent: 'space-evenly',
    marginTop: 10 * heightRatio,
    paddingTop: Platform.OS === 'ios' ? 5 * heightRatio : 0,
  },
  cancelButton: {
    width: 140 * widthRatio,
    height: 52,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 46,
    borderColor: Colors.secondaryColor,
    borderWidth: 1,
  },
  requestButton: {
    backgroundColor: Colors.primaryBtnBgColor,
    width: 140 * widthRatio,
    height: 52,
    borderRadius: 46,

    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: Colors.textForThemeBgBtn,
    fontSize: 16 * heightRatio,
  },
  primaryColorText: {
    color: Colors.themeBackgroundColor,
    fontSize: 16 * heightRatio,
  },
});
