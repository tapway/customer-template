import { get } from 'lodash';

const returnProperty = (property, payload, state) => {
  return get(payload, property, get(state, property));
};

export default returnProperty;
