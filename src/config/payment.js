import RazorpayCheckout from 'react-native-razorpay';
import { Alert } from 'react-native';
import { store } from '../redux/configureStore';
import Colors from '../utils/colors';

const payment = (registerOrder, type, clearCartDispatcher, handlePaymentFailure, price) => {
  const { fullName, email, phoneNumber, profileImage } = store.getState().currentUser;
  const { RAZOR_PAY_KEY } = store.getState().config;
  const options = {
    description: 'Payment for Geekyants',
    image: profileImage,
    currency: 'INR',
    key: RAZOR_PAY_KEY,
    amount: price * 100,
    external: {
      wallets: ['paytm'],
    },
    name: fullName,
    prefill: {
      email,
      contact: phoneNumber,
      name: fullName,
    },
    theme: { color: Colors.primaryColor },
  };

  RazorpayCheckout.open(options)
    .then(data => {
      // handle success
      registerOrder('RazorPay', data.razorpay_payment_id);
      if (type === 'confirmclass') {
        clearCartDispatcher();
      }
      return true;
    })
    .catch(() => {
      // handle failure
      handlePaymentFailure();
      return false;
    });

  RazorpayCheckout.onExternalWalletSelection(data => {
    Alert.alert('External Wallet Selected', `${data.external_wallet}`);
  });
};

export default payment;
