/* eslint-disable no-use-before-define */
import React, { Component } from 'react';
import { View, Text, Animated, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import getImage from '../utils/getImage';
import { heightRatio, widthRatio } from '../utils/styleSheetGuide';
import Colors from '../utils/colors';

export default class EmptyList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.animatedValue = new Animated.Value(0);
    this.animatedValue2 = new Animated.Value(0);
  }

  componentDidMount() {
    this.animate();
  }

  animate() {
    this.animatedValue.setValue(0);
    this.animatedValue2.setValue(0);
    Animated.parallel([
      Animated.timing(this.animatedValue, {
        toValue: 1,
        duration: 500,
      }),
      Animated.timing(this.animatedValue2, {
        toValue: 1,
        duration: 500,
        delay: 500,
      }),
    ]).start();
  }

  render() {
    const { props } = this;
    const opacity = this.animatedValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 0.5, 1],
    });
    const scaleText = this.animatedValue2.interpolate({
      inputRange: [0, 1],
      outputRange: [0.5, 1],
    });
    return (
      <View style={styles.container}>
        <Animated.Image
          source={getImage.cryingIcon}
          style={[styles.image, { transform: [{ scale: scaleText }] }]}
          resizeMode="contain"
        />
        <Animated.View style={[styles.descriptionContainer, { opacity }]}>
          <Text style={styles.descriptionText1}>
            {props.store
              ? 'The store is currently unavailable.'
              : props.items
              ? 'The item is currently unavailable.'
              : 'The order history is empty.'}
          </Text>
        </Animated.View>
      </View>
    );
  }
}
EmptyList.propTypes = {
  store: PropTypes.bool,
  items: PropTypes.bool,
};
EmptyList.defaultProps = {
  store: false,
  items: false,
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: heightRatio * 80,
  },
  image: {
    height: heightRatio * 70,
    width: widthRatio * 200,
  },
  descriptionContainer: {
    flex: 1,
    marginTop: heightRatio * 50,
    width: widthRatio * 330,
  },
  descriptionText1: {
    textAlign: 'center',
    fontSize: heightRatio * 14,
    color: Colors.textForThemeBgColor,
    fontWeight: 'bold',
  },
  descriptionText2: {
    textAlign: 'center',
    fontSize: heightRatio * 12,
    color: Colors.textForThemeBgColor,
    fontWeight: '300',
    marginTop: heightRatio * 10,
  },
});
