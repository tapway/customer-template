import React from 'react';
import { View, Text } from 'react-native';
import IconI from 'react-native-vector-icons/Ionicons';
import PropTypes from 'prop-types';
import styles from './style';
import Colors from '../../utils/colors';

const BottomCard = props => {
  const { orderStatus, orderType, tripAccepted } = props;
  const RideMessages = [
    'Your ride has been requested. Searching for nearby drivers!',
    `${tripAccepted.fullName ||
      'John'} has accepted the request and is on the way to pick you up. Stay tight!`,
    'Seems like you have found your cab! Your ride has been started. Stay safe!',
    'The ride has been ended. Hope the services meet your expectations!',
  ];
  const OrderMessages = [
    'Your order has been placed. Searching for nearby delivery executives',
    `${tripAccepted.fullName ||
      'John'} has accepted the request and is on the way to pick your order. Excitement begins here!`,
    'Your order has been picked. Stay excited!',
    'The order has been delivered. Hope our services meet your expectations!',
  ];

  const OrderStatusHeadings = [
    'Order Placed',
    'Order Accepted',
    'Order Picked Up',
    'Order Delivered',
  ];

  const statusHeading = [
    { heading: 'Order Recieved', flag: -1 },
    { heading: 'Order Picked', flag: 1 },
    { heading: 'Order Delivered', flag: 2 },
  ];

  const TickIcon = tickIconProps => {
    const { tickIconFlag } = tickIconProps;
    return (
      <IconI
        name="ios-checkmark-circle-outline"
        style={[
          styles.tickIconStyle,
          {
            color: orderStatus >= tickIconFlag ? Colors.primaryColor : 'silver',
          },
        ]}
      ></IconI>
    );
  };

  const TickIconJoiner = tickIconJoinerProps => {
    const { joinerFlag } = tickIconJoinerProps;
    return (
      <View
        style={[
          styles.lineStyle,
          {
            backgroundColor: orderStatus >= joinerFlag ? Colors.primaryColor : 'silver',
          },
        ]}
      ></View>
    );
  };

  const StatusHeading = statusHeadingProps => {
    const { statusFlag, heading } = statusHeadingProps;
    return (
      <Text
        style={[
          styles.orderStatusTextStyle,
          {
            color: orderStatus >= statusFlag ? Colors.primaryColor : 'silver',
          },
        ]}
      >
        {heading}
      </Text>
    );
  };

  if (orderType === 'Cab') {
    return (
      <View style={styles.bottomCard}>
        <View style={styles.orderStatusOnlyBottomContainer}>
          <IconI
            name="ios-checkmark-circle-outline"
            style={[
              styles.tickIconStyle,
              {
                color: Colors.primaryColor,
              },
            ]}
          ></IconI>
          <View>
            <Text style={styles.orderStatusHeading}>Ride Scheduled</Text>
            <Text style={styles.orderStatusSubHeading}>{RideMessages[orderStatus + 1]}</Text>
          </View>
        </View>
      </View>
    );
  }

  return (
    <View style={styles.bottomCard}>
      <View style={styles.orderStatusContainer}>
        <View style={styles.orderStatusIconsContainer}>
          <TickIcon tickIconFlag={-1} />
          <TickIconJoiner joinerFlag={1} />
          <TickIcon tickIconFlag={1} />
          <TickIconJoiner joinerFlag={2} />
          <TickIcon tickIconFlag={2} />
        </View>
        <View style={styles.orderStatusTextContainer}>
          {statusHeading.map(value => {
            return (
              <StatusHeading
                heading={value.heading}
                statusFlag={value.flag}
                key={value.heading + value.flag}
              />
            );
          })}
        </View>
      </View>
      <View style={styles.orderStatusBottomContainer}>
        <Text style={styles.orderStatusHeading}>{OrderStatusHeadings[orderStatus + 1]}</Text>
        <Text style={styles.orderStatusSubHeading}>{OrderMessages[orderStatus + 1]}</Text>
      </View>
    </View>
  );
};

BottomCard.propTypes = {
  orderStatus: PropTypes.number.isRequired,
  tripAccepted: PropTypes.objectOf(PropTypes.any).isRequired,
  orderType: PropTypes.string.isRequired,
};

export default BottomCard;
