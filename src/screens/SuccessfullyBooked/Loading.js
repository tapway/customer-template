import React from 'react';
import { View } from 'react-native';
import { PlaceholderContainer, Placeholder } from 'react-native-loading-placeholder';
import LinearGradient from 'react-native-linear-gradient';
import PropTypes from 'prop-types';
import styles from './style';

const Loading = props => {
  const { loadingComponent } = props;
  return <PlaceholderExample loader={loadingComponent} />;
};

const Gradient = (): React.Element<*> => {
  return (
    <LinearGradient
      colors={['#eeeeee', '#dddddd', '#eeeeee']}
      start={{ x: 1.0, y: 0.0 }}
      end={{ x: 0.0, y: 0.0 }}
      style={{
        flex: 1,
        width: 120,
      }}
    />
  );
};

const PlaceholderExample = ({ loader }: { loader: Promise<*> }): React.Element<*> => {
  return (
    <PlaceholderContainer
      style={styles.placeholderContainer}
      animatedComponent={<Gradient />}
      duration={1000}
      delay={1000}
      loader={loader}
    >
      <View style={styles.driverDetails}>
        <Placeholder style={[styles.placeholder, styles.placeholderImage]} />
        <View style={styles.driverDetailsInfo}>
          <Placeholder style={[styles.placeholderName, styles.placeholder]}></Placeholder>
          <Placeholder style={[styles.placeholderName, styles.placeholder]}></Placeholder>
        </View>
      </View>
      <View style={styles.otpContainer}>
        <View style={styles.otp}>
          <Placeholder style={[styles.placeholder, styles.otpPlaceholder]} />
        </View>
      </View>
    </PlaceholderContainer>
  );
};

Loading.propTypes = {
  loadingComponent: PropTypes.objectOf(PropTypes.any),
};
Loading.defaultProps = {
  loadingComponent: undefined,
};

PlaceholderExample.propTypes = {
  loader: PropTypes.objectOf(PropTypes.any),
};
PlaceholderExample.defaultProps = {
  loader: undefined,
};

export default Loading;
