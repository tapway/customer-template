import { SET_USER } from '../actionTypes';

/**
 * Set User To Redux
 */
const setUser = user => {
  return {
    type: SET_USER,
    payload: {
      id: user._id,
      fullName: user.fullName,
      firstName: user.firstName,
      lastName: user.lastName,
      address: user.address,
      gpsLocation: user.gpsLocation,
      savedAddresses: user.savedAddresses,
      phoneNumber: user.phoneNumber,
      email: user.email,
      profileImage: user.profileImage,
      isLogin: true,
      rating: user.rating,
    },
  };
};
export default setUser;
