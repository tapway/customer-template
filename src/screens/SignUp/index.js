/* eslint-disable react/no-unused-state */
import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Keyboard,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';
import PhoneInput from 'react-native-phone-input';
import Toast from 'react-native-easy-toast';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Header from '../../component/HeaderBar';
import styles from './style';
import Loader from '../../component/Loader';
import { checkUserExistanceDb } from '../../apiServices/signUp';
import setUser from '../../redux/action/signUp';
import Colors from '../../utils/colors';

class SignUp extends Component {
  constructor() {
    super();
    this.state = {
      phoneNumber: '+91-',
      email: '',
      imageLoader: false,
      confirmResult: '',
      name: '',
      exist: false,
      userInfo: '',
    };
    this.phone = null;
  }

  loader = () => {
    this.setState({ imageLoader: false });
  };

  handleError = () => {
    const { refs } = this;
    refs.toast.show('Number already Exists');
  };

  accept() {
    const { refs, props } = this;
    const { phoneNumber } = this.state;
    if (this.phone.isValidNumber()) {
      const userInfo = props.navigation.getParam('userInfo');
      const obj = {
        fullName: userInfo.name,
        firstName: userInfo.givenName,
        lastName: userInfo.familyName,
        phoneNumber,
        email: userInfo.email,
        profileImage: userInfo.photo,
      };
      checkUserExistanceDb(phoneNumber, obj, this.handleError, props, this.loader);
    } else {
      refs.toast.show('Invalid Number');
    }
  }

  render() {
    const { phoneNumber, imageLoader } = this.state;
    const { props } = this;
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <KeyboardAvoidingView
          style={styles.modalInputContainer}
          behavior="padding"
          enabled={Platform.OS === 'ios'}
        >
          <View style={styles.container}>
            <Loader loading={imageLoader} />
            <Header onlyBack back={() => props.navigation.goBack()} />
            <View style={styles.mainContainer}>
              <View style={styles.mobileNumberInput}>
                <Text style={styles.mobileNumberText}>Please enter your</Text>
                <Text style={styles.mobileNumberText}>mobile number to proceed</Text>
                <PhoneInput
                  ref={ref => {
                    this.phone = ref;
                  }}
                  style={styles.input}
                  value={phoneNumber}
                  onChangePhoneNumber={text => {
                    this.setState({
                      phoneNumber: text,
                    });
                  }}
                  textStyle={{ fontSize: 20 }}
                  textProps={{ placeholder: 'Enter Phone Number' }}
                />
              </View>
              <View style={styles.nextBtnContainer}>
                <TouchableOpacity
                  style={styles.button1}
                  onPress={() => {
                    this.accept();
                  }}
                >
                  <Text style={styles.nextBtnText}>Next</Text>
                </TouchableOpacity>
              </View>
            </View>
            <Toast
              ref="toast"
              style={{ backgroundColor: Colors.toastBgColor }}
              position="bottom"
              positionValue={400}
              fadeInDuration={750}
              fadeOutDuration={1000}
              opacity={0.8}
              textStyle={{ color: Colors.toastTextColor, fontSize: 15 }}
            />
          </View>
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setUserDispatcher: user => {
      dispatch(setUser(user));
    },
  };
};

SignUp.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  null,
  mapDispatchToProps
)(SignUp);
