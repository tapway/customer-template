import HelperFetch from './HelperFetch';

/**
 * Add new address
 * @returns {Response:true}
 */

const addNewAddressDb = async (userDetails, jwtAccessToken, handleResult) => {
  const handleSuccess = res => {
    if (res.success) {
      handleResult(userDetails);
    }
  };
  const handleError = () => {};

  const extension = `/endUser/newAddress`;

  await HelperFetch(
    {
      extension,
      put: true,
      body: {
        newAddress: userDetails,
      },
      Authorization: jwtAccessToken,
    },
    handleSuccess,
    handleError
  );
};
export default addNewAddressDb;
