import React from 'react';
import { View, StyleSheet, Image } from 'react-native';

import IconMCI from 'react-native-vector-icons/MaterialCommunityIcons';
import IconI from 'react-native-vector-icons/Ionicons';
import { heightRatio } from '../utils/styleSheetGuide';
import Colors from '../utils/colors';
import getImage from '../utils/getImage';

const styles = StyleSheet.create({
  locationPinContainer: { alignItems: 'center', justifyContent: 'center' },
  locationIconContainer: {
    height: 30 * heightRatio,
    width: 30 * heightRatio,
    borderRadius: 15 * heightRatio,
    borderWidth: 3,
    borderColor: Colors.borderWithPrimaryBgColor,
    alignItems: 'center',
    justifyContent: 'center',
  },
  locationPinStroke: {
    height: 15 * heightRatio,
    width: 2,
    backgroundColor: Colors.mapPinStoke,
  },
  locationIcon: { fontSize: 15 * heightRatio, color: Colors.white },
});

const destinationPin = () => {
  return (
    <View style={styles.locationPinContainer}>
      <View style={[styles.locationIconContainer, { backgroundColor: Colors.secondaryColor }]}>
        <IconI name="ios-pin" style={styles.locationIcon}></IconI>
      </View>
      <View style={styles.locationPinStroke}></View>
    </View>
  );
};

const pickUpPin = () => {
  return (
    <View style={styles.locationPinContainer}>
      <View style={[styles.locationIconContainer, { backgroundColor: Colors.primaryColor }]}>
        <IconMCI name="human-greeting" style={styles.locationIcon}></IconMCI>
      </View>
      <View style={styles.locationPinStroke}></View>
    </View>
  );
};

const driverLocationPin = () => {
  return (
    <Image
      resizeMode="contain"
      source={getImage.bikePin}
      style={{ height: 35 * heightRatio }}
    ></Image>
  );
};

export { pickUpPin, destinationPin, driverLocationPin };
