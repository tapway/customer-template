// import { get } from 'lodash';
import {
  FETCH_CURRENT_USER,
  SET_USER,
  SET_CURRENT_LOCATION,
  USER_SIGNOUT,
  ADD_SECOND_ADDRESS,
  CHECK_LOCATION,
} from '../actionTypes';
import returnProperty from '../helper';

const initialState = {
  jwt: '',
  id: '',
  fullName: '',
  firstName: '',
  lastName: '',
  address: '',
  gpsLocation: '',
  savedAddresses: '',
  phoneNumber: '',
  email: '',
  profileImage: '',
  rating: '',
  isLogin: false,
};

export default function(state = initialState, action) {
  // debugger;
  switch (action.type) {
    case FETCH_CURRENT_USER: {
      return { ...state };
    }
    case SET_USER: {
      return {
        jwt: returnProperty('jwt', action.payload, state),
        id: returnProperty('id', action.payload, state),
        firstName: returnProperty('firstName', action.payload, state),
        fullName: returnProperty('fullName', action.payload, state),
        lastName: returnProperty('lastName', action.payload, state),
        address: returnProperty('address', action.payload, state),
        gpsLocation: returnProperty('gpsLocation', action.payload, state),
        savedAddresses: returnProperty('savedAddresses', action.payload, state),
        phoneNumber: returnProperty('phoneNumber', action.payload, state),
        email: returnProperty('email', action.payload, state),
        profileImage: returnProperty('profileImage', action.payload, state),
        isLogin: returnProperty('isLogin', action.payload, state),
        rating: returnProperty('rating', action.payload, state),
      };
    }
    case SET_CURRENT_LOCATION: {
      return { ...state, gpsLocation: action.payload.coordinate };
    }
    case USER_SIGNOUT: {
      return initialState;
    }
    case ADD_SECOND_ADDRESS: {
      return { ...state, savedAddresses: [...state.savedAddresses, action.payload.newAddress] };
    }
    case CHECK_LOCATION: {
      const addresses = state.savedAddresses;
      for (let i = 0; i < addresses.length; i += 1) {
        addresses[i].selected = false;
      }
      addresses[action.payload.index].selected = true;
      return { ...state, savedAddresses: addresses };
    }
    default: {
      return state;
    }
  }
}
