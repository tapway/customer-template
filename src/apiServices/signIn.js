import HelperFetch from './HelperFetch';

/**
 * Sign In user
 * @returns {Response:true}
 */

const signInUserDb = async (phoneNumber, handleResult, handleNotFound) => {
  // eslint-disable-next-line no-undef

  const handleSuccess = res => {
    if (res.success) {
      console.log('signInUserDb', res);

      handleResult();
    } else {
      handleNotFound();
    }
  };

  const handleError = () => {};

  const extension = `/endUserAuth/login`;

  await HelperFetch(
    {
      extension,
      post: true,
      body: {
        phoneNumber,
      },
    },
    handleSuccess,
    handleError
  );
};
export default signInUserDb;
