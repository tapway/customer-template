import { StyleSheet } from 'react-native';
import { heightRatio } from '../../utils/styleSheetGuide';
import Colors from '../../utils/colors';

export default StyleSheet.create({
  mainContainer: { flex: 1 },
  mainTextConatiner: { marginTop: 30, padding: 10 },
  mainText: { fontSize: 16 * heightRatio, fontWeight: 'bold' },
  text: { fontSize: 14 * heightRatio },
  referralText: { fontSize: 13 * heightRatio },
  code: { fontSize: 17 * heightRatio, textAlign: 'center' },
  referalContainer: {
    backgroundColor: Colors.lightgrey,
    alignItems: 'center',
    height: '20%',
    justifyContent: 'center',
    marginTop: 30,
  },
  referalCodeContainer: {
    backgroundColor: Colors.white,
    width: '60%',
    alignItems: 'center',
    marginTop: 10,
    borderColor: Colors.black,
    borderWidth: 1,
    borderStyle: 'dashed',
  },
});
