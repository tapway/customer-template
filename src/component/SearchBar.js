/* eslint-disable no-use-before-define */
import React from 'react';
import {
  View,
  Image,
  StyleSheet,
  Dimensions,
  TextInput,
  TouchableWithoutFeedback,
} from 'react-native';
import { debounce } from 'lodash';
import PropTypes from 'prop-types';
import getImage from '../utils/getImage';
import Colors from '../utils/colors';

const SearchBar = props => {
  let textInputRef = null;
  const { value } = props;
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        textInputRef.focus();
      }}
    >
      <View style={styles.searchBarContainer}>
        <Image resizeMode="contain" source={getImage.search} style={styles.searchIcon} />
        <TextInput
          ref={input => {
            textInputRef = input;
          }}
          defaultValue={value}
          style={styles.searchBar}
          onChangeText={debounce(text => {
            props.handleQueryChange(text);
          }, 500)}
          placeholder="Search"
        />
      </View>
    </TouchableWithoutFeedback>
  );
};
SearchBar.propTypes = {
  value: PropTypes.string.isRequired,
  handleQueryChange: PropTypes.func.isRequired,
};
export default SearchBar;

const { height, width } = Dimensions.get('window');
const heightRatio = height / 667.0;
const widthRatio = width / 375.0;
const styles = StyleSheet.create({
  searchBarContainer: {
    marginTop: 5 * heightRatio,
    // marginBottom: 10 * heightRatio,
    flexDirection: 'row',
    height: 35 * heightRatio,
    alignItems: 'center',
    backgroundColor: Colors.searchBarColor,
    borderRadius: 19.5,
  },
  searchIcon: {
    height: 14 * heightRatio,
    width: 14 * widthRatio,
    marginLeft: 24 * widthRatio,
  },
  searchBar: {
    backgroundColor: Colors.searchBarColor,
    fontSize: 14 * heightRatio,
    marginLeft: 4 * widthRatio,
  },
});
