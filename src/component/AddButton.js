/* eslint-disable no-use-before-define */
import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { heightRatio, widthRatio } from '../utils/styleSheetGuide';
import Colors from '../utils/colors';
import { addProduct } from '../redux/action/cart';

const AddButton = props => {
  return (
    <TouchableOpacity
      onPress={() => {
        props.addItemDispatcher(props.item);
      }}
    >
      <View style={styles.addBtn}>
        <Text style={styles.addBtnTxt}>Add</Text>
      </View>
    </TouchableOpacity>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    addItemDispatcher: item => {
      dispatch(addProduct(item));
    },
  };
};

AddButton.propTypes = {
  addItemDispatcher: PropTypes.func.isRequired,
  item: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number])).isRequired,
};

export default connect(
  null,
  mapDispatchToProps
)(AddButton);

const styles = StyleSheet.create({
  addBtn: {
    borderRadius: 5,
    backgroundColor: Colors.themeBackgroundColor,
    borderColor: Colors.primaryColor,
    borderWidth: 1,
    justifyContent: 'center',
    height: 30 * heightRatio,
    width: 80 * widthRatio,
  },
  addBtnTxt: {
    color: Colors.primaryColor,
    fontSize: 14 * heightRatio,
    textAlign: 'center',
  },
});
