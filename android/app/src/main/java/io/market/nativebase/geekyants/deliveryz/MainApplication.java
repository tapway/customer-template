package io.market.nativebase.geekyants.deliveryz;

import android.app.Application;

import com.facebook.CallbackManager;
import com.facebook.react.ReactApplication;
import com.azendoo.reactnativesnackbar.SnackbarPackage;
// import com.devfd.RNGeocoder.RNGeocoderPackage;
import com.swmansion.reanimated.ReanimatedPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.arttitude360.reactnative.rnpaystack.RNPaystackPackage;
import com.arttitude360.reactnative.rngoogleplaces.RNGooglePlacesPackage;
import com.geektime.rnonesignalandroid.ReactNativeOneSignalPackage;
import io.sentry.RNSentryPackage;
import co.apptailor.googlesignin.RNGoogleSigninPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import org.devio.rn.splashscreen.SplashScreenReactPackage;
import cl.json.RNSharePackage;
import com.taessina.paypal.RNPaypalWrapperPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.razorpay.rn.RazorpayPackage;
import com.facebook.soloader.SoLoader;


import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication{
  //
  private static CallbackManager mCallbackManager = CallbackManager.Factory.create();

  protected static CallbackManager getCallbackManager() {
    return mCallbackManager;
  }
  //
  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new SnackbarPackage(),
           // new RNGeocoderPackage(),
            new ReanimatedPackage(),
            new RNGestureHandlerPackage(),
            new RNPaystackPackage(),
            new RNGooglePlacesPackage(),
            new ReactNativeOneSignalPackage(),
            new RNSentryPackage(),
            // new FBSDKPackage(mCallbackManager),
            new RNGoogleSigninPackage(),
            new SplashScreenReactPackage(),
            new RNSharePackage(),
            new RNPaypalWrapperPackage(),
            new LinearGradientPackage(),
            new RNCWebViewPackage(),
            new VectorIconsPackage(),
            new MapsPackage(),
            //  new RNFusedLocationPackage(),
             new RazorpayPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
  // @Override
  //    public String getFileProviderAuthority() {
  //           return BuildConfig.APPLICATION_ID + ".provider";
  //    }

}

