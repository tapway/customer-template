import { StyleSheet, Platform } from 'react-native';
import Colors from '../../utils/colors';
import { heightRatio, widthRatio } from '../../utils/styleSheetGuide';

export default StyleSheet.create({
  mainContainer: { flex: 1 },
  container: { flex: 1 },

  pickUpText: {
    fontSize: 14 * heightRatio,
    fontWeight: '500',
    color: Colors.textForPrimaryBgColor,
  },
  locationPlaceholder: {
    flex: 1,
    fontSize: 12 * heightRatio,
    color: Colors.placeHolderTextColorForPrimaryBg,
  },
  pickUpLocation: {
    flex: 1,
    fontSize: 12 * heightRatio,
    color: Colors.textForPrimaryBgColor,
  },
  pickUpBottomBar: {
    backgroundColor: Colors.borderWithPrimaryBgColor,
    height: 0.5 * heightRatio,
  },
  pickUpContainer: {
    height: 80 * heightRatio,
    backgroundColor: Colors.primaryColor,
    paddingHorizontal: 18 * widthRatio,
    paddingTop: 10 * heightRatio,
    paddingBottom: 15 * heightRatio,
    justifyContent: 'space-between',
  },
  footerContainer: {
    height: Platform.OS === 'ios' ? 57 * heightRatio : 48 * heightRatio,
    alignItems: 'center',
    paddingTop: Platform.OS === 'ios' ? 5 * heightRatio : 0,
    justifyContent: Platform.OS === 'ios' ? 'flex-start' : 'center',
    backgroundColor: Colors.themeBackgroundColor,
  },
  footerGoBtn: { height: 38, width: 38 },
  pencilImg: {
    height: 13 * heightRatio,
    width: 13 * heightRatio,
    marginLeft: 30 * widthRatio,
  },
  chooseLocation: {
    height: 25 * heightRatio,
    flexDirection: 'row',
    borderBottomColor: Colors.borderWithPrimaryBgColor,
    padding: 0,
    borderBottomWidth: 1,
    alignItems: 'center',
  },
});
