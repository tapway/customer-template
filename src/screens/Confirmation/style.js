import { StyleSheet } from 'react-native';

import { heightRatio, widthRatio } from '../../utils/styleSheetGuide';
import Colors from '../../utils/colors';

export default StyleSheet.create({
  container: { flex: 1, marginVertical: 119 * heightRatio },
  imgContainer: {
    height: 96 * heightRatio,
    alignItems: 'center',
  },
  smileImg: { height: 96 * heightRatio, width: 96 * heightRatio },
  thanksContainer: {
    height: 43 * heightRatio,
    marginTop: 32 * heightRatio,
  },
  thanksText: {
    textAlign: 'center',
    fontSize: 20 * heightRatio,
    fontWeight: '500',
  },
  confirmContainer: {
    height: 42 * heightRatio,
    justifyContent: 'space-evenly',
  },
  confirmationText: {
    textAlign: 'center',
    fontWeight: '100',
    fontSize: 12 * heightRatio,
  },
  homeBtnText: { fontSize: 18, color: Colors.textForPrimaryBtnBgColor },
  emailText: {
    textAlign: 'center',
    fontSize: 12 * heightRatio,
    fontWeight: '100',
  },
  trackBtnText: { fontSize: 18, color: Colors.textForThemeBgBtn },
  homeBtn: {
    height: 46 * heightRatio,
    width: 180 * widthRatio,
    backgroundColor: Colors.primaryColor,
    alignItems: 'center',
    borderRadius: 100,
    justifyContent: 'center',
  },
  trackBtn: {
    height: 46 * heightRatio,
    width: 180 * widthRatio,
    borderWidth: 0.5,
    alignItems: 'center',
    borderRadius: 100,
    justifyContent: 'center',
  },
  btnContainer: {
    flex: 1,
    marginTop: 94 * heightRatio,
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
});
