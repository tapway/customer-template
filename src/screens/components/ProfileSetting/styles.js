import { StyleSheet } from 'react-native';
import { RFPercentage, RFValue } from 'react-native-responsive-fontsize';

const styles = StyleSheet.create({
  inputContainer: {
    flex: 1,
    marginVertical: 10,
    // backgroundColor: 'red',
  },
  headtxtdiv: { position: 'absolute', left: 0, right: 0, top: RFPercentage(5.5), zIndex: -100 },
  inputLabel: {
    fontSize: 18,
    fontWeight: '100',
    color: '#858F99',
    // opacity: 0.7,
  },
  inputItem: {
    color: '#383838',
    borderWidth: 0,
    fontSize: 22,
    marginTop: 3,
    paddingHorizontal: 10,
    height: 55,
    fontFamily: 'Nunito-Regular',
    backgroundColor: '#EFF4F5',
    borderColor: '#EFF4F5',
    borderRadius: 5,
    paddingLeft: 10,
    width: '100%',
  },
  inputText: {
    color: '#383838',
    // opacity: 0.9,
  },
});

export default styles;
