import { CHANGE_DESTINATION } from '../actionTypes';

/**
 * Set Drop Location To Redux
 */

const setDestination = (name, coords) => {
  return {
    type: CHANGE_DESTINATION,
    payload: {
      name,
      coords,
    },
  };
};
export default setDestination;
