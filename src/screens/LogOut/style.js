import { StyleSheet } from 'react-native';
import { heightRatio, widthRatio } from '../../utils/styleSheetGuide';
import Colors from '../../utils/colors';

export default StyleSheet.create({
  backgroundContainer: {
    flex: 1,
    backgroundColor: Colors.modalBackgroundColor,
    justifyContent: 'flex-end',
  },
  mainContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.modalContainerColor,
    borderTopColor: Colors.modalBorderColor,
    borderTopWidth: 4,
    elevation: 80,
    flexDirection: 'column',
    paddingHorizontal: 75 * widthRatio,
    paddingVertical: 20 * heightRatio,
  },
  signOutImg: { height: 50 * heightRatio, width: 45 * heightRatio },
  sureText: {
    marginTop: 16 * heightRatio,
    fontSize: 14 * heightRatio,
    fontWeight: '300',
    color: Colors.textForModalContainerColor,
    textAlign: 'center',
    lineHeight: 22 * heightRatio,
  },
  btns: {
    height: 56,
    width: 180 * widthRatio,
    borderRadius: (46 / 2) * heightRatio,
    backgroundColor: Colors.secondaryBtnBgColor,
    alignItems: 'center',
    justifyContent: 'center',
  },
  primaryColor: { backgroundColor: Colors.primaryColor },
  btnText: { fontSize: 16 * heightRatio, color: Colors.textForPrimaryBtnBgColor },
});
