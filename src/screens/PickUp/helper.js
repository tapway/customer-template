import { getDistance } from 'geolib';

const getInitialState = () => {
  return {
    searchLocationModalVisible: false,
    getDetailsModalVisible: false,
    confirmOrderModalVisible: false,
    tripEstimate: false,

    itemDetails: [],
    detailAddress: '',
    detailAddress2: '',

    origin: 'Search here...',
    destination: 'Search here...',
    data: {},
    details: {},
    region: {
      latitude: 6.442077,
      longitude: 3.479652,
      latitudeDelta: 0.05,
      longitudeDelta: 0.05,
    },
    id: 0,
    marker: [
      {
        title: 'pickup',
        coordinates: {
          latitude: 0,
          longitude: 0,
        },
      },
      {
        title: 'drop',
        coordinates: {
          latitude: 0,
          longitude: 0,
        },
      },
    ],
  };
};

const handleSetAddress = (name, coords, marker, id, handleStateChange) => {
  const mark = marker;
  mark[id].coordinates = {
    latitude: coords.lat,
    longitude: coords.lng,
  };

  let dist = getDistance(marker[0].coordinates, {
    latitude: coords.lat,
    longitude: coords.lng,
  });
  dist = (dist * 1.6) / 1000;
  let delta = 0;

  if (id === 0) {
    delta = 0.05;
    handleStateChange({
      origin: name,
      marker: mark,
      id: 1,
    });
  }

  if (id === 1) {
    delta = dist * 0.018;
    handleStateChange({
      destination: name,
      marker: mark,
      id: 2,
    });
  }
  handleStateChange({
    region: {
      latitude: coords.lat,
      longitude: coords.lng,
      latitudeDelta: delta,
      longitudeDelta: delta,
    },
  });
};

const getRegisterOrderObject = (props, state) => {
  const { detailAddress, detailAddress2, itemDetails } = state;
  return {
    userId: props.user.id,
    paymentMode: 'Cash',
    paymentAmount:
      (getDistance(state.marker[0].coordinates, state.marker[1].coordinates) * 20) / 1000,
    endUserGpsLocation: state.marker[0].coordinates,
    deliveryLocation: [state.marker[1].coordinates.longitude, state.marker[1].coordinates.latitude],
    deliveryInstructions: {
      detailAddress,
      detailAddress2,
      itemDetails,
    },
    pickUpLocation: [state.marker[0].coordinates.longitude, state.marker[0].coordinates.latitude],
    deliveryAddress: state.destination,
    pickUpAddress: state.origin,
    category: 'Pickup',
  };
};
export { getInitialState, handleSetAddress, getRegisterOrderObject };
