import React from 'react';
import { View, ScrollView } from 'react-native';
import { PlaceholderContainer, Placeholder } from 'react-native-loading-placeholder';
import LinearGradient from 'react-native-linear-gradient';
import PropTypes from 'prop-types';
import styles from './style';

const Loading = props => {
    const { loadingComponent } = props;
    return <PlaceholderExample loader={loadingComponent} />;
};

const Gradient = () => {
    return (
        <LinearGradient
            colors={['#eeeeee', '#dddddd', '#eeeeee']}
            start={{ x: 1.0, y: 0.0 }}
            end={{ x: 0.0, y: 0.0 }}
            style={{
                flex: 1,
                width: 120,
            }}
        />
    );
};

const PlaceholderExample = ({ loader }) => {
    const array = ['1', '2', '3', '4', '5', '6', '7'];
    return (
        <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
            <PlaceholderContainer
                animatedComponent={<Gradient />}
                duration={1000}
                delay={1000}
                loader={loader}
            >
                {array.map(item => {
                    return (
                        <View style={[styles.placeholderSingleContainer]} key={item}>
                            <View style={styles.flexRow}>
                                <View>
                                    <Placeholder style={[styles.placeholder, styles.placeHolderImage]}></Placeholder>
                                </View>
                                <View style={styles.resDetails}>
                                    <Placeholder
                                        style={[styles.placeholder, styles.placeHolderLocation]}
                                    ></Placeholder>
                                    <Placeholder
                                        style={[styles.placeholder, styles.placeHolderLocation]}
                                    ></Placeholder>
                                    <Placeholder style={[styles.placeholder, styles.placeHolderLocation]} />
                                </View>
                            </View>
                        </View>
                    );
                })}
            </PlaceholderContainer>
        </ScrollView>
    );
};
Loading.propTypes = {
    loadingComponent: PropTypes.objectOf(PropTypes.any),
};
Loading.defaultProps = {
    loadingComponent: undefined,
};

PlaceholderExample.propTypes = {
    loader: PropTypes.objectOf(PropTypes.any),
};
PlaceholderExample.defaultProps = {
    loader: undefined,
};
export default Loading;
