/* eslint-disable no-use-before-define */
import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import Colors from '../utils/colors';

const FoodTypeBtn = props => {
  const { vegStatus } = props;
  return vegStatus ? (
    <View style={styles.foodTypeBtn}>
      <TouchableOpacity
        style={[styles.foodTypeBtnActive, styles.vegBtn]}
        onPress={() => props.changeFoodType('Veg', true)}
      >
        <Text style={[styles.secondaryColor, styles.foodTypeBtnTxt]}>Veg</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={[styles.foodTypeBtnInActive, styles.nonVegBtn]}
        onPress={() => props.changeFoodType('NonVeg', false)}
      >
        <Text style={styles.foodTypeBtnTxt}>NonVeg</Text>
      </TouchableOpacity>
    </View>
  ) : (
    <View style={styles.foodTypeBtn}>
      <TouchableOpacity
        style={[styles.foodTypeBtnInActive, styles.vegBtn]}
        onPress={() => props.changeFoodType('Veg', true)}
      >
        <Text style={styles.foodTypeBtnTxt}>Veg</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={[styles.foodTypeBtnActive, styles.nonVegBtn]}
        onPress={() => props.changeFoodType('NonVeg', false)}
      >
        <Text style={[styles.secondaryColor, styles.foodTypeBtnTxt]}>NonVeg</Text>
      </TouchableOpacity>
    </View>
  );
};
FoodTypeBtn.propTypes = {
  vegStatus: PropTypes.bool.isRequired,
  changeFoodType: PropTypes.func.isRequired,
};
export default FoodTypeBtn;

const { height, width } = Dimensions.get('window');
const heightRatio = height / 667.0;
const widthRatio = width / 375.0;
const styles = StyleSheet.create({
  secondaryColor: {
    color: Colors.textForPrimaryBgColor,
  },
  foodTypeBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  foodTypeBtnActive: {
    backgroundColor: Colors.primaryColor,
    justifyContent: 'center',
  },
  vegBtn: {
    width: 90 * widthRatio,
    height: 29 * heightRatio,

    borderTopLeftRadius: 2,
    borderBottomLeftRadius: 2,
  },
  nonVegBtn: {
    width: 90 * widthRatio,
    height: 29 * heightRatio,

    borderTopRightRadius: 2,
    borderBottomRightRadius: 2,
  },
  foodTypeBtnInActive: {
    backgroundColor: Colors.themeBackgroundColor,
    borderWidth: 1,
    borderColor: Colors.primaryColor,
    justifyContent: 'center',
  },
  foodTypeBtnTxt: {
    textAlign: 'center',
    fontSize: 13 * heightRatio,
    fontWeight: '600',
  },
});
