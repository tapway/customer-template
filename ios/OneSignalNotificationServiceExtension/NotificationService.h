//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by Anurag Garg on 07/08/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
