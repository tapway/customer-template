import SERVER from '../config/config';

const HelperFetch = async (properties, handleSuccess, handleFailure) => {
  const method = properties.get ? 'GET' : properties.post ? 'POST' : properties.put ? 'PUT' : 'GET';

  const headers = properties.Authorization
    ? {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: properties.Authorization,
      }
    : {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      };
  const parameters = properties.body
    ? {
        method,
        headers,
        body: JSON.stringify(properties.body),
      }
    : { method, headers };

  try {
    // eslint-disable-next-line no-undef
    const res = await fetch(`${SERVER}/api${properties.extension}`, parameters);
    const myJson = await res.json();
    handleSuccess(myJson);
  } catch (err) {
    handleFailure(err);
  }
};
export default HelperFetch;
