/* eslint-disable no-underscore-dangle */
import { ADD_PRODUCT, INCREASE_QUANTITY, DECREASE_QUANTITY, CLEAR_CART } from '../actionTypes';

const { findIndex } = require('lodash');

export default function(state = [], action) {
  switch (action.type) {
    case ADD_PRODUCT: {
      return action.payload;
    }
    case INCREASE_QUANTITY: {
      const arr = [...state];
      const selectedIndex = findIndex(arr, { _id: action.payload.item._id });
      arr[selectedIndex].quantity += 1;
      return [...arr];
    }
    case DECREASE_QUANTITY: {
      const arr = [...state];
      const selectedIndex = findIndex(arr, {
        _id: action.payload.item._id,
      });
      if (arr[selectedIndex].quantity === 1) {
        arr.splice(selectedIndex, 1);
      } else {
        arr[selectedIndex].quantity -= 1;
      }
      return [...arr];
    }
    case CLEAR_CART: {
      return [];
    }
    default: {
      return state;
    }
  }
}
