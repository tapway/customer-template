import { StyleSheet, Dimensions, Platform } from 'react-native';
import Colors from '../../utils/colors';

const { height, width } = Dimensions.get('window');
const heightRatio = height / 667.0;
const widthRatio = width / 375.0;
export default StyleSheet.create({
  container: {
    flex: 1,
  },
  fullFlex: { flex: 1 },
  mainContainer: { flex: 1 },
  locationsContainer: {
    height: 137 * heightRatio,
    flexDirection: 'column',
    backgroundColor: Colors.primaryColor,
    paddingHorizontal: 18 * widthRatio,
    paddingTop: 12 * heightRatio,
  },
  pickUpContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pickUpText: {
    color: Colors.textForPrimaryBgColor,
    fontSize: 14 * heightRatio,
    fontWeight: '500',
    justifyContent: 'flex-start',
  },
  locPlaceholder: {
    color: Colors.placeHolderTextColorForPrimaryBg,
    fontSize: 12 * heightRatio,
    width: (65 / 100) * width,
    marginBottom: 6 * heightRatio,
  },
  pickUpLocation: {
    color: Colors.textForPrimaryBgColor,
    fontSize: 12 * heightRatio,
    width: (65 / 100) * width,
    marginBottom: 3 * heightRatio,
  },
  location: {
    borderBottomColor: Colors.textForPrimaryBgColor,
    borderBottomWidth: 1,
    width: 314 * widthRatio,
    marginTop: 8 * heightRatio,
    marginLeft: 24 * heightRatio,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  footer: {
    height: Platform.OS === 'ios' ? 57 * heightRatio : 52 * heightRatio,
    alignItems: 'center',
    paddingTop: Platform.OS === 'ios' ? 5 * heightRatio : 0,
    justifyContent: Platform.OS === 'ios' ? 'flex-start' : 'center',
  },
  pickUpImg: {
    height: 10 * heightRatio,
    width: 10 * heightRatio,
    marginRight: 14 * heightRatio,
  },
  dropLocImg: {
    height: 16.2 * heightRatio,
    width: 12.8 * heightRatio,
    marginRight: 11.2 * heightRatio,
    justifyContent: 'flex-start',
  },
  searchImg: {
    height: 12.9 * widthRatio,
    width: 13 * widthRatio,
  },
  dropContainer: {
    marginTop: 15 * heightRatio,
    flexDirection: 'row',
    alignItems: 'center',
  },
  footerImg: { height: 35 * heightRatio, width: 35 * heightRatio },
});
