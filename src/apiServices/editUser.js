import HelperFetch from './HelperFetch';

/**
 * Add new address
 * @returns {Response:true}
 */

const editUser = async (user, userId, jwtAccessToken, handleResult) => {
    console.log('userDetails', user);
    const handleSuccess = res => {
        console.log(res);
        if (res.success) {
            handleResult(res.data);
        }
    };
    const handleError = () => { };

    const extension = `/endUser/update/${userId}`;

    await HelperFetch(
        {
            extension,
            put: true,
            body: {
                firstName: user.firstName,
                lastName: user.lastName,
                //fullName: user.firstName ? user.firstName : '' + user.lastName ? user.lastName : '',
                address: user.address,
                phoneNumber: user.phoneNumber
            },
            Authorization: jwtAccessToken,
        },
        handleSuccess,
        handleError
    );
};
export default editUser;
