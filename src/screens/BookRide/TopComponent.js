import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import styles from './style';
import getImage from '../../utils/getImage';

const TopComponent = props => {
  const { origin, destination } = props;
  return (
    <View style={styles.locationsContainer}>
      <View style={styles.pickUpContainer}>
        <Image resizeMode="contain" source={getImage.pickupDot} style={styles.pickUpImg} />
        <Text style={styles.pickUpText}>Pickup from</Text>
      </View>
      <View>
        <TouchableOpacity onPress={() => props.handleLocation(0)} style={styles.location}>
          <Text
            numberOfLines={1}
            style={origin === 'Search here...' ? styles.locPlaceholder : styles.pickUpLocation}
          >
            {origin}
          </Text>

          <Image resizeMode="contain" source={getImage.secondSearch} style={styles.searchImg} />
        </TouchableOpacity>
      </View>
      <View style={styles.dropContainer}>
        <Image resizeMode="contain" source={getImage.dropLoc} style={styles.dropLocImg} />
        <Text style={styles.pickUpText}>Drop location</Text>
      </View>
      <TouchableOpacity onPress={() => props.handleLocation(1)} style={styles.location}>
        <Text
          numberOfLines={1}
          style={destination === 'Search here...' ? styles.locPlaceholder : styles.pickUpLocation}
        >
          {destination}
        </Text>
        <Image resizeMode="contain" source={getImage.secondSearch} style={styles.searchImg} />
      </TouchableOpacity>
    </View>
  );
};

TopComponent.propTypes = {
  origin: PropTypes.string.isRequired,
  destination: PropTypes.string.isRequired,
  handleLocation: PropTypes.func.isRequired,
};

export default TopComponent;
