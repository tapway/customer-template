/* eslint-disable no-use-before-define */
import React from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { heightRatio, widthRatio } from '../utils/styleSheetGuide';
import Colors from '../utils/colors';

const Button = props => {
  const { submit, title, style } = props;
  return (
    <TouchableOpacity style={style || styles.loginButton} onPress={submit}>
      <Text style={styles.loginText}>{title}</Text>
    </TouchableOpacity>
  );
};
Button.propTypes = {
  submit: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  style: PropTypes.oneOfType([
    PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.array])),
    PropTypes.arrayOf(
      PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.array]))
    ),
  ]),
};
Button.defaultProps = {
  style: undefined,
};
export default Button;

const styles = StyleSheet.create({
  loginButton: {
    width: widthRatio * 300,
    alignItems: 'center',
    padding: 13,
    borderRadius: 50,
    backgroundColor: Colors.primaryColor,
    justifyContent: 'center',
    borderColor: Colors.transparent,
  },
  loginText: {
    color: Colors.themeBackgroundColor,
    fontSize: heightRatio * 16,
    fontWeight: '400',
    letterSpacing: 0.4,
  },
});
