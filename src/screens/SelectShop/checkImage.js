import getImage from '../../utils/getImage';

const checkImage = value => {
  switch (value) {
    case 'Foods':
      return [getImage.superMart, getImage.grocery, getImage.ece];
    case 'Electronincs':
      return [getImage.ece, getImage.ece, getImage.ece];
    case 'Sports':
      return [getImage.superMart, getImage.grocery, getImage.ece];
    case 'Groceries':
      return [getImage.superMart, getImage.grocery, getImage.ece];
    case 'Medicals':
      return [getImage.superMart, getImage.grocery, getImage.ece];
    case 'Pet Supplies':
      return [getImage.superMart, getImage.grocery, getImage.ece];
    default:
      return [getImage.ece, getImage.ece, getImage.ece];
  }
};

export default checkImage;
