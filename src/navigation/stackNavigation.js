/* eslint*/
import React from 'react';
import {
  createStackNavigator,
  createBottomTabNavigator,
} from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';

import WelcomeScreen from '../screens/WelcomeScreen';
import {
  fadeIn,
  zoomIn,
  fromLeft,
  fromRight,
} from "react-navigation-transitions";
// import SignUp from '../screens/SignUp';
import Scenes from '../screens/Scenes';
import SignUp from '../screens/SignUp2';
import CheckDeliveryTime from '../screens/CheckDeliveryTime';
import CheckNearestDriver from '../screens/CheckNearestDriver';
import CheckPrice from '../screens/CheckPrice';
import ForgetPassword from '../screens/ForgetPassword';
import Verify from '../screens/Verify';
import Login from '../screens/SignIn';
import Home from '../screens/Home';
import BookRide from '../screens/BookRide';
import Restaurants from '../screens/Restaurants';
import SelectOrderType from '../screens/SelectOrderType';
import ChooseMenu from '../screens/ChooseMenu';
import SelectShop from '../screens/SelectShop';
import ChooseProduct from '../screens/ChooseProduct';
import PickUp from '../screens/PickUp';
import LogOut from '../screens/LogOut';
import OrderHistory from '../screens/OrderHistory';
import Profile from '../screens/Profile';
import About from '../screens/About';
import SavedAddress from '../screens/SavedAddress';
import Confirmation from '../screens/Confirmation';
import PaymentResult from '../screens/PaymentResult';
import RegistrationVerify from '../screens/RegistrationVerify';
import ConfirmOrder from '../screens/ConfirmOrder';
import SuccessfullyBooked from '../screens/SuccessfullyBooked';
import Paystack from '../screens/Paystack';
import RatingPage from '../screens/Rating';
import AuthLoading from "../screens/WelcomeScreen/AuthLoading";
import TermsOfService from '../screens/components/termsOfService'
import  PrivacyPolicy from '../screens/components/privacyPolicy'
import { widthRatio } from '../utils/styleSheetGuide';
import Colors from '../utils/colors';

const checkRouteName = routeName => {
  switch (routeName) {
    case 'home':
      return 'home';
    case 'profile':
      return 'user';
    case 'selectOrderType':
      return 'list';
    default:
      return null;
  }
};
const handleCustomTransition = ({ scenes }) => {
  const prevScene = scenes[scenes.length - 2];
  const nextScene = scenes[scenes.length - 1];

  // Custom transitions go there
  if (
    prevScene &&
    prevScene.route.routeName === "welcomeScreen" &&
    nextScene.route.routeName === "login"
  ) {
    return zoomIn();
  }
  //  else if (
  //   prevScene &&
  //   prevScene.route.routeName === "Splash" &&
  //   nextScene.route.routeName === "HomeScreens"
  // ) {
  //   return fromRight(500);
  // }
  return fromRight();
};
const tabNav = createBottomTabNavigator(
  {
    home: { screen: Home },
    selectOrderType: { screen: SelectOrderType },
    profile: { screen: Profile },
  },
  {
    navigationOptions: ({ navigation }) => ({
      // eslint-disable-next-line react/prop-types
      tabBarIcon: ({ tintColor }) => {
        const { routeName } = navigation.state;
        return (
          <Icon
            name={checkRouteName(routeName)}
            size={checkRouteName(routeName) === 'home' ? 28 : 25}
            style={{ color: tintColor }}
          />
        );
      },
    }),
    tabBarOptions: {
      activeTintColor: Colors.primaryColor,
      showLabel: false,
      style: {
        paddingHorizontal: 30 * widthRatio,
      },
    },
  }
);

const StackNav = createStackNavigator(
  {
    tabNav: { screen: tabNav },
    welcomeScreen: { screen: WelcomeScreen },
    signUp: { screen: SignUp },
    scenes: { screen: Scenes },
    checkDeliveryTime: { screen: CheckDeliveryTime },
    checkNearestDriver: { screen: CheckNearestDriver },
    checkPrice: { screen: CheckPrice },
    forgetPassword: { screen: ForgetPassword },
    verify: { screen: Verify },
    registrationVerify: { screen: RegistrationVerify },
    logOut: { screen: LogOut },
    bookRide: { screen: BookRide },
    orderHistory: { screen: OrderHistory },
    savedAddress: { screen: SavedAddress },
    about: { screen: About },
    paymentResult: { screen: PaymentResult },
    selectShop: { screen: SelectShop },
    login: { screen: Login },
    chooseProduct: { screen: ChooseProduct },
    confirmOrder: { screen: ConfirmOrder },
    successfullyBooked: { screen: SuccessfullyBooked },
    pickupDrop: { screen: PickUp },
    restaurant: { screen: Restaurants },
    chooseMenu: { screen: ChooseMenu },
    confirmation: { screen: Confirmation },
    ratingPage: { screen: RatingPage },
    paystack: { screen: Paystack },
    AuthLoading: { screen: AuthLoading },
    termsOfService: { screen: TermsOfService},
    privacyPolicy: {screen: PrivacyPolicy}
  },
  {
    initialRouteName: 'AuthLoading',
    headerMode: 'none',
    cardStyle: { backgroundColor: Colors.themeBackgroundColor },
    options: { swipeEnabled: false },
    navigationOptions: { gesturesEnabled: false, headerMode: false },
    transitionConfig: nav => handleCustomTransition(nav),
    defaultNavigationOptions: { gesturesEnabled: false },
  }
);

export default StackNav;
