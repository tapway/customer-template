import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import PropTypes from 'prop-types';
import styles from './style';
import getImage from '../../utils/getImage';

const TopComponent = props => {
  const { origin, handleLocation } = props;
  return (
    <View style={styles.pickUpContainer}>
      <Text style={styles.pickUpText}>Pickup location</Text>
      <TouchableOpacity
        onPress={() => {
          handleLocation(0);
        }}
        style={styles.chooseLocation}
      >
        <Text
          numberOfLines={1}
          style={origin === 'Search here...' ? styles.locationPlaceholder : styles.pickUpLocation}
        >
          {origin}
        </Text>
        <Image resizeMode="contain" source={getImage.secondSearch} style={styles.pencilImg} />
      </TouchableOpacity>
    </View>
  );
};
TopComponent.propTypes = {
  origin: PropTypes.string.isRequired,
  handleLocation: PropTypes.func.isRequired,
};
export default TopComponent;
