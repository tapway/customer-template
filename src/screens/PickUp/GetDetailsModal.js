/* eslint-disable react/no-string-refs */
/* eslint-disable no-use-before-define */
import Toast from 'react-native-easy-toast';
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList, StyleSheet, Modal, Platform } from 'react-native';
import { CheckBox } from 'native-base';
import { indexOf, pull } from 'lodash';
import PropTypes from 'prop-types';
import { heightRatio } from '../../utils/styleSheetGuide';
import Colors from '../../utils/colors';

export default class GetDetailsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItemArray: [],
    };
  }

  _renderHeader = () => {
    return (
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>Select Item Type</Text>
      </View>
    );
  };

  _itemSeparator = () => {
    return (
      <View
        style={{
          backgroundColor: Colors.primaryBorderColor,
          height: 1,
          width: '100%',
        }}
      ></View>
    );
  };

  _renderFooter = () => {
    return (
      <View style={styles.footerContainer}>
        <TouchableOpacity
          style={styles.doneBtn}
          onPress={() => {
            this.handleDone();
          }}
        >
          <Text style={styles.doneBtnText}>Done</Text>
        </TouchableOpacity>
      </View>
    );
  };

  _renderItem = item => {
    const { state } = this;
    const active = indexOf(state.selectedItemArray, item) !== -1;
    return (
      <TouchableOpacity
        style={styles.itemContainer}
        onPress={() => {
          this.changeSelection(item);
        }}
      >
        <Text style={active ? styles.itemTextActive : styles.itemTextInActive}>{item}</Text>
        <CheckBox
          style={{ fontSize: 12 }}
          checked={active}
          color={Colors.primaryColor}
          onPress={() => this.changeSelection(item)}
        />
      </TouchableOpacity>
    );
  };

  changeSelection = item => {
    this.setState(prevState => {
      if (indexOf(prevState.selectedItemArray, item) !== -1) {
        const arr = pull(prevState.selectedItemArray, item);
        return { selectedItemArray: arr };
      }
      return { selectedItemArray: [...prevState.selectedItemArray, item] };
    });
  };

  handleDone = () => {
    const { props, state } = this;
    if (state.selectedItemArray.length === 0) {
      this.refs.toast.show('Please select at least one type');
    } else {
      props.handleStateChange({ itemDetails: state.selectedItemArray });
      props.toggleGetDetailsModal();
      props.toggleConfirmOrderModal();
    }
  };

  render() {
    const itemType = ['Documents/Books', 'Foods', 'Sports', 'Electronic Items', 'Others'];

    const { props, state } = this;
    return (
      <Modal animated animationType="fade" visible={props.getDetailsModalVisible} transparent>
        <View style={styles.backgroundContainer}>
          <View style={styles.mainContainer}>
            {this._renderHeader()}
            <FlatList
              bounces={false}
              extraData={state.selectedItemArray}
              showsVerticalScrollIndicator={false}
              ItemSeparatorComponent={this._itemSeparator}
              data={itemType}
              contentContainerStyle={{
                paddingHorizontal: 35,
              }}
              renderItem={({ item }) => this._renderItem(item)}
            ></FlatList>
            {this._renderFooter()}
          </View>
          <Toast
            ref="toast"
            style={{ backgroundColor: Colors.toastBgColor }}
            position="bottom"
            positionValue={400}
            fadeInDuration={750}
            fadeOutDuration={1000}
            opacity={0.8}
            textStyle={{ color: Colors.toastTextColor, fontSize: 15 }}
          />
        </View>
      </Modal>
    );
  }
}

GetDetailsModal.propTypes = {
  handleStateChange: PropTypes.func.isRequired,
  toggleConfirmOrderModal: PropTypes.func.isRequired,
  toggleGetDetailsModal: PropTypes.func.isRequired,
  getDetailsModalVisible: PropTypes.bool.isRequired,
};

const styles = StyleSheet.create({
  backgroundContainer: {
    flex: 1,
    backgroundColor: Colors.modalBackgroundColor,
    position: 'relative',
  },
  mainContainer: {
    position: 'absolute',
    bottom: 0,
    height: 350 * heightRatio,
    width: '100%',
    backgroundColor: Colors.modalContainerColor,
  },
  headerContainer: {
    height: 55 * heightRatio,
    width: '100%',
    justifyContent: 'center',
    paddingHorizontal: 35,
  },
  headerText: { fontSize: 20, fontWeight: '500' },
  itemContainer: {
    height: 44.2 * heightRatio,
    width: '100%',
    paddingRight: 20,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  itemTextActive: { fontSize: 14 * heightRatio, fontWeight: '500' },
  itemTextInActive: { fontSize: 14 * heightRatio, fontWeight: '300' },
  footerContainer: {
    paddingHorizontal: 35,
    borderTopWidth: 1,
    borderTopColor: Colors.primaryBorderColor,
    height: Platform.OS === 'ios' ? 57 * heightRatio : 48 * heightRatio,
    alignItems: 'center',
    justifyContent: 'center',
  },
  doneBtn: {
    width: '100%',
    height: 50,
    backgroundColor: Colors.primaryBtnBgColor,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  doneBtnText: {
    color: Colors.textForPrimaryBtnBgColor,
    fontSize: 20,
    fontWeight: '500',
  },
});
