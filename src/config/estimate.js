const perKm = 20;
const tax = 5;
const DELIVERY_CHARGES = 4;
const MIN_DEL_CHARGES = 100;
export default {
  perKm,
};
export { tax, DELIVERY_CHARGES, MIN_DEL_CHARGES };
