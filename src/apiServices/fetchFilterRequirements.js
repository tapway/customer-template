import HelperFetch from './HelperFetch';

/**
 * Get filter requirements for items
 * @returns {Response:true,data}
 */

const handleFailure = () => {};

const fetchFilterRequirements = async (shopId, handleSuccess) => {
  const extension = `/dashboard/items/getFilterRequirementsByShop`;
  await HelperFetch(
    {
      extension,
      post: true,
      body: {
        id: shopId,
      },
    },
    handleSuccess,
    handleFailure
  );
};
export default fetchFilterRequirements;
