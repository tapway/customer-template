import { ADD_SECOND_ADDRESS, CHECK_LOCATION } from '../actionTypes';

/**
 * Add address To Redux
 */
export const addAddress = userAddress => {
  return {
    type: ADD_SECOND_ADDRESS,
    payload: {
      newAddress: userAddress,
    },
  };
};

/**
 * Check Location From Redux
 */
export const checkLocation = index => {
  return {
    type: CHECK_LOCATION,
    payload: { index },
  };
};
