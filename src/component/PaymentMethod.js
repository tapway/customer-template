/* eslint-disable no-use-before-define */
import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import PropTypes from 'prop-types';
import { heightRatio, widthRatio } from '../utils/styleSheetGuide';
import Colors from '../utils/colors';

export default class PaymentMethod extends Component {
  constructor(props) {
    super(props);
    this.state = {
      methods: [{ value: 'Cash' }, { value: 'Others' }],
    };
  }

  render() {
    const { methods } = this.state;
    const { props } = this;
    return (
      <View style={styles.container}>
        <Dropdown
          containerStyle={styles.containerStyle}
          data={methods}
          value="Cash"
          onChangeText={value => props.payMethodChange(value)}
        />
      </View>
    );
  }
}
PaymentMethod.propTypes = {
  payMethodChange: PropTypes.func.isRequired,
};
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  containerStyle: {
    width: widthRatio * 70,
    backgroundColor: Colors.themeBackgroundColor,
    borderStyle: 'solid',
    marginLeft: heightRatio * 5,
    borderColor: '#EDEDED',
    justifyContent: 'center',
    paddingBottom: heightRatio * 20,
  },
});
