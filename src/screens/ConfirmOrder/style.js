import { StyleSheet, Platform } from 'react-native';
import { heightRatio, widthRatio } from '../../utils/styleSheetGuide';
import Colors from '../../utils/colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.themeBackgroundColor,
  },
  locationView: {
    backgroundColor: Colors.highlightedContainer,
    height: heightRatio * 130,
    paddingLeft: widthRatio * 25,
    paddingRight: widthRatio * 35,
  },
  pickUpConatiner: {
    flex: 1,
    marginTop: heightRatio * 10,
    flexDirection: 'row',
  },
  iconContainer: {},
  image: {
    height: heightRatio * 10,
    width: widthRatio * 10,
    overflow: 'visible',
    marginTop: heightRatio * 3,
    marginRight: widthRatio * 10,
  },
  pickupImage: {
    height: heightRatio * 18.2,
    width: widthRatio * 12.8,
    overflow: 'visible',
    marginTop: heightRatio * 1,
    marginRight: widthRatio * 10,
  },
  headingPickUpText: {
    fontSize: heightRatio * 12,
    color: Colors.textForHighlightedContainer,
  },
  pickUpDescriptionContainer: {
    flexDirection: 'row',
    paddingBottom: heightRatio * 5,
    marginTop: heightRatio * 10,
    borderBottomWidth: 0.5,
    alignItems: 'center',
    width: widthRatio * 300,
    borderColor: Colors.secondaryBorderColor,
  },
  dropDescriptionContainer: {
    flexDirection: 'row',
    paddingBottom: heightRatio * 5,
    marginTop: heightRatio * 10,
    paddingRight: widthRatio * 10,
    borderBottomWidth: 0.5,
    justifyContent: 'space-between',
    alignItems: 'center',
    width: widthRatio * 300,
    borderColor: Colors.secondaryBorderColor,
  },
  pickUpDescriptionText: {
    fontSize: heightRatio * 12,
    fontWeight: '200',
    color: Colors.textForHighlightedContainer,
  },
  pencilImg: {
    height: 12 * heightRatio,
    marginLeft: widthRatio * 10,
    marginRight: widthRatio * 10,
    width: 12 * widthRatio,
  },
  mainContainer: {
    flex: 0.4,
    paddingTop: heightRatio * 15,
    paddingLeft: widthRatio * 15,
    paddingRight: widthRatio * 15,
  },
  productsContainer: Platform.select({
    ios: {
      padding: heightRatio * 15,
      marginBottom: heightRatio * 5,
      flex: 1,
      backgroundColor: Colors.themeBackgroundColor,
      shadowColor: Colors.primaryShadowColor,
      shadowOffset: { width: 0.3, height: 0.3 },
      shadowOpacity: 0.5,
      shadowRadius: 1,
    },
    android: {
      padding: heightRatio * 15,
      marginBottom: heightRatio * 5,
      flex: 1,
      backgroundColor: Colors.themeBackgroundColor,
      borderWidth: 1,
      borderColor: Colors.primaryBorderColor,
    },
  }),
  invoiceConatiner: Platform.select({
    ios: {
      padding: heightRatio * 15,
      marginBottom: heightRatio * 50,
      flex: 1,
      backgroundColor: Colors.themeBackgroundColor,
      shadowColor: Colors.primaryShadowColor,
      shadowOffset: { width: 0.3, height: 0.3 },
      shadowOpacity: 0.5,
      shadowRadius: 1,
    },
    android: {
      padding: heightRatio * 15,
      marginBottom: heightRatio * 50,
      flex: 1,
      backgroundColor: Colors.themeBackgroundColor,
      borderWidth: 1,
      borderColor: Colors.primaryBorderColor,
    },
  }),
  productsHeading: {
    fontSize: heightRatio * 15,
    fontWeight: '500',
  },
  productList: {
    marginTop: heightRatio * 10,
  },
  partnerChargesContainer: {
    marginTop: heightRatio * 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  partnerChargesText: {
    fontSize: heightRatio * 12,
  },
  grandTotalContainer: {
    marginTop: heightRatio * 12,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 0.5,
    borderColor: Colors.secondaryBorderColor,
    paddingBottom: heightRatio * 20,
  },
  paidConatiner: {
    flexDirection: 'row',
  },
  paidTextFirst: { fontSize: heightRatio * 12, fontWeight: 'bold' },
  paidTextSecond: {
    fontSize: heightRatio * 12,
    color: Colors.textForThemeBgColor,
    fontWeight: '500',
  },
  estimateContainer: {
    backgroundColor: Colors.themeBackgroundColor,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: heightRatio * 20,
  },
  sigleListContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: heightRatio * 8,
    paddingTop: heightRatio * 8,
  },
  itemSeperator: {
    height: 1,
    width: '100%',
    backgroundColor: Colors.primaryBorderColor,
  },
  sigleListQuantityCont: {
    marginRight: widthRatio * 10,
  },
  itemDescriptionText: {
    fontSize: heightRatio * 12,
    fontWeight: '400',
    width: widthRatio * 250,
  },
  menuDescriptionText: {
    fontSize: heightRatio * 12,
    fontWeight: '400',
    width: widthRatio * 250,
  },
  itemBrand: {
    flexDirection: 'row',
    marginTop: heightRatio * 5,
  },
  itemBrandText: {
    fontWeight: '200',
  },
  sigleListDescription: { flex: 1 },
  itemBrandDescText: {
    fontWeight: 'bold',
  },
  itemPriceText: {
    color: Colors.textForThemeBgColor,
    fontWeight: '200',
  },
  quantityCont: {
    padding: widthRatio * 5,
  },
  quantityText: {
    color: Colors.textForThemeBgColor,
    fontSize: heightRatio * 15,
    fontWeight: '200',
  },
});
