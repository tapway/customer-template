/* eslint-disable no-undef */
/* eslint-disable no-use-before-define */
import React, { PureComponent } from 'react';
import {
  StyleSheet,
  View,
  Modal,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  TextInput,
  Keyboard,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';
import { heightRatio, widthRatio } from '../utils/styleSheetGuide';
import Colors from '../utils/colors';
import Header from './HeaderBar';

export default class AddressModal extends PureComponent {
  constructor() {
    super();
    this.state = {
      type: 'Others',
      active: true,
    };
  }

  render() {
    const { props, state } = this;
    const { addressModalVisible } = this.props;
    return (
      <Modal
        animationType="slide"
        visible={addressModalVisible}
        onRequestClose={() => {
          props.setAddressModal();
        }}
      >
        <Header onlyBack back={() => props.setAddressModal()}></Header>

        <TouchableWithoutFeedback
          onPress={() => {
            Keyboard.dismiss();
          }}
          accessible={false}
        >
          <KeyboardAvoidingView
            style={styles.modalInputContainer}
            behavior="padding"
            enabled={Platform.OS === 'ios'}
          >
            <Text style={styles.inputHeading}>Please enter address type</Text>
            <View style={styles.gap}>
              <TextInput
                onFocus={() => this.setState({ active: true })}
                onBlur={() => this.setState({ active: false })}
                autoFocus
                autoCorrect={false}
                autoCompleteType="off"
                placeholder="eg. Home, Work etc."
                style={state.active ? styles.activeInputStyles : styles.inactiveInputStyles}
                onChangeText={text => {
                  this.setState({ type: text });
                }}
              ></TextInput>
            </View>

            <View style={styles.loginBtnContainer}>
              <TouchableOpacity
                style={styles.button}
                onPress={() => {
                  props.add(state.type);
                }}
              >
                <Text style={styles.addText}>Add</Text>
              </TouchableOpacity>
            </View>
          </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}

AddressModal.propTypes = {
  addressModalVisible: PropTypes.bool.isRequired,
  setAddressModal: PropTypes.func.isRequired,
  add: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  inputHeading: {
    fontWeight: '200',
    fontSize: 16 * heightRatio,
  },
  modalInputContainer: {
    flex: 1,
    padding: heightRatio * 40,
    paddingBottom: 200 * heightRatio,
    justifyContent: 'center',
    backgroundColor: Colors.themeBackgroundColor,
  },
  gap: {
    flex: 1,
    paddingTop: 20,
  },
  activeInputStyles: {
    height: 30 * heightRatio,
    borderBottomColor: Colors.primaryColor,
    borderBottomWidth: 2,
    fontSize: 14 * heightRatio,
    padding: 10,
  },
  inactiveInputStyles: {
    height: 30 * heightRatio,
    borderBottomColor: Colors.primaryBorderColor,
    borderBottomWidth: 2,
    fontSize: 14 * heightRatio,
    padding: 10,
  },
  loginBtnContainer: {
    height: 60 * heightRatio,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  addText: {
    fontSize: 16 * heightRatio,
    textAlign: 'center',
    color: Colors.textForPrimaryBtnBgColor,
  },
  button: {
    height: 50,
    width: widthRatio * 200,
    justifyContent: 'center',
    borderColor: Colors.primaryBtnBgColor,
    backgroundColor: Colors.primaryBtnBgColor,
    borderWidth: 1,
    borderRadius: 25,
  },
});
