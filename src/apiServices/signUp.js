/* eslint-disable no-undef */
import { store } from '../redux/configureStore';
import HelperFetch from './HelperFetch';

export const signUpDb = async (
  phoneNumber,
  obj,
  verificationCode,
  handleSucess,
  handleFailure
) => {
  const { deviceInfo } = store.getState();

  const handleSuccess = res => {
    if (res.success) {
      handleSucess(res);
    } else {
      handleFailure();
    }
  };

  const handleError = () => { };

  const extension = `/otpVerification/otpVerifyEndUser`;
  await HelperFetch(
    {
      extension,
      post: true,
      body: {
        userObj: obj,
        phoneNumber,
        otp: verificationCode,
        deviceInfo,
      },
    },
    handleSuccess,
    handleError
  );

  // await fetch(`${SERVER}/api/otpVerification/otpVerifyEndUser`, {
  //   method: 'POST',
  //   headers: {
  //     Accept: 'application/json',
  //     'Content-Type': 'application/json',
  //   },
  //   body: JSON.stringify({
  //     userObj: obj,
  //     phoneNumber,
  //     otp: verificationCode,
  //     deviceInfo,
  //   }),
  // })
  //   .then(res => res.json())
  //   .then(res => {
  //     if (res.success) {
  //       handleSucess(res);
  //     } else {
  //       handleError();
  //     }
  //   })
  //   .catch(err => {
  //     throw err;
  //   });
};

/**
 * Send data for registration
 * @return {success,savedUser}
 */

export const registerVerify = async (phoneNumber, obj, props, stopLoader) => {
  const handleSuccess = res => {
    console.log(res);

    if (res.success) {
      stopLoader();
      props.navigation.navigate('registrationVerify', { phoneNumber, obj });
    }
  };

  const handleError = () => { };

  const extension = `/otpVerification/register`;
  await HelperFetch(
    {
      extension,
      post: true,
      body: {
        phoneNumber,
        obj
      },
    },
    handleSuccess,
    handleError
  );

  // await fetch(`${SERVER}/api/otpVerification/register`, {
  //   method: 'POST',
  //   headers: {
  //     Accept: 'application/json',
  //     'Content-Type': 'application/json',
  //   },
  //   body: JSON.stringify({
  //     phoneNumber,
  //   }),
  // })
  //   .then(res => res.json())
  //   .then(res => {
  //     if (res.success) {
  //       loader();
  //       props.navigation.navigate('registrationVerify', { phoneNumber, obj });
  //     }
  //   })
  //   .catch(err => {
  //     throw err;
  //   });
};

export const checkUserExistanceDb = async (
  phoneNumber,
  userInfo,
  handleFailure,
  props,
  stopLoader
) => {
  const handleSuccess = res => {
    if (!res.success) {
      registerVerify(phoneNumber, userInfo, props, stopLoader);
    } else {
      handleFailure();
      stopLoader();
    }
  };

  const handleError = () => { };

  const extension = `/endUserAuth/checkUserExistance`;
  await HelperFetch(
    {
      extension,
      post: true,
      body: {
        phoneNumber,
      },
    },
    handleSuccess,
    handleError
  );
};
