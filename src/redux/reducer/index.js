import { combineReducers } from 'redux';
import currentUser from './currentUser';
import dataOrderType from './dataOrderType';
import orderHistory from './orderHistory';
import deviceInfo from './deviceInfo';
import cart from './cart';
import config from './config';
import request from './request';
import tripAccepted from './tripAccepted';
import destination from './destination';

const store = combineReducers({
  currentUser,
  request,
  dataOrderType,
  deviceInfo,
  orderHistory,
  cart,
  config,
  tripAccepted,
  destination,
});

export default store;
