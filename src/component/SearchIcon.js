/* eslint-disable no-use-before-define */
import React from 'react';
import { TouchableOpacity, Image, Dimensions, View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import getImage from '../utils/getImage';

const SearchIcon = props => {
  return (
    <TouchableOpacity
      onPress={() => {
        props.pressHandler();
      }}
    >
      <View style={styles.btnContainer}>
        <Image resizeMode="contain" source={getImage.search} style={styles.searchImg} />
      </View>
    </TouchableOpacity>
  );
};
SearchIcon.propTypes = {
  pressHandler: PropTypes.func.isRequired,
};
export default SearchIcon;
const { height } = Dimensions.get('window');
const heightRatio = height / 667.0;
const styles = StyleSheet.create({
  searchImg: {
    height: 15.5 * heightRatio,
    width: 15.5 * heightRatio,
  },
  btnContainer: {
    height: 31 * heightRatio,
    width: 31 * heightRatio,
    borderRadius: 15.5 * heightRatio,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
