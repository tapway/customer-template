import { StyleSheet, Platform } from 'react-native';

import { height, width, heightRatio, widthRatio } from '../../utils/styleSheetGuide';
import Colors from '../../utils/colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.themeBackgroundColor,
  },
  placeholder: {
    height: 8,
    marginTop: 6,
    marginLeft: 15,
    alignSelf: 'flex-start',
    justifyContent: 'center',
    backgroundColor: Colors.flatListPlaceholderColor,
  },
  placeholderSingleContainer: Platform.select({
    ios: {
      width: widthRatio * 350,
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingRight: widthRatio * 20,
      marginLeft: widthRatio * 10,
      marginVertical: 10 * heightRatio,
      padding: heightRatio * 10,
      backgroundColor: Colors.flatListItemBgColor,
      shadowColor: Colors.primaryShadowColor,
      shadowOffset: { width: 0.3, height: 0.3 },
      shadowOpacity: 0.5,
      shadowRadius: 1,
    },
    android: {
      width: widthRatio * 350,
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingRight: widthRatio * 20,
      marginLeft: widthRatio * 10,
      marginVertical: 10 * heightRatio,
      padding: heightRatio * 10,
      backgroundColor: Colors.flatListItemBgColor,
      borderWidth: 1,
      borderColor: Colors.primaryBorderColor,
    },
  }),
  flexRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
  },
  resDetails: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  placeHolderLocation: {
    height: heightRatio * 10,
    width: widthRatio * 190,
  },
  placeHolderImage: {
    height: heightRatio * 50,
    width: widthRatio * 70,
  },
  progressImage: {
    height: heightRatio * 20,
    width: widthRatio * 20,
    marginLeft: widthRatio * 80,
  },
  trackBtn: {
    width: 120 * widthRatio,
    height: 30 * heightRatio,
    alignItems: 'center',
    borderRadius: 50,
    borderColor: Colors.primaryColor,
    borderWidth: 1,
    justifyContent: 'center',
  },
  trackText: {
    color: Colors.textForThemeBgColor,
    fontSize: heightRatio * 12,
    fontWeight: '400',
    letterSpacing: 0.4,
  },
  detailsBlock: {
    flexDirection: 'column',
    flex: 1,
    margin: width / 50,
  },
  datContainer: { flex: 1, flexDirection: 'row' },
  singleDetail: Platform.select({
    ios: {
      flex: 1,
      margin: 3,
      marginBottom: heightRatio * 10,
      paddingTop: height / 60,
      paddingBottom: height / 60,
      paddingRight: width / 32,
      backgroundColor: Colors.flatListItemBgColor,
      shadowColor: Colors.primaryShadowColor,
      shadowOffset: { width: 0.3, height: 0.3 },
      shadowOpacity: 0.5,
      shadowRadius: 1,
    },
    android: {
      flex: 1,
      margin: 3,
      marginBottom: heightRatio * 10,
      paddingTop: height / 40,
      paddingBottom: height / 40,
      paddingRight: width / 32,
      backgroundColor: Colors.flatListItemBgColor,
      borderWidth: 1,
      borderColor: Colors.primaryBorderColor,
    },
  }),
  imgContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: width / 5,
    marginRight: width / 100,
  },
  dishImg: {
    height: height / 16,
    width: width / 7.4,
    overflow: 'visible',
  },
  progressContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: heightRatio * 5,
    justifyContent: 'space-between',
    width: '100%',
    height: 30 * heightRatio,
  },
  progressSecCont: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  ideaImg: {
    height: height / 15,
    width: width / 9,
    overflow: 'visible',
  },
  carImg: {
    height: height / 23,
    width: width / 8,
    overflow: 'visible',
  },
  details: {
    flex: 1,
  },
  nopad: {
    paddingTop: 0,
  },
  noOrder: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  location: {
    flexDirection: 'row',
  },
  ptrImg: {
    height: height / 20,
    width: width / 22,
    marginRight: width / 50,
  },
  locationTxt: {
    flex: 1,
  },
  address: {
    color: Colors.textForThemeBgColor,
    fontSize: height / 65,
    marginBottom: height / 60,
  },

  value: { flexDirection: 'row', flex: 1, alignItems: 'center' },
  price: {
    color: Colors.textForThemeBgColor,
    fontSize: heightRatio * 12,
    marginLeft: width / 40,
    justifyContent: 'center',
    alignContent: 'center',
  },
  bar: {
    color: Colors.primaryBorderColor,
    marginLeft: width / 60,
    fontSize: height / 40,
    fontWeight: '300',
  },
  date: {
    flex: 1,
    marginLeft: width / 35,
    fontSize: height / 70,
    fontWeight: 'bold',
    textAlignVertical: 'center',
  },
});
