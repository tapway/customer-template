import { store } from '../redux/configureStore';
import HelperFetch from './HelperFetch';

/**
 * Verify the otp
 * @returns {Response:true}
 */

const verify = async (phoneNumber, otp, handleFailure, handleResult) => {
  const { deviceInfo } = store.getState();

  const handleSuccess = res => {
    console.log('here', res);

    if (res.success) {
      handleResult(res);
    } else {
      handleFailure();
    }
  };

  const handleError = () => { };

  const extension = `/endUserAuth/otpverify`;

  await HelperFetch(
    {
      extension,
      post: true,
      body: {
        phoneNumber,
        otp,
        deviceInfo,
      },
    },
    handleSuccess,
    handleError
  );
};

export default verify;
