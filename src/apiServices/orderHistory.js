import HelperFetch from './HelperFetch';

/**
 * Fetch Order History
 * @returns {Response:true,data:Array}
 */

const fetchOrderHistoryDb = async (
  jwtAccessToken,
  id,
  limit = 10,
  skip,
  handleResult,
  handleLoading,
  resolveHandler
) => {
  const handleSuccess = res => {
    handleResult(res.data);
    if (resolveHandler) {
      resolveHandler(true);
    }
    handleLoading();
  };

  const handleFailure = () => {};
  const extension = `/orders/fetchLimitedOrders`;
  await HelperFetch(
    {
      extension,
      post: true,
      body: {
        userId: id,
        limit,
        skip,
      },
      Authorization: jwtAccessToken,
    },
    handleSuccess,
    handleFailure
  );
};
export default fetchOrderHistoryDb;
